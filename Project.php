<?php

	class Project 
	{

		/**
		 * Project status
		 */
		const STATUS_INACTIVE = 0;
		const STATUS_ACTIVE = 1;
		const STATUS_SUSPENDED = 2;
		const STATUS_DELETED = 3;

		/**
		 * Get project
		 */
		static function get($id)
		{
			static $projects;

			if (isset($projects[$id])) return $projects[$id];
			// Get project
			$getProject = Cl::$db->select('p2h_project', self::fields(), TRUE, array(
				array('c'=> 'pr_id', 't'=> 'i', 'v'=> $id)
			));
			// Return
			return ($getProject['rows'] > 0) ? ($projects[$id]=$getProject['data']) : NULL;
		}

		static function create($name, $description, $dateStart, $dateEnd, $uid)
		{
			// Just create
			$createProject = Cl::$db->insert('p2h_project', array(
				array('c'=> 'pr_name', 't'=> 's', 'v'=> $name),
				array('c'=> 'pr_description', 't'=> 's', 'v'=> $description),
				array('c'=> 'pr_status', 't'=> 'i', 'v'=> self::STATUS_ACTIVE),
				array('c'=> 'pr_date_start', 't'=> 's', 'v'=> $dateStart),
				array('c'=> 'pr_date_end', 't'=> 's', 'v'=> $dateEnd),
				array('c'=> 'u_id', 't'=> 'i', 'v'=> $uid)
			));
			// Return id
			return $createProject['last_id'];
		}

		/**
		 * Add goods
		 */
		static function addGoods($goods, $prId)
		{
			// Added
			$added = 0;
			// Add goods
			if ($goods)
			{
				// Loop
				foreach ($goods as $good)
				{
					// Insert goods
					$insertGoods = Cl::$db->insert('p2h_goods_target', array(
						array('c'=> 'gt_description', 't'=> 's', 'v'=> $good['description']),
						array('c'=> 'gt_quantity', 't'=> 'd', 'v'=> $good['quantity']),
						array('c'=> 'gt_unit', 't'=> 's', 'v'=> $good['unit']),
						array('c'=> 'gt_status', 't'=> 'i', 'v'=> self::STATUS_ACTIVE),
						array('c'=> 'pr_id', 't'=> 'i', 'v'=> $prId)
					));
					// Add
					$added += ($insertGoods['last_id'] ? 1 : 0);
				}
			}
			// Return
			return $added;
		}

		/**
		 * Get goods
		 */
		static function getGoods($prId)
		{
			// Select goods
			$getGoods = Cl::$db->select('p2h_goods_target', self::goodsFields(), FALSE, array(
				array('c'=> 'pr_id', 't'=> 'i', 'v'=> $prId)
			));
			// Return 
			return ($getGoods['rows'] > 0) ? $getGoods['data'] : NULL;
		}

		/**
		 * Add station
		 */
		static function addStation($name, $description, $address, $lat, $lng, $zoom, $prId)
		{
			// Insert
			$createStation = Cl::$db->insert('p2h_station', array(
				array('c'=> 's_name', 't'=> 's', 'v'=> $name),
				array('c'=> 's_description', 't'=> 's', 'v'=> $description),
				array('c'=> 's_address', 't'=> 's', 'v'=> $address),
				array('c'=> 's_lat', 't'=> 'd', 'v'=> $lat),
				array('c'=> 's_lng', 't'=> 'd', 'v'=> $lng),
				array('c'=> 's_zoom', 't'=> 'i', 'v'=> $zoom),
				array('c'=> 's_status', 't'=> 'i', 'v'=> self::STATUS_ACTIVE),
				array('c'=> 'pr_id', 't'=> 'i', 'v'=> $prId)
			));
			// Return
			return $createStation['last_id'];
		}

		/**
		 * Get stations
		 */
		static function getStations($prId)
		{
			// Get all stations
			$getStations = Cl::$db->select('p2h_station', Station::fields(), FALSE, array(
				array('c'=> 'pr_id', 't'=> 'i', 'v'=> $prId)
			));
			// Return
			return ($getStations['rows'] > 0) ? $getStations['data'] : NULL;
		}


		/**
		 * User fields
		 */
		static function fields()
		{
			// Return fields
			return array(
				'id'					=> 'pr_id id', 
				'name'		=> 'pr_name name', 
				'description'					=> 'pr_description description', 
				'status'=> 'pr_status status', 
				'date_start'		=> 'pr_date_start date_start', 
				'date_end'			=> 'pr_date_end date_end',
				'uid'					=> 'u_id uid'
			);
		}

		/**
		 * Goods fields
		 */
		static function goodsFields()
		{
			// Return fields
			return array(
				'id'					=> 'gt_id id', 
				'description'					=> 'gt_description description', 
				'quantity'					=> 'gt_quantity quantity', 
				'unit'					=> 'gt_unit unit', 
				'status'=> 'gt_status status', 
				'prid'					=> 'pr_id prid'
			);
		}
	}