<?php


	
	class Action
	{

		/**
		 * Print options
		 */
		const OPTION_RETURN_ONLY = 0;
		const OPTION_OVERWRITE_AND_RETURN = 1;
		const OPTION_RETURN_STATUS = 2;
		/**
		 * Action var
		 */
		static $var = '_action';
		/**
		 * Current action
		 */
		static $action = NULL;
		/**
		 * Default values
		 */
		static $value = array();
		/**
		 * Response values
		 */
		static $response = array();
		/**
		 * Actions path
		 */
		static $path;

		/**
		 * Initialize actions
		 */
		static function init()
		{
			// Check if there's action var
			if ($action=Input::request(self::$var))
			{
				// Execute action
				self::exec($action);
			}
		}

		/**
		 * Execute an action
		 */
		static function exec($action)
		{
			// If action starts with @, then trim it
			if ($action[0]=='@') $action = substr($action, 1);
			// Check if action file exists
			if (is_file($file=(self::$path.'/'.$action.'.php')))
			{
				// Set action
				self::$action = $action;
				// Require action file
				@require($file);
			}
		}
		
		/**
		 * Begin form
		 */
		static function begin($action, $targetUrl=NULL, $method='post', $attr=NULL)
		{
			// Set current action
			self::$action = $action;
			// If there's no target url, then it should be the current one
			if ($targetUrl===NULL) $targetUrl=Page::thisUrl();
			// Set action attribute
			$attr['action'] = $targetUrl;
			// Set method attribute
			$attr['method'] = $method;
			// Print
			echo '<form ',Str::attr($attr, TRUE),'>';
		}
		/**
		 * End form
		 */
		static function end()
		{
			// If there's no action, then quit
			if (self::$action===NULL) return NULL;
			// If there's action, then print
			if (self::$action[0]!='@')
			{
				self::input(self::$var, 'hidden', self::$action);
			}
			// Delete action
			self::$action = NULL;
			// Print
			echo '</form>';
		}
		
		/**
		 * Draw an input
		 */
		static function input($name=NULL, $type='text', $value=NULL, $attr=NULL)
		{
			// Set attributes
			$attr['name'] = $name;
			$attr['type'] = $type;
			// Select which type
			switch ($type)
			{
				case 'checkbox':
				// Text, Hidden, Password, Checkbox
				case 'text':
				case 'hidden':
				case 'password':
					// Set value
					if ($value!==NULL) $attr['value'] = $value;
					break;
			}
			// Print
			echo '<input ',Str::attr($attr, TRUE),' />';
		}

		/**
		 * Draw a textarea
		 */
		static function textarea($name, $value=NULL, $attr=NULL)
		{
			// Set attributes
			$attr['name'] = $name;
			// Print
			echo '<textarea ',Str::attr($attr, TRUE),'>',Str::p($value, TRUE),'</textarea>';
		}

		/**
		 * Draw a select option
		 */
		static function select($options, $name=NULL, $value=NULL, $attr=NULL)
		{
			// Set attribute
			if ($name) $attr['name'] = $name;
			// Print head
			echo '<select',($attr?(' '.Str::attr($attr, TRUE)):''),'>';
			// Print options
			self::options($options, $value);
			// Print foot
			echo '</select>';
		}

		/**
		 * Draw select options
		 */
		static function options($options, $value=NULL)
		{
			if ($options)
			{
				// Loop through each option
				foreach ($options as $optionValue=> $option)
				{
					// Set option attributes
					$optionAttr = array();
					// Set option html
					$optionHtml = '';
					// If option is string
					if (is_string($option) || is_numeric($option))
					{
						// Set html
						$optionHtml = $option;
					}
					elseif (is_array($option))
					{
						// If label and options is set
						if (isset($option['label']) && isset($option['options']))
						{
							// Set attr
							$optGroupAttr = isset($option['attr'])?$option['attr']:array();
							// Set label
							$optGroupAttr['label'] = $option['label'];
							// Set optgroup
							echo '<optgroup ',Str::attr($optGroupAttr, TRUE),'>';
							// Recurse options
							self::options($option['options'], $value);
							// Close optgroup tag
							echo '</optgroup>';
							// Skip
							continue;
						}
						else
						{
							// If option attr is set
							if (isset($option['attr']) && is_array($option['attr']) && $option['attr'])
							{
								// Set attributes
								$optionAttr = $option['attr'];
							}
							// If html is set
							if (isset($option['html']) && is_string($option['html']))
							{
								// Set html
								$optionHtml = $option['html'];
							}
						}
					}
					// Set option value
					$optionAttr['value'] = $optionValue;
					// Set selected value
					if ($value!==NULL && $optionValue==$value)
					{
						// Set selected
						$optionAttr['selected'] = 'selected';
					}
					// Print option
					echo '<option',($optionAttr?(' '.Str::attr($optionAttr, TRUE)):''),'>',$optionHtml,'</option>';
				}
			}
		}

		/**
		 * Draw a button
		 */
		static function button($name=NULL, $type='submit', $text=NULL, $value=NULL, $attr=NULL)
		{
			// Set attributes
			$attr['type'] = $type;
			if ($name) $attr['name'] = $name;
			if ($value) $attr['value'] = $value;
			// Print
			echo '<button ',Str::attr($attr, TRUE),'>',$text,'</button>';
		}

		/**
		 * Set default value
		 * @param string $name Name
		 * @param string $value Value
		 * @return string If there's, second parameter, return default value of given name
		 */
		static function value($name, $value=NULL)
		{
			// If there's no action, exit
			if (self::$action===NULL) return NULL;
			// Set to default inputs
			if ($value!==NULL)
			{
				self::$value[self::$action][$name] = $value;
			}
			else
			{
				// Return value
				return isset(self::$value[self::$action][$name])?self::$value[self::$action][$name]:NULL;
			}
		}

		/**
		 * Set default value
		 * @param string|Response $nameOrResponse Name or Response
		 * @param string $value Value
		 * @return string If there's, second parameter, return response value of given name
		 */
		static function response($nameOrResponse, $returns=array(), $option=self::OPTION_RETURN_ONLY, $return=FALSE)
		{
			// If there's no action, exit
			if (self::$action===NULL) return NULL;
			// Check if string
			if (is_string($nameOrResponse))
			{
				// Parameter is a name
				// Set thisResponse
				$thisResponse = 
					isset(self::$response[self::$action]->responses[$nameOrResponse])?
					(self::$response[self::$action]->responses[$nameOrResponse]):
					array('type'=> Response::TYPE_DEFAULT, 'messages'=> array(''));
				// Response
				$response = NULL;
				// Get type
				switch ($option)
				{
					// Overwrite and return
					case self::OPTION_OVERWRITE_AND_RETURN:
						if (!isset($returns[$thisResponse['type']])) $returns[$thisResponse['type']] = $thisResponse['messages'][0];
					// Return only
					case self::OPTION_RETURN_ONLY:
						// Get what to response
						$response = isset($returns[$thisResponse['type']])?$returns[$thisResponse['type']]:NULL;
						break;
					// Return status
					case self::OPTION_RETURN_STATUS:
						// Set response
						$response = $thisResponse['type'];
						// Return is forced to true
						$return = TRUE;
						break;
				}
				// Print if not return
				if (!$return) echo $response;
				// Return
				return $response;
			}
			else
			{
				// Parameter is a response
				self::$response[self::$action] = $nameOrResponse;
			}
		}

		/**
		 * Get action response type
		 */
		static function responseType($action=NULL)
		{
			// If no action, get current
			if (!$action) $action = self::$action;
			// If response is not set, return nothing
			if (!isset(self::$response[$action])) return NULL;
			// Return type
			return self::$response[$action]->type;
		}

	}