package com.pledge;

import com.pledge.R;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class NewsActivity extends Activity {
	WebView mWebView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        mWebView = (WebView) findViewById(R.id.webview);
        mWebView.setWebViewClient(new NewsClient());
        mWebView.loadUrl("http://192.168.1.108/pledge2help/android/assets/www/public/register");
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && mWebView.canGoBack()) {
            mWebView.goBack();            
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
   
    private class NewsClient extends WebViewClient {
    	
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
        	System.out.println("URL: " + url);
        	view.loadUrl("javascript:changeLocation('" + url + "')");
            return true;
        }
    }
}