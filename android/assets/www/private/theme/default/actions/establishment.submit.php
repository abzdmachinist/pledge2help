<?php

	/**
	 * Submit an establishment
	 * @author Ronald Borla
	 * @copyright July 24, 2013
	 * @package establishment.submit
	 */

	// Make sure page is submit or submit step
	if (Page::$page=='submit' || Page::$page=='submit-step')
	{
		// Get step
		$step = TDF::getSubmitStep();
		// If next
		if (Input::post('btn_submit')=='next')
		{
			// If step is valid
			if ($step >= 1 && $step <= 5)
			{
				// Filter submit
				TDF::filterSubmit(Input::post(), $step);
				// Redirect to next page
				Page::go('submit-step', array('step-'.($step+1)));
			}
			elseif ($step==6)
			{
				// Set response
				$submit = new Response();
				// If there's agree
				if (Input::post('agree')!==NULL)
				{
					// Set agree value
					Action::value('agree', 1);
					// Process submit data
					TDF::processSubmit();
				}
				else
				{
					// Set error
					$submit->set('agree', 'You must agree on our terms of service and privacy policy', Response::TYPE_FAILED);
				}
				// If invalid, set response
				Action::response($submit);
			}
		}
		elseif (Input::post('btn_submit')=='reset')
		{
			// Clear data
			TDF::clearSubmit();
			// Redirect to submit page
			Page::go('submit');
		}
	}