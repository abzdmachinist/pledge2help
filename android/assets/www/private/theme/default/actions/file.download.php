<?php

	/**
	 * File download
	 * @author Ronald Borla
	 * @copyright August 14, 2013
	 * @package file.download
	 */

	// Return data
	$returnData = array();

	// Upload file
	if ($file = File::download(Input::post('url')))
	{
		// Attempt to create thumbs
		$image = new Image(File::dir('temp').'/'.$file['filename']);
		// If image is valid
		if ($image->info)
		{
			// Create thumbs
			@$image->createThumbs();
			// Return name
			$returnData = $file;
		}
	}

	// If _ajax
	if (Input::request('_ajax')!==NULL)
	{
		// Exit with json data
		exit(json_encode($returnData));
	}