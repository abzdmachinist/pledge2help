<?php

	/**
	 * Login user
	 * @author Ronald Borla
	 * @copyright November 23, 2013
	 * @package user.register
	 */

	// Make sure page is register and there's no user logged in
	if ((Page::$page=='register' || Page::$page=='organization') && !User::$loggedIn)
	{
		// Set default value
		Action::value('name', Input::post('name'));
		Action::value('email', Input::post('email'));
		// Declare response
		$register = new Response();

		// Name, Email, Password
		$name = trim(Input::post('name'));
		$email = trim(Input::post('email'));
		$password = trim(Input::post('password'));

		// If there's no name
		if (!$name) $register->set('name', 'Type your full name', Response::TYPE_FAILED);
		// If there's no user
		if (!$email) $register->set('email', 'Type your email address', Response::TYPE_FAILED);
		// If there's no password
		if (!$password) $register->set('password', 'Type your password', Response::TYPE_FAILED);

		// If take
		if (User::find($email)) $register->set('email', 'Email is already taken', Response::TYPE_FAILED);

		if ($register->type <= Response::TYPE_SUCCESS)
		{
			// Set auth
			$auth = array(User::AUTH_TYPE_EMAIL=> $email);

			// Proceed creating user
			$createUser = User::create($auth, Input::post('is_org')?User::PRIVILEGE_ORGANIZATION:User::PRIVILEGE_MEMBER, User::STATUS_ACTIVE);
			// Set password
			User::updatePassword($createUser['id'], $password);

			// After creating, add first and last name
			$profileId = Profile::create($createUser['id']);
			// Set profile
			Profile::set($profileId, array(
				'name'=> $name
			));
		}

		// Set response
		Action::response($register);
	}