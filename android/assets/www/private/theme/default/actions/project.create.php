<?php

	/**
	 * Create project
	 * @author Ronald Borla
	 * @copyright November 23, 2013
	 * @package project.create
	 */

	// Make sure page is register and there's no user logged in
	if (Page::$page=='project' && User::$loggedIn && User::is(User::PRIVILEGE_ORGANIZATION, User::$loggedIn['privilege']))
	{
		// Set default value
		Action::value('name', Input::post('name'));
		Action::value('description', Input::post('description'));
		Action::value('date_start', Input::post('date_start'));
		Action::value('date_end', Input::post('date_end'));
		// Declare response
		$createProject = new Response();

		// Name, Description, Date Start, Date End
		$name = trim(Input::post('name'));
		$description = trim(Input::post('description'));
		$date_start = trim(Input::post('date_start'));
		$date_end = trim(Input::post('date_end'));

		// If there's no name
		if (!$name) $createProject->set('name', 'Type the name of the organization', Response::TYPE_FAILED);
		// If there's no name
		if (!$description) $createProject->set('name', 'Type a description of the organization', Response::TYPE_FAILED);
		// If there's no name
		if (!$date_start) $createProject->set('date_start', 'Type date start', Response::TYPE_FAILED);
		// If there's no name
		if (!$date_end) $createProject->set('date_end', 'Type end date', Response::TYPE_FAILED);

		// Create
		if ($createProject->type <= Response::TYPE_SUCCESS)
		{
			// Do create
			$projectId = Project::create($name, $description, $date_start, $date_end, User::$loggedIn['id']);
			// Redirect to project create station page
			Page::go('project-station', array($projectId));
		}

		// If invalid, set response
		Action::response($createProject);
	}