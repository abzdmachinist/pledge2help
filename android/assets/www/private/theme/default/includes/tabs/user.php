<?php

	/**
	 * User tabs menu
	 * @author Ronald Borla
	 * @copyright May 08, 2013
	 * @package tabsUser
	 */

?>

          <div class="menu">
            <ul>
              <?php
              if (User::$loggedIn)
              {
              ?><li<?php if (Page::$page=='profile') echo ' class="selected"'; ?>><a href="<?php Page::url('profile'); ?>">Profile</a></li>
              <li<?php if (Page::$page=='account') echo ' class="selected"'; ?>><a href="<?php Page::url('account'); ?>">My Account</a></li>
              <li<?php if (Page::$page=='password') echo ' class="selected"'; ?>><a href="<?php Page::url('password'); ?>">Update Password</a></li><?php
              }
              else
              {
              ?><li<?php if (Page::$page=='login') echo ' class="selected"'; ?>><a href="<?php Page::url('login'); ?>">Login</a></li>
              <li<?php if (Page::$page=='register') echo ' class="selected"'; ?>><a href="<?php Page::url('register'); ?>">Register</a></li><?php
              }
              ?>

              <li<?php if (Page::$page=='submit' || Page::$page=='submit-step') echo ' class="selected"'; ?>><a href="<?php Page::url('submit'); ?>">Submit</a></li>
            </ul>
          </div>