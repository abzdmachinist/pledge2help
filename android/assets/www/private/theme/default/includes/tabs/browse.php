<?php

	/**
	 * Browse tabs menu
	 * @author Ronald Borla
	 * @copyright September 03, 2013
	 * @package tabsBrowse
	 */

?>

          <div class="menu">
            <ul>
              <li class="selected"><a href="<?php Page::url('browse'); ?>">Browse</a></li>
            </ul>
          </div>