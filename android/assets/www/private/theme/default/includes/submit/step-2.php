<?php

	/**
	 * Establishment submit step 2
	 * @author Ronald Borla
	 * @copyright July 08, 2013
	 */
  
?>

            <div id="tab">
              <?php Action::begin('establishment.submit', Page::url('submit-step', array('step-2'), TRUE)); ?>

                <div class="head">
                  <h1>Select categories for your establishment</h1>
                  <div>
                    <p>Categories will help your clients find your establishment. Select up to five categories</p>
                  </div><?php
                  // Load steps
                  Theme::load('submit-steps');
                  ?>

                </div>
                <div class="row">
                  <div class="input-row">
                    <label>
                      <div class="icon">
                        <img src="<?php Theme::root(); ?>/style/images/icon-categories.png" />
                      </div>
                      <span class="title">Add Category</span>
                      <span class="text-wrap"><?php Action::input(NULL, 'text', '', array('class'=> 'wide focus', 'autocomplete'=> 'categories')); ?></span>
                      <span class="message<?php 
                        Str::p(TDF::$data['se'][2]?' invalid':''); ?>"><?php 
                        Str::p(TDF::$data['se'][2]?TDF::$data['se'][2][0]:'Select a category (type anything related to the establishment, e.g. "hotel")'); ?></span>
                    </label>
                  </div>
                  <div id="categories"><?php
                    // If there are categories
                    if (isset(TDF::$data['s'][2]['category']) && TDF::$data['s'][2]['category'])
                    {
                      // Loop throuch each category
                      foreach (TDF::$data['s'][2]['category'] as $cgId)
                      {
                        // Set category
                        $category = Establishment::getCategoryById($cgId);
                    ?>

                    <span class="category" cgid="<?php echo $category['id']; ?>">
                      <h2><?php Str::p($category['name']); ?> <img src="<?php Theme::root(); ?>/style/images/icon-close.png" title="Remove category" /></h2>
                      <input type="hidden" name="category[]" value="<?php echo $category['id']; ?>" />
                    </span><?php
                      }
                    }
                    ?>

                  </div>
                </div>
                <div class="foot">
                  <div class="hint">
                    <p>* There should be at least one (1) category and a maximum of five (5) for every establishment</p>
                    <p>* Take note that we will review the categories that you select. Please choose the ones most appropriate</p>
                    <p>* Press [up] or [down] button on the text box to show the list of available categories or you may refer <a href="#">here</a>. Press [Esc] to hide list</p>
                    <p>* You may reorder the categories by dragging the category according to your order preference</p>
                    <p>* To avoid spamming, if we think that a category does not fit your establishment, we will remove it without prior notice</p>
                  </div>
                  <div class="option"></div>
                  <div class="buttons">
                    <?php Action::button('btn_submit', 'submit', isset(TDF::$data['s'][2])?'Save Changes':'Next Step', 'next', isset(TDF::$data['s'][2])?array('class'=> 'narrow'):NULL); ?>

                    <span>or</span>
                    <?php Action::button('btn_submit', 'submit', 'Start Over', 'reset', array('confirm'=> 'Are you sure you want to start over?\n\nPress OK to continue')); ?>

                  </div>
                </div>
                <div class="clear overflow"></div>
              <?php Action::end(); ?>

            </div>