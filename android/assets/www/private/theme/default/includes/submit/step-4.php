<?php

	/**
	 * Establishment submit step 4
	 * @author Ronald Borla
	 * @copyright August 04, 2013
	 */

?>

            <div id="tab" class="wide">
              <?php Action::begin('establishment.submit', Page::url('submit-step', array('step-4'), TRUE)); ?>

                <div class="head">
                  <h1>Setup contact information of the establishment</h1>
                  <div>
                    <p>Fill-in contact information of your establishment. If contact information is not applicable, you may skip this step</p>
                  </div><?php
                  // Load steps
                  Theme::load('submit-steps');
                  ?>

                </div>
                <div class="row">
                  <div class="input-row">
                    <label>
                      <div class="icon">
                        <img src="<?php Theme::root(); ?>/style/images/icon-contact.png" />
                      </div>
                      <span class="title">Add Contact Information</span>
                      <span class="text-wrap"><?php Action::select(array(0=> '') + Contact::getAllTypes(), NULL, NULL, array('id'=> 'contact')); ?></span>
                      <span class="message">Select the type of contact information to add</span>
                    </label>
                  </div>
                  <div id="contacts"><?php
                    // If there are contacts
                    if (isset(TDF::$data['s'][4]['contact']) && TDF::$data['s'][4]['contact'])
                    {
                      // Loop through each data
                      foreach (TDF::$data['s'][4]['contact'] as $type=> $contacts)
                      {
                        // Loop through each contact
                        foreach ($contacts as $i=> $contact)
                        {
                    ?>

                    <div class="input-row" type="<?php echo $type; ?>">
                      <label>
                        <div class="icon"></div>
                        <span class="title small"><?php Str::p(Contact::getAllTypes($type)); ?></span>
                        <span class="text-wrap"><?php Action::input('contact['.$type.'][]', 'text', $contact); ?></span>
                      </label>
                      <label>
                        <span class="text-wrap default">
                          <div class="default small">Add description</div>
                          <?php Action::input('description['.$type.'][]', 'text', TDF::$data['s'][4]['description'][$type][$i], array('class'=> 'small', 'default'=> 'Add description')); ?>

                        </span>
                        <span class="icon-edit icon-edit-move" title="Move contact"></span>
                        <span class="icon-edit icon-edit-remove" title="Remove contact"></span>
                        <span class="message<?php 
                          Str::p(TDF::$data['se'][4][$type][$i]?' invalid':''); ?>"><?php 
                          Str::p(TDF::$data['se'][4][$type][$i]?TDF::$data['se'][4][$type][$i][0]:('Type '.Str::lcase(Contact::getAllTypes($type)))); ?></span>
                      </label>
                    </div><?php
                        }
                      }
                    }
                    ?>

                  </div>
                </div>
                <div class="foot">
                  <div class="hint">
                    <p>* Each contact information may be rearranged (by dragging the hand icon or the title vertically) according to your order of preference</p>
                    <p>* You may submit multiple contact information under the same types</p>
                    <p>* Contact information with common types will automatically be grouped together upon submission</p>
                    <p>* Take note that Social Media accounts such as Facebook and Twitter can be added later and does not fall as a contact information</p>
                  </div>
                  <div class="option"></div>
                  <div class="buttons">
                    <?php Action::button('btn_submit', 'submit', isset(TDF::$data['s'][4])?'Save Changes':'Next Step', 'next', isset(TDF::$data['s'][4])?array('class'=> 'narrow'):NULL); ?>

                    <span>or</span>
                    <?php Action::button('btn_submit', 'submit', 'Start Over', 'reset', array('confirm'=> 'Are you sure you want to start over?\n\nPress OK to continue')); ?>

                  </div>
                </div>
                <div class="clear overflow"></div>
              <?php Action::end(); ?>

            </div>