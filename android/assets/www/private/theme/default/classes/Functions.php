<?php

	/**
	 * Default theme functions
	 * @author Ronald Borla
	 * @copyright July 25, 2013
	 * @package ThemeDefaultFunctions
	 */

	class ThemeDefaultFunctions
	{

		/**
		 * Data key
		 */
		static $dataKey = '{$+Default*-*Data*-*Key+$}';
		/**
		 * Data var
		 */
		static $dataVar = '_data';
		/**
		 * Session data
		 */
		static $data;

		/**
		 * Page auto correct
		 */
		static function pageAutoCorrect()
		{
			// Auto correct page if there's no wildcard
			if (!Page::$wildcards)
			{
				// Auto correc
				Page::correct(Page::$page);
			}
		}

		/**
		 * Load data
		 */
		static function loadData()
		{
			// Decode raw data
			self::$data = Encryption::decode(Session::get(self::$dataVar), self::$dataKey);
			// Shutdown function
			register_shutdown_function(array('ThemeDefaultFunctions', 'saveData'));
		}

		/**
		 * Save submit data
		 */
		static function saveData()
		{
			// If there's submit
			Session::set(self::$dataVar, Encryption::encode(self::$data, self::$dataKey));
		}

		/**
		 * Import data
		 */
		static function importSubmit($data)
		{
			// If there's no data, exit
			if (!$data) return NULL;
			// Decode data
			$decode = Encryption::decode($data, self::$dataKey);
			// Filter data
			self::filterSubmit($decode);
			// If there's data, redirect to step 6
			if ($decode) Page::go('submit-step', array('step-6'));
		}

		/**
		 * Export data
		 */
		static function exportSubmit($shorten=TRUE)
		{
			// Cache
			static $cache;
			// Encode data
			$encode = Encryption::encode(self::$data['s'], self::$dataKey);
			// Get hash
			$hash = sha1($encode);
			// If cache exists, return
			if (isset($cache[$hash])) return $cache[$hash];
			// Append to submit page
			$url = Page::url('submit', array(), TRUE).'?import='.urlencode($encode);
			// Set to cache
			$cache[$hash] = $encode;
			// If not shorten, then return with long url
			if (!$shorten) return $url;
			// Get shortened version and return
			return Page::shorten($url);
		}

		/**
		 * Get submit step
		 */
		static function getSubmitStep()
		{
			static $step;
			// If there's already a step, return
			if ($step) return $step;
		  // Set step
		  $step = 1;
		  // If submit step
		  if (Page::$page=='submit-step')
		  {
		    // Find which step
		    $findStep = array_search(Str::lcase(Page::$wildcards[0]), array('step-1', 'step-2', 'step-3', 'step-4', 'step-5', 'step-6'));
		    // Set step
		    if ($findStep!==FALSE)
		    {
		      // Increment step by 1
		      $step = $findStep+1;
		      // If step is 1, then redirect to submit
		      if ($step==1)
		      {
		        // Redirect to submit
		        Page::go('submit', array(), Page::$query);
		      }
		      else
		      {
		        // Correct page
		        Page::correct('submit-step', array('step-'.$step));
		      }
		    }
		    else
		    {
		      // Error
		      return 0;
		    }
		  }
		  // Return step
		  return $step;
		}

		/**
		 * Filter submit data
		 */
		static function filterSubmit($data, $step=NULL)
		{
			// If there's no data, exit
			if (!$data) $data = array();
			// If there's step
			if ($step)
			{
				// Select which step
				switch ($step)
				{
					// Step 1
					case 1:
						// Set errors
						$errors = array();
						// Process name
						self::$data['s'][1]['name'] = isset($data['name'])?trim($data['name']):'';
						// If there's no name, set error
						if (!self::$data['s'][1]['name']) $errors['name'][] = 'Type establishment name';
						// Set description
						self::$data['s'][1]['description'] = isset($data['description'])?Str::subs(Str::cleanEssay($data['description']), 0, 1000):'';
						// Set errors
						self::$data['se'][1] = $errors;
						// Break
						break;
					// Step 2
					case 2:
						// Set data
						self::$data['s'][2] = array();
						// Set errors
						$errors = array();
						// Make sure there are categories
						$categories = (isset($data['category']) && is_array($data['category']))?$data['category']:array();
						// Check if there are categories
						if ($categories)
						{
							// Set new categories
							$newCategories = array();
							// Make sure array content is unique
							$categories = array_unique($categories);
							// Make sure all categories are valid
							foreach ($categories as $cgId)
							{
								// Check if it exists
								if (Establishment::getCategoryById(intval($cgId)))
								{
									// Append
									$newCategories[] = $cgId;
								}
							}
							// Set new categories to list
							$categories = $newCategories;
						}
						// If it's empty
						if (!$categories) $errors[] = 'Select at least one (1) category';
						// Trim categories
						$categories = array_slice($categories, 0, 5);
						// If there are categories, set data
						if ($categories) self::$data['s'][2]['category'] = $categories;
						// Set errors
						self::$data['se'][2] = $errors;
						// Break
						break;
					// Step 3
					case 3:
						// Empty
						self::$data['s'][3] = array();
						// Set valid inputs
						$address = array('province', 'municipality', 'zip', 'district', 'barangay', 'village', 'street', 'hint', 'x', 'y', 'z');
						// Loop through each address
						foreach ($address as $add)
						{
							// Save
							if (isset($data[$add]))
							{
								// Set it
								self::$data['s'][3][$add] = trim($data[$add]);
							}
						}
						// Break
						break;
					// Step 4
					case 4:
						// Filter all contacts
						$filterContacts = Contact::filterAll($data);
						// Set contact and description
						self::$data['s'][4] = array(
							'contact'=> $filterContacts['contact'],
							'description'=> $filterContacts['description']
						);
						// Set errors
						self::$data['se'][4] = $filterContacts['error'];
						// Break
						break;
					// Step 5
					case 5:
						// Empty photos
						self::$data['s'][5] = array();
						// Check if there's temp
						if (isset($data['temp']) && $data['temp'])
						{
							// Default found
							$defaultFound = FALSE;
							// Loop through each temp
							foreach ($data['temp'] as $i=> $temp)
							{
								// Make sure temp exists
								if (!is_file(File::dir('temp').'/'.$temp)) continue;
								// Set title
								$title = isset($data['title'][$i])?trim($data['title'][$i]):date('Y-m-d h:i:s a');
								// If default
								$default = isset($data['default'][$i])?($data['default'][$i]=='1'):FALSE;
								// If default found, set to false
								if ($defaultFound) $default = FALSE;
								// If found
								if ($default) $defaultFound = TRUE;
								// Append to photos
								self::$data['s'][5]['temp'][] = $temp;
								self::$data['s'][5]['title'][] = $title;
								self::$data['s'][5]['default'][] = $default?1:0;
							}
							// If there's no default, set first as default
							if (!$defaultFound) self::$data['s'][5]['default'][0] = TRUE;
						}
						break;
				}
			}
			else
			{
				// Loop throuch each data
				if ($data && is_array($data))
				{
					// Clear first
					self::clearSubmit();
					// Loop throuch each
					foreach ($data as $i=> $submitData)
					{
						// Filter submit by step
						self::filterSubmit($submitData, intval($i));
					}
				}
			}
		}

		/**
		 * Clear submit
		 */
		static function clearSubmit()
		{
			// Clear data
			self::$data['s'] = array();
			// Clear errors
			self::$data['se'] = array();
		}

		/**
		 * Process submit data
		 */
		static function processSubmit()
		{
			// If there's no data, exit
			if (!isset(self::$data['s']) || !self::$data['s']) return NULL;
			// Filter all data
			self::filterSubmit(self::$data['s']); 
			// Check for errors
			if (isset(self::$data['se']) && self::$data['se'])
			{
				// Loop through each error
				foreach (self::$data['se'] as $error)
				{
					// If there's a single error, exit
					if ($error) return NULL;
				}
			}
			// If there's no user
			if (!User::$loggedIn) return NULL;
			// Create establishment first
			$eid = Establishment::create(User::$loggedIn['id']);
			// Set name and description
			Establishment::set(array(
				'name'=> self::$data['s'][1]['name'],
				'description'=> self::$data['s'][1]['description']
			), $eid);
			// Set permalink
			Establishment::setPermalink(self::$data['s'][1]['name'], $eid);
			// Add categories
			Establishment::editCategories(self::$data['s'][2]['category'], $eid);
			// Setup address
			$addressId = Address::create(self::$data['s'][3], 'Official Address', $eid, Address::REF_ESTABLISHMENT);
			// Update with default address
			Establishment::set(array('default_address'=> $addressId), $eid);
			// Setup contact information only if there are any
			if (self::$data['s'][4]) Contact::edit(self::$data['s'][4], $eid, Contact::REF_ESTABLISHMENT);
			// Setup photos only if there are any
			if (self::$data['s'][5])
			{
				// Create album
				$albumId = Photo::createAlbum('Main Photos', $eid);
				// Add photos
				$addPhotos = Photo::editAlbum(self::$data['s'][5], $albumId);
				// Get album
				$album = Photo::getAlbum($albumId);
				// Update establishment
				Establishment::set(array('default_photo'=> $album['default_photo']), $eid);
			}
			// Done
			
		}

	}

  /*
  $getCats = Cl::$db->select('dd_category', '*', FALSE, NULL, 'cg_name ASC');
  $cats = array();
  foreach ($getCats['data'] as $cat)
  {
    $cats[] = array(
      'id'=> $cat['cg_id'],
      'name'=> $cat['cg_name'],
      'tag'=> $cat['cg_search_tag']
    );
  }
  echo json_encode($cats);exit;
  */

	if (function_exists('class_alias'))
	{
		// Create alias
		class_alias('ThemeDefaultFunctions', 'TDF');
	}
	else
	{
		// Declare alias
		class TDF extends ThemeDefaultFunctions {}
	}