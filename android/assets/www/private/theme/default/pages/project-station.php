<?php

	/**
	 * Project create station
	 * @author Ronald Borla
	 * @copyright November 23, 2013
	 * @package project.station
	 */

	// Get project ID
	$projectId = Page::$wildcards[0];
	// Check if project exists
	$project = Project::get($projectId);

	// If there's no project
	if (!$project || !User::$loggedIn || (User::$loggedIn['id'] != $project['uid']))
	{
		// Go to error page
		Page::go('error');
	}

  // Set register title
  Theme::title('Pledge2Help - Create Station');
  // Load header
  Theme::load('header');

?>

<script type="text/javascript">
	$(document).ready(function(){
    // Initialize google map
    var gmapId = gmap.init($('.gmap')[0]);
    // If there's id
    if (gmapId>=0){
      // Get center
      var center = gmap.maps[gmapId].getCenter();
      // Add marker
      var markerId = gmap.mark(gmapId, center.lat(), center.lng(), 'Location on map');
      // Get x, y, z inputs
      var x = $('input:hidden[name="x"]');
      var y = $('input:hidden[name="y"]');
      var z = $('input:hidden[name="z"]');
      // Add static marker
      gmap.change(gmapId, function(){
        // Get center
        center = gmap.maps[gmapId].getCenter();
        // Set marker
        gmap.markers[gmapId][markerId].setPosition(center);
        // Set x, y, and z
        x.val(center.lat());
        y.val(center.lng());
        z.val(gmap.maps[gmapId].getZoom());
      });
    }

    $('input[name="address"]').on('change blur', function(){
    	// Locate
    	gmap.locate($(this).val(), function(e){
    		// Set center
    		gmap.center(gmapId, e);
    		gmap.zoom(gmapId, 16);
    	});
    });
	});
</script>

<?php Action::begin('project.create', Page::url('project', array(), TRUE)); ?>

<div>
  <span>Station Name</span> <?php Action::input('name', 'text', Action::value('name'), array('placeholder'=> 'Station Name')); ?>
</div>
<div>
  <span>Description</span> <?php Action::textarea('description', Action::value('description'), array('placeholder'=> 'Description')); ?>
</div>
<div>
  <span>Address</span> <?php Action::input('address', 'text', Action::value('address'), array('placeholder'=> 'Address')); ?>
</div>
<div>
	<div class="gmap" style="width: 400px; height: 300px; border: 3px white solid; box-shadow: 1px 1px 2px #888;"></div>
</div>
<?php Action::button('btn_create', 'submit', 'Create Project'); ?>

<?php Action::end(); ?>

<?php

  // Load footer
  Theme::load('footer');