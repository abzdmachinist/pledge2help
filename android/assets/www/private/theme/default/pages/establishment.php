<?php

	/**
	 * Establishment page
	 * @author Ronald Borla
	 * @copyright April 16, 2013
	 */

  // Declare est as global
  global $est;
	// Declare establishment
	$est = new Establishment(isset(Page::$wildcards[0])?trim(Page::$wildcards[0]):'');
	// If Establishment is not found
	if (!$est->info) return FALSE;
  // Declare section
  $section = 'info'; // Default is info
	// Correct establishment
	if (Page::$page=='establishment')
	{
		// Correct page
		Page::correct('establishment', array($est->info['permalink']));
	}
	elseif (Page::$page=='establishment-section')
	{
    // Set valid sections
    $validSections = array(
      Establishment::SECTION_INFO     => 'info',
      Establishment::SECTION_PHOTOS   => 'photos',
      Establishment::SECTION_REVIEWS  => 'reviews',
      Establishment::SECTION_CONTACT  => 'contact'
    );
    // Section is wildcard index 1
    $section = Str::lcase(Page::$wildcards[1]);
    // Find section
    $sectionKey = array_search($section, $validSections);
    // If false, not found
    if ($sectionKey === FALSE) return FALSE;
    // If key is info
    if ($sectionKey == Establishment::SECTION_INFO)
    {
      // Redirect to main establishment page
      Page::correct('establishment', array($est->info['permalink']));
    }
    // Correct page
    Page::correct('establishment-section', array($est->info['permalink'], $section));
	}

  // Script function calls
  $scriptFn = array('_core.establishment.init();');

  // Check which section
  switch ($section)
  {
    // Basic info
    case 'info':
      // Insert script file
      Theme::script('text/javascript', Theme::$root.'/script/jquery.cycle.all.js');
      // Append script
      $scriptFn[] = '_core.establishment.info.init();';
      break;
  }
  // Insert script 
  Theme::script('text/javascript', NULL, '_core.onInit(function(){'.implode('', $scriptFn).'});');

  // Set login title
  Theme::title('Davao Directory - '.$est->info['name']);
  // Load header
  Theme::load('header');
  
?>

    <div id="middle-wrap">
      <div id="middle"><?php 
        // Load search widget
        Theme::load('widget-search'); 
        ?>
        
        <div id="tab-wrap"><?php
          // Load tab for user
          Theme::load('tab-browse');
          ?>

          <div id="tab-body">
            <div id="tab">
              <div class="head">
                <h1><a href="<?php echo $est->permalink; ?>"><?php Str::p($est->info['name']); ?></a></h1>
                <div id="categories">
                  <?php
                  // Loop through each category
                  foreach ($est->categories as $i=> $category)
                  {
                  // If greater than 0, add comma
                  if ($i>0) echo ', ';
                  ?><a class="category" href="#"><?php Str::p($category['name']); ?></a><?php
                  }
                  ?>

                </div>
              </div>
              <div class="body">
                <div class="border-top"></div>
                <div class="content"><?php
                  // Load section
                  Theme::load('establishment-'.$section);
                  ?>

                </div>
                <div class="sidebar">
                  <div class="top"></div>
                  <div class="middle"><?php
                    // Load sidebar widgets
                    Theme::load('widget-sections')
                    ?>

                  </div>
                  <div class="bottom"></div>
                </div>
              </div>
              <div class="clear overflow"></div>
            </div>
          </div>
        </div>
      </div>
    </div><?php
  
  // Load footer
  Theme::load('footer');

  exit;
	// Print info
	print_r($est->info);
	// Print categories
	print_r($est->categories);
	// Print address
	print_r($est->address);
	// Print contacts
	print_r($est->contacts);
	// Print photos
	print_r($est->photos);
  // Print sections
  print_r($est->sections);