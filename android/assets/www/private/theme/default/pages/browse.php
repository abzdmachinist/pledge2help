<?php

	/**
	 * Default browse page
	 * @author Ronald Borla
	 * @copyright September 03, 2013
	 */

	// Initialize actions
	Action::init();
	
	// Set search title
	Theme::title('Davao Directory - Browse');
	// Load header
	Theme::load('header');
	
?>

    <div id="middle-wrap">
      <div id="middle"><?php 
      	// Load search widget
      	Theme::load('widget-search'); 
        ?>
        
      </div>
    </div><?php
	
	// Load footer
	Theme::load('footer');