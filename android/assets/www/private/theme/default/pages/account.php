<?php

	/**
	 * Default theme Account page
	 * @author Ronald Borla
	 * @copyright July 05, 2013
	 */

  if (!User::$loggedIn) Page::go('login');

  // Initialize actions
  Action::init();
	 
	// Set profile title
	Theme::title('Davao Directory - My Account');
	// Load header
	Theme::load('header');
	
?>

    <div id="middle-wrap">
      <div id="middle"><?php 
      	// Load search widget
      	Theme::load('widget-search'); 
        ?>
        
        <div id="tab-wrap"><?php
        	// Load tab for user
        	Theme::load('tab-user');
          ?>

          <div id="tab-body">
            <div id="tab">
              <?php Action::begin('account.update', Page::url('account', array(), TRUE)); ?>

                <div class="head">
                  <h1>Account settings</h1>
                  <div>
                    <p>Manage your account settings</p>
                  </div>
                </div>
                <div class="row">
                  <div class="input-row">
                    <label>
                      <div class="icon">
                        <img src="<?php Theme::root(); ?>/style/images/icon-account.png" />
                      </div>
                      <span class="title">Email Address</span>
                      <span class="text-wrap"><?php Action::input('email', 'text', User::getAuth(User::$loggedIn['id'], User::AUTH_TYPE_EMAIL), (Action::response('email', NULL, Action::OPTION_RETURN_STATUS) == Response::TYPE_FAILED)?array('class'=> 'focus', 'disabled'=> 'disabled'):array('disabled'=> 'disabled')); ?></span>
                      <span class="message<?php 
                        Action::response('email', array(Response::TYPE_FAILED=> ' invalid'), Action::OPTION_RETURN_ONLY); ?>"><?php 
                        Action::response('email', array(Response::TYPE_DEFAULT=> 'Your email address'), Action::OPTION_OVERWRITE_AND_RETURN); ?></span>
                    </label>
                  </div>
                  <div class="input-row">
                    <label>
                      <div class="icon"></div>
                      <span class="title">Username</span>
                      <span class="text-wrap"><?php Action::input('username', 'text', User::getAuth(User::$loggedIn['id'], User::AUTH_TYPE_USERNAME), (Action::response('username', NULL, Action::OPTION_RETURN_STATUS) == Response::TYPE_FAILED)?array('class'=> 'focus'):array()); ?></span>
                      <span class="message<?php 
                        Action::response('username', array(Response::TYPE_FAILED=> ' invalid'), Action::OPTION_RETURN_ONLY); ?>"><?php 
                        Action::response('username', array(Response::TYPE_DEFAULT=> 'Setup a username'), Action::OPTION_OVERWRITE_AND_RETURN); ?></span>
                    </label>
                  </div>
                  <div class="input-row">
                    <label>
                      <div class="icon"></div>
                      <span class="title">Mobile Number</span>
                      <span class="text-wrap"><?php Action::input('mobile', 'text', User::getAuth(User::$loggedIn['id'], User::AUTH_TYPE_MOBILE), (Action::response('mobile', NULL, Action::OPTION_RETURN_STATUS) == Response::TYPE_FAILED)?array('class'=> 'focus', 'disabled'=> 'disabled'):array('disabled'=> 'disabled')); ?></span>
                      <span class="message<?php 
                        Action::response('mobile', array(Response::TYPE_FAILED=> ' invalid'), Action::OPTION_RETURN_ONLY); ?>"><?php 
                        Action::response('mobile', array(Response::TYPE_DEFAULT=> 'Setup a mobile number (coming soon)'), Action::OPTION_OVERWRITE_AND_RETURN); ?></span>
                    </label>
                  </div>
                </div>
                <div class="foot">
                  <div class="hint">
                    <p>* You can setup a username for you to log in alternatively with</p>
                  </div>
                  <div class="option"></div>
                  <div class="buttons">
                    <?php Action::button('btn_save', 'submit', 'Update Account', NULL, array('class'=> 'medium')); ?>

                    <?php if (isset(Action::$response[Action::$action]) && Action::$response[Action::$action]->type == Response::TYPE_SUCCESS){ ?><span class="message valid">Account successfully updated</span><?php } ?>

                  </div>
                </div>
                <div class="clear overflow"></div>
              <?php Action::end(); ?>

            </div>
          </div>
        </div>
      </div>
    </div><?php
	
	// Load footer
	Theme::load('footer');