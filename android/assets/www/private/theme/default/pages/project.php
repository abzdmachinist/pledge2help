<?php

	/**
	 * Project page
	 * @author Ronald Borla
	 * @copyright November 23, 2013
	 * @package project
	 */

  // Initialize actions
  Action::init();

  // If user is already logged in, go to profile page
  if (!User::$loggedIn || !User::is(User::PRIVILEGE_ORGANIZATION, User::$loggedIn['privilege'])) Page::go('login');

  // Set register title
  Theme::title('Pledge2Help - Create Project');
  // Load header
  Theme::load('header');

?>

<?php Action::begin('project.create', Page::url('project', array(), TRUE)); ?>

<div class="page-name">Add Project</div>
<div class="center-div">
<div>
  <?php Action::input('name', 'text', Action::value('name'), array('placeholder'=> 'Project Name','class'=>'input-text')); ?>
</div>
<div>
  <?php Action::textarea('description', Action::value('description'), array('placeholder'=> 'Description','class'=>'input-text')); ?>
</div>
<div>
  <?php Action::input('date_start', 'text', Action::value('date_start'), array('placeholder'=> 'Date Start','class'=>'input-text')); ?>
</div>
<div>
  <?php Action::input('date_end', 'text', Action::value('date_end'), array('placeholder'=> 'Date End','class'=>'input-text')); ?>
</div>
<?php Action::button('btn_create', 'submit', 'Create','',array('class'=>'input-submit')); ?>
</div>
<?php Action::end(); ?>

<?php

  // Load footer
  Theme::load('footer');