<?php

  /**
   * Default theme Login page
   * @author Ronald Borla
   * @copyright November 23, 2013
   */
  
  // Initialize actions
  Action::init();
   
  // If user is already logged in, go to profile page
  //if (User::$loggedIn) Page::go('profile');

  // Set register title
  Theme::title('Pledge2Help - Login');
  // Load header
  Theme::load('header');
  
?>

<?php Action::begin('user.login', Page::url('login', array(), TRUE).((Input::request('next')!==NULL)?('?next='.urlencode(Input::request('next'))):'')); ?>
<div id="front">
  <div id="logo-front"></div>
  <div>
    <?php Action::input('email', 'text', Action::value('email'),array('placeholder'=> 'Email Address', 'class'=>'input-text')); ?>
    <?php Action::response('email', array(Response::TYPE_DEFAULT=> ''), Action::OPTION_OVERWRITE_AND_RETURN); ?>
  </div>
  <div>
    <?php Action::input('password', 'password', Action::value('password'),array('placeholder'=> 'Password', 'class'=>'input-text')); ?>
    <?php Action::response('password', array(Response::TYPE_DEFAULT=> ''), Action::OPTION_OVERWRITE_AND_RETURN); ?>
  </div>
  <?php Action::button('btn_login', 'submit', 'Login','',array('class'=>'input-submit')); ?>
  <a href="http://google.com">Register</a>
</div>
<?php Action::end(); ?>

<?php
  
  // Load footer
  Theme::load('footer');