<?php

	/**
	 * Logout page
	 * @author Ronald Borla
	 * @copyright June 04, 2013
	 * @package Logout
	 */

	// Do logout
	User::logout();
	// Redirect
	$next = Input::get('next');
	// If there's no next
	if ($next === NULL)
	{
		$next = Page::url('login', array(), TRUE);
	}
	// Do redirect
	header('Location: '.$next);