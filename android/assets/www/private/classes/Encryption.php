<?php

	/**
	 * Encryption Class
	 *
	 * @package Encryption
	 * @author Ronald Borla
	 * @copyright September 20, 2012
	 * @version 1.1
	 */
	
	class Encryption {
		
		const CHARS_DEFAULT = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz`~!@#$%^&*()-_=+\\|{[]};:\'",<.>/?';
		const CHARS_ALPHA_ALL_CASE = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
		const CHARS_ALPHA_UPPERCASE = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		const CHARS_ALPHA_LOWERCASE = 'abcdefghijklmnopqrstuvwxyz';
		const CHARS_NUM = '0123456789';
		const CHARS_ALPHA_NUM = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		
		/**
		 * Generate random string
		 * @param int $length Length of string
		 * @param string $chars Characters to use
		 * @return string Generated random string
		 */
		static function generateRandomString($length, $chars=self::CHARS_DEFAULT)
		{
			$string='';
			// Chars length
			$clen = strlen($chars);
			// Loop through length
			for ($i=0; $i<$length; $i++)
			{
				// Get a random character from chars
				$rnd = mt_rand(0, $clen-1);
				$string .= $chars[$rnd];
			}
			// Return with generated random string
			return $string;
		}
		
		/**
		 * Generate AES key
		 * @param string $key AES raw key
		 * @return string AES Key
		 */
		static function aesKey($key)
		{
			$new_key = str_repeat(chr(0), 16);
			for($i=0,$len=strlen($key);$i<$len;$i++)
			{
				$new_key[$i%16] = $new_key[$i%16] ^ $key[$i];
			}
			return $new_key;
		}
		
		/**
		 * AES Encrypt
		 * @param string $str String to encrypt
		 * @param string $key Encryption key
		 * @return string Encrypted string
		 */
		static function aesEncrypt($str, $key)
		{
			$key = self::aesKey($key);
			$pad_value = 16-(strlen($str) % 16);
			$str = str_pad($str, (16*(floor(strlen($str) / 16)+1)), chr($pad_value));
			return mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $str, MCRYPT_MODE_ECB, 
				mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB), MCRYPT_DEV_URANDOM));
		}
		
		/**
		 * AES Decrypt
		 * @param string $str String to decrypt
		 * @param string $key Encryption key
		 * @return string Decrypted string
		 */
		static function aesDecrypt($str, $key)
		{
			$key = self::aesKey($key);
			$str = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $str, MCRYPT_MODE_ECB,
				mcrypt_create_iv( mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB), MCRYPT_DEV_URANDOM));
			return rtrim($str, chr(0).chr(1).chr(2).chr(3).chr(4).chr(5).chr(6).chr(7).chr(8).chr(9).chr(10).chr(11).chr(12).chr(13).chr(14).chr(15).chr(16));
		}
	
		/**
		 * Encode data
		 * @param string $key Encryption key
		 * @param string $data Initial data
		 */
		static function encode($data, $key, $compress=TRUE)
		{
			// Json encode
			$json = json_encode($data);
			// Compress
			if ($compress) $json = gzcompress($json, 9);
			// Encrypt
			$encrypt = self::aesEncrypt($json, $key);
			// Compress
			if ($compress) $encrypt = gzcompress($encrypt, 9);
			// Base 64 encode
			return base64_encode($encrypt);
		}

		/**
		 * Decode data
		 * @param string $key Encryption key
		 * @param string $data Initial data
		 */
		static function decode($data, $key, $decompress=TRUE)
		{
			// Base64 decode
			$decode = base64_decode($data);
			// If there's no data, return
			if (!$decode) return NULL;
			// Decompress
			if ($decompress) $decode = gzuncompress($decode);
			// If there's data, decrypt
			$decrypt = self::aesDecrypt($decode, $key);
			// if there's no data, return
			if (!$decrypt) return NULL;
			// Decompress
			if ($decompress) $decrypt = gzuncompress($decrypt);
			// Return json
			return json_decode($decrypt, TRUE);
		}

	}