<?php

	/**
	 * Str Class
	 * @author Ronald Borla
	 * @copyright September 20, 2012
	 */
	
	class Str {
		
		// Default encoding
		static $encoding='utf-8';
		
		// Print text with specified encoding
		static function p($str, $return=false, $encoding=NULL)
		{
			if (!$encoding) $encoding = self::$encoding;
			// Set str
			$str = htmlentities($str, ENT_COMPAT, $encoding);
			// Print
			if (!$return) echo $str;
			// Return
			return $str;
		}

		/**
		 * Clean essay
		 */
		static function cleanEssay($str, $encoding=NULL)
		{
			// If there's no encoding, set default encoding
			if (!$encoding) $encoding = self::$encoding;
			// Replace all \r with \n
			$str = str_replace("\r", "\n", $str);
			// Replace all triple \n with double \n
			while (strpos($str, "\n\n\n"))
			{
				// Replace
				$str = str_replace("\n\n\n", "\n\n", $str);
			}
			// Return
			return $str;
		}

		/**
		 * Print essay
		 */
		static function essay($str, $return=FALSE, $cleanEssay=FALSE, $encoding=NULL)
		{
			// If there's no encoding, set default encoding
			if (!$encoding) $encoding = self::$encoding;
			// If clean essay, then clean it first
			if ($cleanEssay) $str = self::cleanEssay($str, $encoding);
			// Replace \n with <br />
			$str = str_replace("\n", "<br />", $str);
			// If not return
			if (!$return) echo $str;
			// Return
			return $str;
		}
		
		static function stripTwins($str, $char)
		{
			$new_str = ''; $i=0;
			while (isset($str[$i]) && $str[$i]!==NULL)
			{
				// Check if character is the char should be appended on the new string
				if ((isset($str[$i+1]) && $str[$i+1]!=$char) || !isset($str[$i+1]) || $str[$i]!=$char)
				{
					// Append string with the valid char
					$new_str .= $str[$i];
				}
				// Increment to proceed to next character
				$i++ ;
			}
			// Return with the new string
			return $new_str;
		}
		
		// Get lowercase with correct encoding
		static function lcase($str, $encoding=NULL)
		{
			if (!$encoding) $encoding = self::$encoding;
			return mb_strtolower($str, $encoding);
		}
		
		// Get uppercase with correct encoding
		static function ucase($str, $encoding=NULL)
		{
			if (!$encoding) $encoding = self::$encoding;
			return mb_strtoupper($str, $encoding);
		}
		
		// Get position of string with correct encoding
		static function spos($str, $find, $offset=0, $encoding=NULL)
		{
			if (!$encoding) $encoding = self::$encoding;
			return mb_strpos($str, $find, $offset, $encoding);
		}
		
		// Get substring with correct encoding
		static function subs($str, $start, $length=NULL, $encoding=NULL)
		{
			if (!$encoding) $encoding = self::$encoding;
			if ($length===NULL)
				$length = (self::len($str, $encoding)-$start);
			return mb_substr($str, $start, $length, $encoding);
		}
		
		// Get length of string with correct encoding
		static function len($str, $encoding=NULL)
		{
			if (!$encoding) $encoding = self::$encoding;
			return mb_strlen($str, $encoding);
		}
		
		// Get last characters of a string
		static function right($str, $len, $encoding=NULL)
		{
			if (!$encoding) $encoding = self::$encoding;
			return self::subs($str, self::len($str,$encoding)-$len, NULL, $encoding);
		}
		
		// Check whether a string matches a regex
		static function is($match, $str)
		{
			$matches = array(
				// Valid url
				'url'=> '/(http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/',
				// Check if string is an email address
				'email'=> '/^\w+[\+\.\w-]*@([\w-]+\.)*\w+[\w-]*\.([a-z]{1,30}|\d+)$/i',
				// Check if string is a mobile number
				'mobile'=> '/^[0-9\+]+$/i',
				// Check if string is a username
				'username'=> '/^[a-zA-Z]+[a-zA-Z0-9]*$/i',
				// Check if alpha
				'alpha'=> '/^[a-zA-Z]+$/i',
				// Check if numeric
				'numeric'=> '/^[0-9]+$/i',
				// Check if alphanumeric
				'alphanumeric'=> '/^[a-zA-Z0-9]+$/i'
			);
			return isset($matches[$match])?preg_match($matches[$match], $str):preg_match($match, $str);
		}
		
		// Unicode to ascii
		static function unicodeToAscii($str)
		{
			$arrchs = array('á','à','â','ã','å','ä','ª','Á','À','Â','Ã','Ä','Å',
											'é','è','ê','ë','É','È','Ê','Ë',
											'í','ì','î','ï','Í','Ì','Î','Ï',
											'ò','ó','ô','õ','º','ö','Ó','Ò','Ô','Õ','Ö',
											'ú','ù','û','ü','Ú','Ù','Û','Ü','ç','Ç','Ñ','ñ');
			$arrnew = array('a','a','a','a','a','a','a','A','A','A','A','A','A',
											'e','e','e','e','E','E','E','E',
											'i','i','i','i','I','I','I','I',
											'o','o','o','o','o','o','O','O','O','O','O',
											'u','u','u','u','U','U','U','U','c','C','N','n');
			return str_replace($arrchs, $arrnew, $str);
		}
		
		/**
		 * Build attributes
		 * @param array $attributes Array of attributes
		 * @return bool True to return only instead of printing
		 */
		static function attr($attributes, $return=FALSE)
		{
			$attr = array();
			if ($attributes)
			{
				foreach ($attributes as $param=> $value)
				{
					$attr[] = $param.'="'.self::p($value, TRUE).'"';
				}
			}
			// Form string
			$attrStr = implode(' ', $attr);
			// If return is false, print
			if (!$return) echo $attrStr;
			// Return string
			return $attrStr;
		}

		/**
		 * Get filename
		 */
		static function filename($path)
		{
			// Get basename
			$basename = basename($path);
			// Explode by .
			$arrNames = explode('.', $basename);
			// Pop array
			array_pop($arrNames);
			// Return with implode
			return implode('.', $arrNames);
		}

		/**
		 * Shorten text
		 */
		static function shorten($str, $maxlen, $chars='..', $encoding=NULL)
		{	
			if (!$encoding) $encoding = self::$encoding;
			// Get len of chars
			$charsLen = self::len($chars, $encoding);
			// Get len of str
			$len = self::len($str);
			// If len exceeds maxlen
			if ($len > $maxlen)
			{
				// Set shortened str
				$str = self::subs($str, 0, $maxlen-$charsLen).$chars;
			}
			// Return str
			return $str;
		}
		
	}