<?php

	/**
	 * Project class
	 * @author Ronald Borla
	 * @copyright November 23, 2013
	 * @package Project
	 */

	class Project 
	{

		/**
		 * Project status
		 */
		const STATUS_INACTIVE = 0;
		const STATUS_ACTIVE = 1;
		const STATUS_SUSPENDED = 2;
		const STATUS_DELETED = 3;

		/**
		 * Get project
		 */
		static function get($id)
		{
			// Get project
			$getProject = Cl::$db->select('p2h_project', self::fields(), TRUE, array(
				array('c'=> 'pr_id', 't'=> 'i', 'v'=> $id)
			));
			// Return
			return ($getProject['rows'] > 0) ? $getProject['data'] : NULL;
		}

		static function create($name, $description, $dateStart, $dateEnd, $uid)
		{
			// Just create
			$createProject = Cl::$db->insert('p2h_project', array(
				array('c'=> 'pr_name', 't'=> 's', 'v'=> $name),
				array('c'=> 'pr_description', 't'=> 's', 'v'=> $description),
				array('c'=> 'pr_status', 't'=> 'i', 'v'=> self::STATUS_ACTIVE),
				array('c'=> 'pr_date_start', 't'=> 's', 'v'=> $dateStart),
				array('c'=> 'pr_date_end', 't'=> 's', 'v'=> $dateEnd),
				array('c'=> 'u_id', 't'=> 'i', 'v'=> $uid)
			));
			// Return id
			return $createProject['last_id'];
		}

		/**
		 * User fields
		 */
		static function fields()
		{
			// Return fields
			return array(
				'id'					=> 'pr_id id', 
				'name'		=> 'pr_name name', 
				'description'					=> 'pr_description description', 
				'status'=> 'pr_status status', 
				'date_start'		=> 'pr_date_start date_start', 
				'date_end'			=> 'pr_date_end date_end',
				'uid'					=> 'u_id uid'
			);
		}
	}