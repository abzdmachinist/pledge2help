<?php

	/**
	 * Contact class
	 * @author Ronald Borla
	 * @copyright August 04, 2013
	 * @package Contact
	 */

	class Contact
	{

		/**
		 * Reference types
		 */
		const REF_ESTABLISHMENT = 1;
		const REF_PROFILE				= 2;

		/**
		 * Contact types
		 */
		const TYPE_TELEPHONE 	= 100;
		const TYPE_FAX 				= 200;
		const TYPE_MOBILE 		= 300;
		const TYPE_EMAIL 			= 400;
		const TYPE_WEBSITE 		= 500;
		const TYPE_MESSENGER 	= 600;

		/**
		 * Messenger accounts
		 */
		const MESSENGER_YAHOO = 601;
		const MESSENGER_SKYPE = 602;
		const MESSENGER_MSN 	= 603;
		const MESSENGER_AOL 	= 604;
		const MESSENGER_ICQ 	= 605;

		/**
		 * Get contacts
		 */
		static function get($refId, $ref=self::REF_ESTABLISHMENT)
		{
			// Get all
			$getContacts = Cl::$db->select('dd_contact', self::fields(), FALSE, array(
				array('c'=> 'c_ref', 't'=> 'i', 'v'=> $ref),
				array('c'=> 'c_ref_id', 't'=> 'i', 'v'=> $refId)
			), 'c_order ASC');
			// Set contacts to return
			$contacts = array();
			// Check if there are any results
			if ($getContacts['rows']>0)
			{
				// Loop through each contact
				foreach ($getContacts['data'] as $contact)
				{
					// Append by type
					$contacts[$contact['type']][] = $contact;
				}
			}
			// Return contacts
			return $contacts;
		}

		/**
		 * Create contact
		 */
		static function create($type, $value, $description, $order, $refId, $ref=self::REF_ESTABLISHMENT)
		{
			// If invalid type
			if (!self::getAllTypes($type)) return NULL;
			// Just insert data
			$createContact = Cl::$db->insert('dd_contact', array(
				array('c'=> 'c_type', 't'=> 'i', 'v'=> $type),
				array('c'=> 'c_value', 't'=> 's', 'v'=> $value),
				array('c'=> 'c_description', 't'=> 's', 'v'=> $description),
				array('c'=> 'c_order', 't'=> 'i', 'v'=> $order),
				array('c'=> 'c_ref', 't'=> 'i', 'v'=> $ref),
				array('c'=> 'c_ref_id', 't'=> 'i', 'v'=> $refId)
			));
			// Return true
			return TRUE;
		}

		/**
		 * Edit contacts
		 */
		static function edit($contacts, $refId, $ref=self::REF_ESTABLISHMENT)
		{
			// Set order
			$order = 0;
			// Clear contacts
			$deleteContacts = Cl::$db->delete('dd_contact', array(
				array('c'=> 'c_ref', 't'=> 'i', 'v'=> $ref),
				array('c'=> 'c_ref_id', 't'=> 'i', 'v'=> $refId)
			));
			// If there are contacts
			if (isset($contacts['contact']) && $contacts['contact'])
			{
				// Just simply add them
				foreach ($contacts['contact'] as $type=> $contact)
				{
					// Loop through each contact
					foreach ($contact as $i=> $value)
					{
						// Increment order
						$order++;
						// Insert contacts
						$createContact = Cl::$db->insert('dd_contact', array(
							array('c'=> 'c_type', 't'=> 'i', 'v'=> $type),
							array('c'=> 'c_value', 't'=> 's', 'v'=> $value),
							array('c'=> 'c_description', 't'=>'s', 'v'=> $contacts['description'][$type][$i]),
							array('c'=> 'c_order', 't'=> 'i', 'v'=> $order),
							array('c'=> 'c_ref', 't'=> 'i', 'v'=> $ref),
							array('c'=> 'c_ref_id', 't'=> 'i', 'v'=> $refId)
						));
					}
				}
			}
		}

		/**
		 * Get types
		 */
		static function getTypes($type=NULL)
		{
			$types = array(
				self::TYPE_TELEPHONE	=> 'Telephone Number',
				self::TYPE_FAX				=> 'Fax Number',
				self::TYPE_MOBILE			=> 'Mobile Number',
				self::TYPE_EMAIL			=> 'Email Address',
				self::TYPE_WEBSITE		=> 'Website Address',
				self::TYPE_MESSENGER	=> 'Messenger Account'
			);
			// If type is set, return with it
			if ($type)
			{
				return isset($types[$type])?$types[$type]:NULL;
			}
			else
			{
				// Return with all types
				return $types;
			}
		}

		/**
		 * Get messenger types
		 */
		static function getMessengerTypes($messengerType=NULL)
		{
			$messengerTypes = array(
				self::MESSENGER_YAHOO	=> 'Yahoo Messenger',
				self::MESSENGER_SKYPE	=> 'Skype Messenger',
				self::MESSENGER_MSN		=> 'MSN Messenger',
				self::MESSENGER_AOL		=> 'AOL Messenger',
				self::MESSENGER_ICQ		=> 'ICQ Messenger'
			);
			// If type is set, return with it
			if ($messengerType)
			{
				return isset($messengerTypes[$messengerType])?$messengerTypes[$messengerType]:NULL;
			}
			else
			{
				// Return with all messenger types
				return $messengerTypes;
			}
		}

		/**
		 * Get all types
		 */
		static function getAllTypes($type=NULL)
		{
			// If there's type
			if ($type)
			{
				// Get remainder
				$minorType = $type % 100;
				// Remove remainder
				$majorType = $type - $minorType;
				// Check if type exists
				if ($thisType = self::getTypes($majorType))
				{
					// Check if there's remainder
					if ($minorType)
					{
						// Switch
						switch ($majorType)
						{
							// Case messenger
							case self::TYPE_MESSENGER:
								// Return messenger type
								return self::getMessengerTypes($type);
								// Quit
								break;
						}
					}
					else
					{
						// Return thisType
						return $thisType;
					}
				}
				// Return nothing
				return NULL;
			}
			else
			{
				// Get types first
				$types = self::getTypes();
				// Replace messenger
				$types[self::TYPE_MESSENGER] = array(
					// Set label
					'label'=> $types[self::TYPE_MESSENGER],
					// Set options
					'options'=> self::getMessengerTypes()
				);
				// Return types
				return $types;
			}
		}

		/**
		 * Filter all contact data
		 * @param array $data Contact data (contacts, description)
		 */
		static function filterAll($data)
		{
			// New data
			$newData = array('contact'=> array(), 'description'=> array(), 'error'=> array());
			// If there are contacts
			if (isset($data['contact']) && $data['contact'])
			{
				// Loop through each contact type
				foreach ($data['contact'] as $type=> $contacts)
				{
					// Check if there are contacts
					if ($contacts)
					{
						// Loop through each contact
						foreach ($contacts as $i=> $contact)
						{
							// Trim contact
							$contact = trim($contact);
							// If type is website
							if ($type == Contact::TYPE_WEBSITE)
							{
								// If first few characters match
								if ((Str::subs($contact, 0, 7) != 'http://') && (Str::subs($contact, 0, 8) != 'https://'))
								{
									// Then prepend
									$contact = 'http://'.$contact;
								}
							}
							// Set data
							$newData['contact'][$type][$i] = $contact;
							$newData['description'][$type][$i] = trim($data['description'][$type][$i]);
							// Check if empty
							if (!$contact)
							{
								// Set error
								$newData['error'][$type][$i][] = 'Type '.Str::lcase(self::getAllTypes($type));
							}
							else
							{
								// Check if invalid
								if (!Contact::isValid($type, $contact))
								{
									// Set errors
									$newData['error'][$type][$i][] = 'Invalid '.Str::lcase(self::getAllTypes($type));
								}
							}
						}
					}
				}
			}
			// Return new data
			return $newData;
		}

		/**
		 * Check if contact is valid
		 */
		static function isValid($type, $value)
		{
			// Get minor type (get remainder with 100)
			$minorType = $type % 100;
			// Get major type
			$majorType = $type - $minorType;
			// Switch majorType
			switch ($majorType)
			{
				// Telephone
				case self::TYPE_TELEPHONE:
				// Fax number
				case self::TYPE_FAX:
				// Mobile
				case self::TYPE_MOBILE:
					// Check if valid phone/fax/mobile
					return Str::is('/^[0-9\+\(\)\- ]+$/i', $value);
					break;
				// Email
				case self::TYPE_EMAIL:
					// Check if valid email
					return Str::is('email', $value);
					break;
				// Website
				case self::TYPE_WEBSITE:
					// Check if valid website
					return Str::is('url', $value);
					break;
				// Messenger
				case self::TYPE_MESSENGER:
					// Just return
					return (Str::is('email', $value) || Str::is('/^[a-zA-Z0-9\._]+$/i', $value));
					break;
			}
			// Return false
			return FALSE;
		}

		/**
		 * Get fields
		 */
		static function fields()
		{
			// Return fields
			return array(
				'type'				=> 'c_type type', 
				'value'				=> 'c_value value',
				'description'	=> 'c_description description', 
				'order'				=> 'c_order `order`', 
				'ref'					=> 'c_ref ref', 
				'ref_id'			=> 'c_ref_id ref_id'
			);
		}

	}