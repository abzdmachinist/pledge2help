<?php

	/**
	 * Davao Directory Controller
	 * @author Ronald Borla
	 * @copyright April 15, 2013
	 * @package Controller
	 */
	 
	class Controller
	{
		// Configuration
		static $config;
		// Database handler
		static $db;
		
		/**
		 * Initialize controller
		 * @param string $theme Theme to use
		 */
		static function init($theme='default')
		{
			// Load configuration
			require(PRIVATE_PATH.'/config.php');
			// Declare database
			self::$db = new Database( 
				self::$config['db']['host'],
				self::$config['db']['user'],
				self::$config['db']['password'],
				self::$config['db']['name']
			);
			// Set character set
			self::$db->set_charset(self::$config['db']['charset']);
			// Initialize theme
			Theme::init($theme);
		}
	}
	
	// Setup alias
	if (function_exists('class_alias'))
	{
		class_alias('Controller', 'Cl');
	}
	else
	{
		// Manual aliasing
		class Cl extends Controller {}
	}