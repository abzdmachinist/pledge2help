<?php

	/**
	 * Page class
	 * @author Ronald Borla
	 * @copyright April 15, 2013
	 * @package Page
	 */
	 
	class Page
	{
		
		// Pages
		static $pages;
		// Requested page
		static $requested;
		// Query string
		static $query;
		// Current page
		static $page;
		// Wildcards
		static $wildcards;
		
		/**
		 * Initialize page
		 * @param array|string $callback Call function when page is found
		 */
		static function init($callback=NULL)
		{
			// Get page requested
			self::$requested = substr($_SERVER['REQUEST_URI'], strlen(SUB_DIR));
			// Start position
			$spos = (self::$requested[0]=='/')?1:0;
			// Find question mark
			$qpos = strpos(self::$requested, '?');
			// Strip
			if ($spos || ($qpos!==FALSE))
			{
				self::$requested = ($qpos!==FALSE)?
					substr(self::$requested, $spos, $qpos-$spos):
					substr(self::$requested, $spos);
			}
			// Set query
			self::$query = $_SERVER['QUERY_STRING'];
			// Clean up requested url
			$cleanUrl = Str::stripTwins(trim(str_replace('\\', '/', self::$requested), '/'), '/');
			// Explode url
			$urlParts = explode('/', $cleanUrl);
			// Count
			$cp = count($urlParts);
			// Loop through each apge
			foreach (self::$pages as $page=> $pageInfo)
			{
				// Set current page
				self::$page = $page;
				// If there's no file or file doesn't exist, then skip
				if (!isset($pageInfo['path']) || !is_file($pageInfo['path'])) continue;
				// Get urls
				$urls = is_array($pageInfo['url'])?$pageInfo['url']:array($pageInfo['url']);
				// Loop through each url
				foreach ($urls as $url)
				{
					// Get real urls
					$realUrls = self::realUrls($url);
					// Loop through each real url
					foreach ($realUrls as $realUrl)
					{
						// Get url parts
						$pageUrlParts = explode('/', $realUrl);
						// If count matches, then check
						if (count($pageUrlParts)==$cp)
						{
							// Set if found
							$found = TRUE;
							// Wildcard
							$wildcards = array();
							// Loop through each part
							foreach ($urlParts as $i=> $urlPart)
							{
								// Set matches
								$matches = array();
								// Check for matches
								preg_match('/^'.$pageUrlParts[$i].'$/i', $urlPart, $matches);
								// If first is found, then there is a match
								if (isset($matches[0]))
								{
									// If there's second index, then that is the wildcard
									if (isset($matches[1])) $wildcards[] = $matches[1];
								}
								else
								{
									// If there's nothing found, then it's the wrong url
									$found = FALSE;
									// Quit and move to next url
									break;
								}
							}
							// If found
							if ($found)
							{
								// Set wildcards
								self::$wildcards = $wildcards;
								// Call callback function
								if ($callback) call_user_func($callback);
								// Attempt to load file associated with the url
								if (@require($pageInfo['path']))
								{
									// Quit when require doesn't return false
									return TRUE;
								}
							}
						}
					}
				}
			}
			// If it reaches this part, then it means page wasn't found, redirect to error page
			self::go('error');
		}
		
		/**
		 * Get real url/s
		 * @param string $url Url
		 * @return array Real urls
		 */
		static function realUrls($url)
		{
			// Set urls
			$urls = array();
			// First thing to do is find %
			$sppos = strpos($url, '%');
			// If found, then get parent
			if ($sppos!==FALSE)
			{
				// Get next %
				$eppos = strpos($url, '%', $sppos+1);
				// Page name
				$page = substr($url, $sppos+1, $eppos-$sppos-1);
				// Get page urls
				$pageUrls = isset(self::$pages[$page]['url'])?self::$pages[$page]['url']:array();
				// If not array, put in array
				if (!is_array($pageUrls)) $pageUrls = array($pageUrls);
				// Loop through each url
				foreach ($pageUrls as $pageUrl)
				{
					// Get real urls
					$realUrls = self::realUrls($pageUrl);
					// Loop through each urls
					foreach ($realUrls as $realUrl)
					{
						$urls[] = trim(substr_replace($url, $realUrl, $sppos, $eppos-$sppos+1), '/');
					}
				}
			}
			else
			{
				// Just append to urls
				$urls[] = trim($url, '/');
			}
			// Return urls
			return $urls;
		}
		
		/**
		 * Get page url
		 * @param string $page Page name
		 * @param array $wildcards Wildcard values (optional)
		 * @param bool $return True to return only
		 * @param bool $absolute True to return absolute url
		 * @return string Page url
		 */
		static function url($page, $wildcards=array(), $return=FALSE, $absolute=TRUE)
		{
			// Check if page is valid
			if (!isset(self::$pages[$page]['url'])) return NULL;
			// Get real urls
			$pageUrls = self::realUrls(
				is_array(self::$pages[$page]['url'])?
				self::$pages[$page]['url'][0]:
				self::$pages[$page]['url']);
			// If there's no first url, then exit
			if (!isset($pageUrls[0])) return NULL;
			// Get only first url
			$url = $pageUrls[0];
			// Loop through each wildcard
			if ($wildcards)
			{
				foreach ($wildcards as $wildcard)
				{
					// Find next wildcard
					$pos = strpos($url, '(.+)');
					// If found
					if ($pos!==FALSE)
					{
						// Replace with correct wildcard
						$url = substr_replace($url, $wildcard, $pos, 4);
					}
				}
			}
			// Set final url
			$finalUrl = ($absolute?(SITE_ROOT.'/'):'').$url;
			// If not return, then print
			if (!$return) echo $finalUrl;
			// Return with url
			return $finalUrl;
		}
		
		/**
		 * Get current url
		 */
		static function thisUrl($return=TRUE)
		{
			// Return with url
			return self::url(self::$page, self::$wildcards, $return);
		}
		
		/**
		 * Go to page
		 * @param string $page Page name
		 * @param array $wildcards Wildcard values (optional)
		 * @param string $query Query string
		 * @return bool True if successful
		 */
		static function go($page, $wildcards=array(), $query='')
		{
			// Get url
			$url = self::url($page, $wildcards, TRUE);
			// If there's url
			if ($url)
			{
				// Redirect
				self::redirect($url.($query?('?'.$query):''));
			}
			else
			{
				// Return with false
				return FALSE;
			}
		}

		/**
		 * Redirect to a page
		 */
		static function redirect($url, $exit=TRUE)
		{
			// Redirect
			header('Location: '.$url);
			// If exit
			if ($exit) exit;
		}

		/**
		 * Correct page
		 * @param string $page Page
		 * @param array $wildcards Wildcards
		 * @param bool $caseSensitive True to set as case sensitive
		 */
		static function correct($page, $wildcards=array(), $caseSensitive=TRUE)
		{
			// Match
			$legitUrl = self::url($page, $wildcards, TRUE, FALSE);
			// Requested url
			$requestedUrl = self::$requested;
			// If case insensitve
			if (!$caseSensitive)
			{
				// Convert to lowercase
				$legitUrl = Str::lcase($legitUrl);
				$requestedUrl = Str::lcase($requestedUrl);
			}
			// Compare both
			if ($legitUrl==$requestedUrl)
			{
				// Return true
				return TRUE;
			}
			else
			{
				// Redirect to correct page
				Page::go($page, $wildcards, self::$query);
			}
		}

		/**
		 * Shorten page URL
		 * @param string $url Url
		 * @return string Shortened URL
		 */
		static function shorten($url)
		{
			// If disabled
			if (isset(Cl::$config['bitly']['disabled']) && Cl::$config['bitly']['disabled']) return $url;
			// Set params
			$params = array(
				'version'=> Cl::$config['bitly']['version'],
				'longUrl'=> $url,
				'login'=> Cl::$config['bitly']['login'],
				'apiKey'=> Cl::$config['bitly']['key'],
				'format'=> 'json'
			);
			// Get shortened url
			$response = file_get_contents('http://api.bit.ly/shorten?'.http_build_query($params));
			// Convert to json
			$json = @json_decode($response,true);
			// Return URL
			return isset($json['results'][$url]['shortUrl'])?$json['results'][$url]['shortUrl']:NULL;
		}
		
	}