<?php

	/**
	 * Mailer class
	 * @author Ronald Borla
	 * @copyright June 21, 2013
	 * @package Mailer
	 */

	class Mailer
	{

		/**
		 * Send single email
		 * @param string $recipient Email recipient
		 * @param string $subject Subject
		 * @param string $content Content
		 * @param string $from Sender
		 * @return bool True if successful
		 */
		static function send($recipient, $subject, $content, $from='Davao Directory <support@davaodirectory.com>')
		{
			// Send email
			return @mail($recipient, $subject, $content, implode("\n", array(
				'Organization: Davao Directory',
				'MIME-Version: 1.0',
				'Content-Type: text/html; charset="UTF-8"',
				'X-Priority: 3',
				'X-Mailer: PHP'.phpversion(),
				'From: '.$from, 
				'Return-Path: '.$from,
				'Reply-To: '.$from
			)));
		}

		/**
		 * Send email with template
		 * @param string $name Template name
		 * @param array $recipient Recipient
		 * @param array $params Parameters
		 */
		static function sendTemplate($name, $recipient, $params=array())
		{
			// Load template
			$template =Template::load(Template::TYPE_MAIL, $name, $params);
			// Send
			return self::send($recipient, $template['subject'], $template['content'], $template['sender']);
		}
	}