
var gmap = {
  /**
   * Initialized maps
   */
  maps: [],
  /**
   * Markers
   */
  markers: {},
  /**
   * Set geocoder
   */
  geocoder: new google.maps.Geocoder(),
  /**
   * Initialize an element
   */
  init: function(elem){
    // Set default configuration
    var defaultConfig = {
      x: 7.077171743017243,
      y: 125.61252636572272,
      z: 9
    };
    // Get configuration
    var config = $(elem).attr('config');
    // If there's config
    if (config){
      // Convert to json
      eval('config='+config+';');
      // Replace
      for (var i in config){
        // Set config
        defaultConfig[i] = parseFloat(config[i]);
      }
    }
    // Set map options
    var options = {
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      center: new google.maps.LatLng(defaultConfig['x'], defaultConfig['y']),
      zoom: defaultConfig['z']
    };
    // Get mapId
    var gmapId = $(elem).attr('gmap-id');
    // If there's no gmapId, create one
    if (!gmapId) gmapId = gmap.maps.length;
    // Set element gmapId
    $(elem).attr('gmap-id', gmapId);
    // Declare gmap
    gmap.maps[gmapId] = new google.maps.Map(elem, options);
    // Return gmapId
    return gmapId;
  },
  /**
   * Get element gmap id
   */
  getId: function(elem){
    // Get map
    var gmapId = $(elem).attr('gmap-id');
    // Return id
    return (gmapId && gmaps.maps[gmapId])?parseInt(gmapId):-1;
  },
  /**
   * Add marker
   */
  mark: function(gmapId, x, y, title){
    // Check if marker is not set, declare if it's not
    if (!gmap.markers[gmapId]) gmap.markers[gmapId] = [];
    // Set marker id
    var markerId = gmap.markers[gmapId].length;
    // Set marker
    gmap.markers[gmapId][markerId] = new google.maps.Marker({ 
      'position': new google.maps.LatLng(x, y),
      'title': title, 
      'map': gmap.maps[gmapId]
    });
    // Return marker id
    return markerId;
  },
  /**
   * Disable map
   */
  disable: function(gmapId){
    // Set options
    gmap.maps[gmapId].setOptions({
      disableDefaultUI: true,
      mapTypeControl: false,
      draggable: false,
      scrollwheel: false
    });
  },
  /**
   * Get/Set center
   */
  center: function(gmapId, location){
    // Set center
    if (location == 'undefined'){
      // Return
      return gmap.maps[gmapId].getCenter();
    }
    // Set center
    gmap.maps[gmapId].setCenter(location);
  },
  /**
   * Set zoom
   */
  zoom: function(gmapId, level){
    // If set
    if (level == 'undefined'){
      // Return
      return gmap.maps[gmapId].getZoom();
    }
    // Set zoom
    gmap.maps[gmapId].setZoom(level);
  },
  /**
   * On map change
   */
  change: function(gmapId, fn){
    // Add listener
    google.maps.event.addListener(gmap.maps[gmapId], 'bounds_changed', fn);
    // Return true
    return true;
  },
  /**
   * Locate on map
   */
  locate: function(address, fn){
    // Locate
    gmap.geocoder.geocode({'address': address}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        // if there's function
        if (fn){
          // Set
          fn(results[0].geometry.location);
        }
      } else {
        // Not found
      }
    });
  },
  /**
   * Detect location
   */
  getCurrentPosition: function(fn){
    // Get current position
    navigator.geolocation.getCurrentPosition(function(e){
      // Send to function
      if (fn) fn({'lat': e.coords.latitude, 'lng': e.coords.longitude});
    });
  }
};