<?php



	class Arr
	{

		/**
		 * Find a string in an array
		 * @param string $str String to find
		 * @param array $array Array
		 * @param bool $caseSensitive True if sensitive
		 * @return int|string Key of found index
		 */
		static function find($str, $array, $caseSensitive=FALSE)
		{
			// Make sure there's array
			if ($array && is_array($array))
			{
				// If not case sensitive
				if (!$caseSensitive) $str = Str::lcase($str);
				// Loop through array
				foreach ($array as $key=> $value)
				{
					if (!$caseSensitive)
					{
						// Set to lowercase
						$value = Str::lcase($value);
					}
					// Match
					if ($str == $value)
					{
						// Return key
						return $key;
					}
				}
			}
			// Return false
			return FALSE;
		}

	}