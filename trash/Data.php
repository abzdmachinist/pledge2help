<?php

	class Data
	{

		/**
		 * Data
		 */
		public $data;
		/**
		 * Encryption key
		 */
		public $key;

		/**
		 * Construct
		 * @param string $data Initial data
		 * @param string $key Encryption key
		 */
		function __construct($data, $key)
		{
			// Set key
			$this->key = $key;
			// Decode data
			$this->data = $this->decode($data, $key);
		}

		/**
		 * Override __toString
		 */
		function __toString()
		{
			// Return encoded data
			return $this->encode($this->data, $this->key);
		}

		/**
		 * Override set
		 */
		function __set($name, $value)
		{
			// If data
			if ($name=='data'){
				$this->data = $value;
				return;
			} 
			// If key
			if ($name=='key'){
				$this->key = $value;
				return;
			}
			// Set data
			$this->data[$name] = $value;
		}

		/**
		 * Override __get
		 */
		function __get($name)
		{
			// If data
			if ($name=='data') return $this->data;
			// If key
			if ($name=='key') return $this->key;



		}

		/**
		 * Encode data
		 * @param string $key Encryption key
		 * @param string $data Initial data
		 */
		function encode($data, $key)
		{
			// Json encode
			$json = json_encode($data);
			// Encrypt
			$encrypt = Encryption::aesEncrypt($json, $key);
			// Base 64 encode
			return base64_encode($encrypt);
		}

		/**
		 * Decode data
		 * @param string $key Encryption key
		 * @param string $data Initial data
		 */
		function decode($data, $key)
		{
			// Base64 decode
			$decode = base64_decode($data);
			// If there's no data, return
			if (!$decode) return NULL;
			// If there's data, decrypt
			$decrypt = Encryption::aesDecrypt($decode, $key);
			// if there's no data, return
			if (!$decrypt) return NULL;
			// Return json
			return json_decode($decrypt, TRUE);
		}

	}