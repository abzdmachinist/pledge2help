<?php

	class File
	{

		/**
		 * Get dir
		 * @param string $subdir Subdirectory
		 */
		static function dir($subdir=NULL)
		{
			// Set dir
			$dir = PUBLIC_PATH . '/dd-upload';
			// If there's subdir, append
			if ($subdir) $dir .= ('/'.$subdir);
			// Return dir
			return $dir;
		}

		/**
		 * Get root
		 * @param string $subdir Subdirectory
		 */
		static function root($subroot=NULL)
		{
			// Set root
			$root = SITE_ROOT . '/dd-upload';
			// If there's subroot, append
			if ($subroot) $root .= ('/'.$subroot);
			// Return root
			return $root;
		}

		/**
		 * Upload file
		 * @param string $name Input name
		 */
		static function upload($name)
		{
			// If file is not set
			if (!isset($_FILES[$name]['tmp_name'])) return NULL;
			// Get temporary filename
			$filename = self::temp();
			// Move uploaded file
			if (!@move_uploaded_file($_FILES[$name]['tmp_name'], self::dir('temp').'/'.$filename)) return NULL;
			// Return name and filename
			return array(
				'name'=> isset($_FILES[$name]['name'])?$_FILES[$name]['name']:Str::filename($filename),
				'filename'=> $filename
			);
		}

		/**
		 * Download file
		 * @param string $url File url
		 */
		static function download($url)
		{
			// Make sure url is valid
			if (!Str::is('url', $url)) return NULL;
			// Get temporary filename
			$filename = self::temp();
			// Proceed downloading
			// Open file to save
			$file = @fopen(self::dir('temp').'/'.$filename, 'w');
			// Use curl
			$ch = curl_init($url);
			// Set file option
			curl_setopt($ch, CURLOPT_FILE, $file);
			curl_setopt($ch, CURLOPT_ENCODING, ''); 
			curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:17.0) Gecko/20100101 Firefox/17.0' ); 
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, TRUE);
			// Execute
			$data = curl_exec($ch);
			// Close curl
			curl_close($ch);
			// Close file
			@fclose($file);
			// Return name and filename
			return array(
				'name'=> Str::filename($url),
				'filename'=> $filename
			);
		}

		/**
		 * Generate temporary filename
		 */
		static function temp()
		{
			// Temp dir
			$dir = self::dir('temp').'/';
			// Date
			$date = date('Y-m-d');
			// Generate filename
			$filename = date('His').'.'.str_pad(microtime(TRUE), 15, '0');
			// If directory not valid, create it
			if (!is_dir($dir.$date)) @mkdir($dir.$date);
			// Set counter
			$counter = 0;
			// Check if filename is valid
			while (is_file($dir.$date.'/'.$filename.($counter?('-'.$counter):'')))
			{
				// Increment counter
				$counter++;
			}
			// Return temp filename
			return $date.'/'.$filename.($counter?('-'.$counter):'');
		}

		/**
		 * Delete file
		 * @param string $filename Filename
		 */
		static function delete($filename)
		{
			// Unlink
			return @unlink($filename);
		}

	}