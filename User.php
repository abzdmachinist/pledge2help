<?php
	 
	class User
	{
		
		// Logged in user
		static $loggedIn;
		// Facebook object
		static $fb;
		// Set profile
		static $profile;
		// Auth var
		const AUTH_VAR = '_u';
		// Auth key
		const AUTH_KEY = '[@Pledge#2#Help*User*Session#Key@]';
		// Fb key var
		const FB_VAR_KEY = 'fbkey';
		const FB_VAR_PERSIST = 'persist';
		// Username length
		const USERNAME_MIN_LENGTH = 6;
		const USERNAME_MAX_LENGTH = 16;
		// Password length
		const PASSWORD_MIN_LENGTH = 6;
		const PASSWORD_MAX_LENGTH = 20;
		// Session status
		const SESSION_STATUS_INACTIVE = 0;
		const SESSION_STATUS_ACTIVE = 1;

		// Auth types
		const AUTH_TYPE_USERNAME	= 1;
		const AUTH_TYPE_EMAIL 		= 2;
		const AUTH_TYPE_MOBILE		= 3;
		const AUTH_TYPE_FB				= 4;
		// User privileges
		const PRIVILEGE_BOT 			= 1;
		const PRIVILEGE_VISITOR 	= 2;
		const PRIVILEGE_MEMBER 		= 4;
		const PRIVILEGE_ORGANIZATION = 8;
		const PRIVILEGE_MODERATOR = 16;
		const PRIVILEGE_ADMIN 		= 32;
		// User status
		const STATUS_INACTIVE 	= 0;
		const STATUS_ACTIVE 		= 1;
		const STATUS_SUSPENDED 	= 2;
		const STATUS_DELETED		= 3;

		// Facebook login scope
		const FB_SCOPE = 'email,user_birthday,user_relationships';

		/**
		 * Initialize user
		 */
		static function init()
		{
			// Declare FB
			self::$fb = new Facebook(Cl::$config['fb']);
			// Sources
			$sources = array(
				Session::get(self::AUTH_VAR),
				Cookie::get(self::AUTH_VAR),
				Input::get(self::AUTH_VAR),
				Input::post(self::AUTH_VAR)
			);
			// Loop through sources
			foreach ($sources as $src)
			{
				// If there's a source
				if ($src!==NULL)
				{
					// Decode
					$auth = @json_decode(
						@Encryption::aesDecrypt(
							@base64_decode($src), self::AUTH_KEY), TRUE);
					// If auth is found
					if (isset($auth['u']) && isset($auth['s']))
					{
						// Validate session
						$validateSession = Cl::$db->select('sys_user_session', self::sessionFields(), TRUE, array(
							array('c'=> 'us_id', 't'=> 'i', 'v'=> $auth['s']),
							array('c'=> 'u_id', 't'=> 'i', 'v'=> $auth['u'])
						));
						// If there's session
						if ($validateSession['rows']>0)
						{
							// Convert last active date to timestamp
							$lastActive = strtotime($validateSession['data']['date_active']);
							// Check if lastActive date plus expires is still valid
							if ((time() - $lastActive) > $validateSession['data']['expires'])
							{
								// Session is now timed out
								self::updateSessionStatus($auth['s'], self::SESSION_STATUS_INACTIVE);
								// Clear session
								self::clearSession();
							}
							else
							{
								// Set loggedIn user
								self::$loggedIn = self::get($auth['u']);
								// Set sid
								self::$loggedIn['sid'] = $auth['s'];
								// Set profile
								self::$profile = new Profile(self::$loggedIn['id']);
								// Update session
								self::updateSession($auth['s']);
							}
						}
					}
					// Break
					break;
				}
			}
		}

		/**
		 * Refresh user
		 */
		static function refresh()
		{
			// There should be a user
			if (self::$loggedIn)
			{
				// Backup sid
				$sid = $loggedIn['sid'];
				// Refresh user info
				self::$loggedIn = self::get(self::$loggedIn['id']);
				// Set sid
				self::$loggedIn['sid'] = $sid;
				// Update profile
				self::$profile->refresh();
			}
		}

		/**
		 * Get auth types
		 * @param int $type Type (optional)
		 * @return array Auth type names
		 */
		static function getAuthTypes($type=NULL)
		{
			$types = array(
				self::AUTH_TYPE_USERNAME	=> 'Username',
				self::AUTH_TYPE_EMAIL			=> 'Email address',
				self::AUTH_TYPE_MOBILE		=> 'Mobile number',
				self::AUTH_TYPE_FB				=> 'Facebook ID'
			);
			// If type is set, return with it
			if ($type)
			{
				return isset($types[$type])?$types[$type]:NULL;
			}
			else
			{
				// Return with all types
				return $types;
			}
		}

		/**
		 * Get privileges
		 * @param int $privilege Privilege (optional)
		 * @return array Privileges
		 */
		static function getPrivileges($privilege=NULL)
		{
			$privileges = array(
				self::PRIVILEGE_BOT				=> 'Search Engine Bot',
				self::PRIVILEGE_VISITOR		=> 'Visitor',
				self::PRIVILEGE_MEMBER		=> 'Regular Member',
				self::PRIVILEGE_ORGANIZATION	=> 'Organization',
				self::PRIVILEGE_MODERATOR	=> 'Moderator',
				self::PRIVILEGE_ADMIN			=> 'Administrator'
			);
			// If privilege is set, return with it
			if ($privilege)
			{
				return isset($privileges[$privilege])?$privileges[$privilege]:NULL;
			}
			else
			{
				// Return with all privileges
				return $privileges;
			}
		}

		/**
		 * Get statuses
		 * @param int $status Status (optional)
		 * @return array Statuses
		 */
		static function getStatuses($status=NULL)
		{
			$statuses = array(
				self::STATUS_INACTIVE		=> 'Inactive',
				self::STATUS_ACTIVE			=> 'Active',
				self::STATUS_SUSPENDED	=> 'Suspended',
				self::STATUS_DELETED		=> 'Deleted'
			);
			// If status is set, return with it
			if ($status)
			{
				return isset($statuses[$status])?$statuses[$status]:NULL;
			}
			else
			{
				// Return with all statuses
				return $statuses;
			}
		}

		/**
		 * Login user
		 * @param string $uid User id
		 * @param bool $useCookies Use cookies
		 * @return bool True if successful
		 */
		static function login($uid, $useCookie=FALSE)
		{
			// Get user
			$user = self::get($uid);
			// If not found
			if (!$user) return FALSE;
			// Generate session
			$sessionId = self::generateSession($uid, $useCookie?2592000:1800);
			// Generate auth (json encode, encrypt, base64 encode)
			$auth = base64_encode(
				Encryption::aesEncrypt(
					json_encode(array('u'=> $uid, 's'=> $sessionId)), self::AUTH_KEY));
			// Set auth
			if ($useCookie)
			{
				// Set for 30 days
				Cookie::set(self::AUTH_VAR, $auth, 2592000);
			}
			else
			{
				// Set for 30 minutes
				Session::set(self::AUTH_VAR, $auth);
			}
			// Set sid
			$user['sid'] = $sessionId;
			// Set logged in user
			self::$loggedIn = $user;
			// Set profile
			self::$profile = new Profile($uid);
			// Return
			return TRUE;
		}

		/**
		 * Facebook login 
		 * @param string $redirect Url to redirect after login
		 */
		static function fbLogin($redirect)
		{
			// Set params
			$params = array(
				'scope'=> self::FB_SCOPE,
				'redirect_uri'=> $redirect
			);
			// Redirect to login page
			header('Location: '.self::$fb->getLoginUrl($params));
			// Exit
			exit;
		}

		/**
		 * Create user
		 * @param array $user User/s
		 * @param int $privilege Privilege
		 * @param int $status Status
		 * @param Response $response Response object
		 * @return array User data
		 */
		static function create($user, $privilege=self::PRIVILEGE_MEMBER, $status=self::STATUS_INACTIVE)
		{
			// Create user
			$createUser = Cl::$db->insert('sys_user', array(
				array('c'=> 'u_date_created', 't'=> 'fn', 'v'=> 'utc_timestamp()'),
				array('c'=> 'u_privilege', 		't'=> 'i',	'v'=> $privilege),
				array('c'=> 'u_status', 			't'=> 'i',	'v'=> $status)
			));
			// Setup auths
			foreach ($user as $type=> $value)
			{
				// Set auth
				self::setAuth($createUser['last_id'], $type, $value);
			}
			// Return id and password
			return array(
				'id'=> $createUser['last_id']
			);
		}

		/**
		 * Get auth
		 * @param int $uid User id
		 * @param int $type Auth type
		 * @param string|array Auth type/s
		 */
		static function getAuth($uid, $type=NULL)
		{
			// Set conditions
			$cond = array(
				array('c'=> 'u_id', 't'=> 'i', 'v'=> $uid)
			);
			// If there's a type, set
			if ($type)
			{
				$cond[] = array('c'=> 'ua_type', 't'=> 'i', 'v'=> $type);
			}
			// Get auth/s
			$getAuth = Cl::$db->select('sys_user_auth', self::authFields(), $type?TRUE:FALSE, $cond, NULL, $type?'0, 1':NULL);
			// If exists
			if ($getAuth['rows'] > 0)
			{
				// If there's type, return only string
				if ($type)
				{
					return $getAuth['data']['user'];
				}
				else
				{
					// Set auth/s
					$auth = array();
					// Loop through each result
					foreach ($getAuth['data'] as $authData)
					{
						// Set each
						$auth[$authData['type']] = $authData['user'];
					}
					// Return auth/s
					return $auth;
				}
			}
			// Return null
			return NULL;
		}

		/**
		 * Set auth
		 * @param int $uid User id
		 * @param int $type Auth type
		 * @param string $value Auth value
		 */
		static function setAuth($uid, $type, $value)
		{
			// Get auth
			$getAuth = self::getAuth($uid, $type);
			// If not in use, then insert
			if (!$getAuth)
			{
				$insertAuth = Cl::$db->insert('sys_user_auth', array(
					array('c'=> 'ua_type', 	't'=> 'i', 'v'=> $type),
					array('c'=> 'ua_value', 't'=> 's', 'v'=> Str::lcase($value)),
					array('c'=> 'u_id', 		't'=> 'i', 'v'=> $uid)
				));
			}
			else
			{
				// Update
				$updateAuth = Cl::$db->update('sys_user_auth', array(
					array('c'=> 'ua_value', 't'=> 's', 'v'=> Str::lcase($value)),
				), array(
					array('c'=> 'ua_type', 	't'=> 'i', 'v'=> $type),
					array('c'=> 'u_id', 		't'=> 'i', 'v'=> $uid)
				));
			}
		}

		/**
		 * Filter auth
		 * @param int $type Auth type
		 * @param string $value Auth value
		 * @return array Success/Errors
		 */
		static function filterAuth($type, $value)
		{
			// Set response
			$return = array(
				'success'=> TRUE, 
				'errors'=> array()
			);
			// Auth type
			$authType = self::getAuthTypes($type);
			// Get short
			$arrAuth = explode(' ', $authType);
			// Short
			$short = Str::lcase($arrAuth[0]);
			// Value length
			$valueLength = Str::len($value);
			// Format
			$format = '';
			// Switch type
			switch ($type)
			{
				// Username
				case self::AUTH_TYPE_USERNAME:
					// If too short
					if ($valueLength < self::USERNAME_MIN_LENGTH) $return['errors'][] = $authType.' must contain at least '.self::USERNAME_MIN_LENGTH.' characters';
					// If too long
					if ($valueLength > self::USERNAME_MAX_LENGTH) $return['errors'][] = $authType.' must not contain more than '.self::USERNAME_MAX_LENGTH.' characters';
					// Set format
					$format = 'Must contain only alphanumerics';

				case self::AUTH_TYPE_EMAIL:
				case self::AUTH_TYPE_MOBILE:
					// Make sure it's a valid username/email/mobile
					if (!Str::is($short, $value)) $return['errors'][] = $authType.' is invalid'.($format?('. '.$format):'');
					break;
				// Facebook
				case self::AUTH_TYPE_FB:
					// Make sure it's a valid facebook id
					if (!Str::is('numeric', $value)) $return['errors'][] = $authType.' is invalid';
					break;
			}
			// Make sure auth doesn't exist yet
			$findAuth = Cl::$db->select('sys_user_auth', 'COUNT(*) rows', TRUE, array(
				array('c'=> 'ua_type', 't'=> 'i', 'v'=> $type),
				array('c'=> 'ua_value', 't'=> 's', 'v'=> $value)
			));
			// If it exists
			if ($findAuth['data']['rows'] > 0) $return['errors'][] = self::getAuthTypes($type).' is already taken';
			// Set success
			$return['success'] = !$return['errors'];
			// Return with $return
			return $return;
		}

		/**
		 * Verify user password
		 * @param int $uid User id
		 * @param string $password Password
		 * @return bool True if correct
		 */
		static function verifyPassword($uid, $password)
		{
			// Get user password info
			$getUser = Cl::$db->select('sys_user', 'u_password password, u_key `key`', TRUE, array(
				array('c'=> 'u_id', 't'=> 'i', 'v'=> $uid)
			));
			// If user found
			if ($getUser['rows']>0)
			{
				// Encrypt/encode password
				$passwordHash = sha1(Encryption::aesEncrypt($password, $getUser['data']['key']));
				// Return result
				return ($passwordHash == $getUser['data']['password']);
			}
			// Return false by default
			return FALSE;
		}

		/**
		 * Update password
		 * @param int $uid User id
		 * @param string $password Password
		 * @return string New password
		 */
		static function updatePassword($uid, $password=NULL)
		{
			// Generate a random key
			$key = Encryption::generateRandomString(mt_rand(16, 20));
			// If there's no password, set a random one
			if (!$password)
			{
				// Generate a random password
				$password = Encryption::generateRandomString(self::PASSWORD_MIN_LENGTH, Str::lcase(Encryption::CHARS_ALPHA_NUM));
			}
			// Encode password
			$encodedPassword = sha1(Encryption::aesEncrypt($password, $key));
			// Update password
			$updatePassword = Cl::$db->update('sys_user', array(
				array('c'=> 'u_password', 't'=> 's', 'v'=> $encodedPassword),
				array('c'=> 'u_key', 			't'=> 's', 'v'=> $key)
			), array(
				array('c'=> 'u_id', 't'=> 'i', 'v'=> $uid)
			));
			// Return with password
			return $password;
		}

		/**
		 * Logout user
		 */
		static function logout()
		{
			// If there's session id
			if (self::$loggedIn)
			{
				// Clear session
				self::updateSessionStatus(self::$loggedIn['sid'], self::SESSION_STATUS_INACTIVE);
			}
			// Clear session/cookie
			self::clearSession();
			// Clear facebook session
			self::$fb->destroySession();
		}

		/**
		 * Clear session/cookie
		 */
		static function clearSession()
		{
			Session::clear(self::AUTH_VAR);
			Cookie::clear(self::AUTH_VAR);
		}

		/**
		 * Generate session
		 */
		static function generateSession($uid, $expires=1800)
		{
			// Generate session
			$generateSession = Cl::$db->insert('sys_user_session', array(
				array('c'=> 'us_ip', 't'=> 's', 'v'=> self::ipAddress()),
				array('c'=> 'us_date_created', 't'=> 'fn', 'v'=> 'utc_timestamp()'),
				array('c'=> 'us_date_active', 't'=> 'fn', 'v'=> 'utc_timestamp()'),
				array('c'=> 'us_expires', 't'=> 'i', 'v'=> $expires),
				array('c'=> 'us_status', 't'=> 'i', 'v'=> self::SESSION_STATUS_ACTIVE),
				array('c'=> 'u_id', 't'=> 'i', 'v'=> $uid)
			));
			// Return with session id
			return $generateSession['last_id'];
		}

		/**
		 * Set status
		 * @param int $uid User id
		 * @param int $status User status
		 */
		static function setStatus($uid, $status)
		{
			// Make sure status is valid
			if (!self::getStatuses($status)) return NULL;
			// Set status
			$setStatus = Cl::$db->update('sys_user', array(
				array('c'=> 'u_status', 't'=> 'i', 'v'=> $status)
			), array(
				array('c'=> 'u_id', 't'=> 'i', 'v'=> $uid)
			));
			// Return
			return $setStatus['success'];
		}

		/**
		 * Set privilege
		 * @param int $uid User id
		 * @param int $privilege User privilege
		 */
		static function setPrivilege($uid, $privilege)
		{
			// Make sure status is valid
			if (!self::getPrivileges($privilege)) return NULL;
			// Set privilege
			$setPrivilege = Cl::$db->update('sys_user', array(
				array('c'=> 'u_privilege', 't'=> 'i', 'v'=> $privilege)
			), array(
				array('c'=> 'u_id', 't'=> 'i', 'v'=> $uid)
			));
			// Return
			return $setPrivilege['success'];
		}

		/**
		 * Update session
		 */
		static function updateSession($sid)
		{
			$updateSession = Cl::$db->update('sys_user_session', array(
				array('c'=> 'us_date_active', 't'=> 'fn', 'v'=> 'utc_timestamp()')
			), array(
				array('c'=> 'us_id', 't'=> 'i', 'v'=> $sid)
			));
			// Return
			return $updateSession['success'];
		}

		/**
		 * Update session status
		 */
		static function updateSessionStatus($sid, $status)
		{
			$updateSessionStatus = Cl::$db->update('sys_user_session', array(
				array('c'=> 'us_status', 't'=> 'i', 'v'=> $status)
			), array(
				array('c'=> 'us_id', 't'=> 'i', 'v'=> $sid)
			));
			// Return
			return $updateSessionStatus['success'];
		}

		/**
		 * Get user by id
		 * @param int $userId User id
		 * @return array User
		 */
		static function get($userId)
		{
			static $users;

			if (isset($users[$userId])) return $users[$userId];
			// Get user
			$getUser = Cl::$db->select('sys_user', self::fields(), TRUE, array(
				array('c'=> 'u_id', 't'=> 'i', 'v'=> $userId)
			), NULL, '0, 1');
			// Return user
			return ($getUser['rows']>0)? ($users[$userId]=$getUser['data']) :NULL;
		}

		/**
		 * Find user 
		 * @param string $user User 
		 * @param int $type User auth type
		 */
		static function find($user, $type=NULL)
		{
			// User should be small caps
			$user = Str::lcase($user);
			// Set condition
			$condition = array(
				array('c'=> 'ua_value', 't'=> 's', 'v'=> $user)
			);
			// If type is not null
			if ($type!==NULL)
			{
				$condition[] = array('c'=> 'ua_type', 't'=> 'i', 'v'=> $type);
			}
			// Find user
			$findUser = Cl::$db->select('sys_user_auth', self::authFields(), TRUE, $condition, NULL, '0, 1');
			// If user is found
			if ($findUser['rows']>0)
			{
				// Return user id
				return $findUser['data']['uid'];
			}
		}

		/**
		 * Check if user matches a permission
		 * @name is
		 * @access public
		 * @param int|array $permission User permission/s
		 * @param int $user_permission User permission value
		 * @return bool True if it matches permission
		 */
		static function is($permission, $user_permission=NULL)
		{
			// Always check if user_permission has value
			if ($user_permission)
			{
				// Check if permission is not in array, else put in array
				if (!is_array($permission))
				{
					$permission = array($permission);
				}
				// Check each permission by bit value
				foreach ($permission as $perm)
				{
					// If permission matches, return true
					if ($user_permission & $perm)
					{
						return TRUE;
					}
				}
			}
			// Return false by default
			return FALSE;
		}

		/**
		 * Get user IP address
		 */
		static function ipAddress()
		{
			return isset($_SERVER['HTTP_X_FORWARDED_FOR'])?
				array_pop(explode(',', $_SERVER['HTTP_X_FORWARDED_FOR'])):
				$_SERVER['REMOTE_ADDR'];
		}

		/**
		 * User fields
		 */
		static function fields()
		{
			// Return fields
			return array(
				'id'					=> 'u_id id', 
				'password'		=> 'u_password password', 
				'key'					=> 'u_key `key`', 
				'date_created'=> 'u_date_created date_created', 
				'privilege'		=> 'u_privilege privilege', 
				'status'			=> 'u_status status'
			);
		}
		/**
		 * User auth fields
		 */
		static function authFields()
		{
			// Return fields
			return array(
				'type'=> 'ua_type `type`', 
				'user'=> 'ua_value user',
				'uid'	=> 'u_id uid'
			);
		}
		/**
		 * User session fields
		 */
		static function sessionFields()
		{
			// Return fields
			return array(
				'id'					=> 'us_id id', 
				'ip'					=> 'us_ip ip', 
				'date_created'=> 'us_date_created date_created',
				'date_active'	=> 'us_date_active date_active',
				'expires'			=> 'us_expires expires',
				'status'			=> 'us_status status',
				'uid'					=> 'u_id uid'
			);
		}
		
	}