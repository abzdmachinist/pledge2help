<?php

	class Profile
	{

		/**
		 * Profile types
		 */
		const TYPE_USER = 1;
		/**
		 * Gender
		 */
		const GENDER_MALE = 0;
		const GENDER_FEMALE = 1;
		/**
		 * Relationship status
		 */
		const RELATIONSHIP_STATUS_SINGLE = 0;
		const RELATIONSHIP_STATUS_IN_A_RELATIONSHIP = 1;
		const RELATIONSHIP_STATUS_ENGAGED = 2;
		const RELATIONSHIP_STATUS_MARRIED = 3;
		const RELATIONSHIP_STATUS_ITS_COMPLICATED = 4;
		const RELATIONSHIP_STATUS_IN_AN_OPEN_RELATIONSHIP = 5;
		const RELATIONSHIP_STATUS_WIDOWED = 6;
		const RELATIONSHIP_STATUS_SEPARATED = 7;
		const RELATIONSHIP_STATUS_DIVORCED = 8;

		/**
		 * Profile data
		 */
		private $data;

		/**
		 * Construct
		 * @param int $refId Reference id
		 * @param int $type Profile type
		 */
		function __construct($refId, $type=self::TYPE_USER)
		{
			// Get data
			$this->data = self::find($refId, $type);
			// If not found, then create
			if (!$this->data)
			{
				// Create profile
				$profileId = self::create($refId, $type);
				// Get data
				$this->data = self::get($profileId);
			}
		}

		/**
		 * Get override
		 */
		function __get($name)
		{
			// Set valid fields
			$validFields = array(
				'id',
				'name'
			);
			// Return if set
			if (in_array($name, $validFields) && isset($this->data[$name]))
			{
				return $this->data[$name];
			}
			else
			{
				// Return NULL
				return NULL;
			}
		}

		/**
		 * Get genders
		 */
		static function getGenders($gender=NULL)
		{
			$genders = array(
				self::GENDER_MALE		=> 'Male',
				self::GENDER_FEMALE	=> 'Female'
			);
			// If gender is set, return with it
			if ($gender)
			{
				return isset($genders[$gender])?$genders[$gender]:NULL;
			}
			else
			{
				// Return with all genders
				return $genders;
			}
		}

		/**
		 * Get relationship statuses
		 */
		static function getRelationshipStatuses($relationshipStatus=NULL)
		{
			$relationshipStatuses = array(
				self::RELATIONSHIP_STATUS_SINGLE									=> 'Single',
				self::RELATIONSHIP_STATUS_IN_A_RELATIONSHIP				=> 'In a Relationship',
				self::RELATIONSHIP_STATUS_ENGAGED									=> 'Engaged',
				self::RELATIONSHIP_STATUS_MARRIED									=> 'Married',
				self::RELATIONSHIP_STATUS_ITS_COMPLICATED					=> 'It\'s Complicated',
				self::RELATIONSHIP_STATUS_IN_AN_OPEN_RELATIONSHIP	=> 'In an Open Relationship',
				self::RELATIONSHIP_STATUS_WIDOWED									=> 'Widowed',
				self::RELATIONSHIP_STATUS_SEPARATED								=> 'Separated',
				self::RELATIONSHIP_STATUS_DIVORCED								=> 'Divorced'
			);
			// If relationship status is set, return with it
			if ($relationshipStatus)
			{
				return isset($relationshipStatuses[$relationshipStatus])?$relationshipStatuses[$relationshipStatus]:NULL;
			}
			else
			{
				// Return with all relationship statuses
				return $relationshipStatuses;
			}
		}

		/**
		 * Refresh profile data
		 */
		function refresh()
		{
			// Set to data
			$this->data = self::get($this->data['id']);
		}

		/**
		 * Get profile
		 * @param int $id Profile id
		 * @return array Profile data
		 */
		static function get($id)
		{
			// Get profile
			$getProfile = Cl::$db->select('dd_profile', self::fields(), TRUE, array(
				array('c'=> 'p_id', 't'=> 'i', 'v'=> $id)
			));
			// Return profile
			return ($getProfile['rows']>0)?$getProfile['data']:NULL;
		}

		/**
		 * Set profile
		 * @param int $id Profile id
		 * @param array $profile Profile data
		 */
		static function set($id, $profile)
		{
			// If there's no profile, return
			if (!$profile || !is_array($profile)) return NULL;
			// Fields to edit
			$validFields = array(
				'name'=> 's',
			);
			// Set parameters
			$params = array();
			// Loop through each profile
			foreach ($profile as $field=> $value)
			{
				// If valid, then set
				if (isset($validFields[$field]))
				{
					$params[] = array(
						'c'=> 'p_'.$field,
						't'=> $validFields[$field],
						'v'=> $value
					);
				}
			}
			// Update profile
			$updateProfile = Cl::$db->update('p2h_profile', $params, array(
				array('c'=> 'p_id', 't'=> 'i', 'v'=> $id)
			));
			// Return
			return $updateProfile['success'];
		}

		/**
		 * Create profile
		 * @param int $refId Reference id
		 * @param int $type Profile type
		 * @return int Profile id
		 */
		static function create($refId, $type=self::TYPE_USER)
		{
			// Create profile
			$createProfile = Cl::$db->insert('p2h_profile', array(
				array('c'=> 'u_id', 't'=> 'i', 'v'=> $refId)
			));
			// Return id
			return $createProfile['last_id'];
		}

		/**
		 * Find profile
		 * @param int $refId Reference id
		 * @param int $type Profile type
		 */
		static function find($refId, $type=self::TYPE_USER)
		{
			static $profiles;

			if (isset($profiles[$refId])) return $profiles[$refId];
			// Select profile
			$findProfile = Cl::$db->select('p2h_profile', self::fields(), TRUE, array(
				array('c'=> 'u_id', 't'=> 'i', 'v'=> $refId)
			), NULL, '0, 1');
			// Set data
			return ($findProfile['rows']>0)? ($profiles[$refId]=$findProfile['data']) :NULL;
		}

		static function fields()
		{
			// Return fields
			return array(
				'id'									=> 'p_id id',
				'name'								=> 'p_name name',
				'uid'									=> 'u_id uid'
			);
		}

	}