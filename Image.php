<?php

	class Image
	{
		/**
		 * Types
		 */
		const TYPE_JPG = 1;
		const TYPE_PNG = 2;
		const TYPE_GIF = 3;
		const TYPE_BMP = 4;

		/**
		 * Image filename
		 */
		public $filename;
		/**
		 * Image handler
		 */
		public $image;
		/**
		 * Image info
		 */
		public $info = array();

		/**
		 * Construct
		 */
		function __construct($filename=NULl)
		{
			// If there's filename, load
			if ($filename) $this->load($filename);
		}

		/**
		 * Destruct
		 */
		function __destruct()
		{
			// Destroy image
			$this->destroy();
		}

		/**
		 * Load an image
		 */
		function load($filename)
		{
			// If file does not exist, exit
			if (!is_file($filename)) return NULL;
			// Destroy image first
			$this->destroy();
			// Get image info
			$this->info = @getimagesize($filename);
			// If there's mime
			if (isset($this->info['mime']))
			{
				// Switch mime
				switch ($this->info['mime'])
				{
					// JPG
					case 'image/pjpeg':
					case 'image/jpeg':
						// Create jpeg image
						$this->image = @imagecreatefromjpeg($filename);
						// Set type
						$this->info['type'] = self::TYPE_JPG;
						break;
					// PNG
					case 'image/png':
						// Create png image
						$this->image = @imagecreatefrompng($filename);
						// Set type
						$this->info['type'] = self::TYPE_PNG;
						break;
					// GIF
					case 'image/gif':
						// Create gif image
						$this->image = @imagecreatefromgif($filename);
						// Set type
						$this->info['type'] = self::TYPE_GIF;
						break;
					// BMP
					case 'image/x-ms-bmp':
						// Create bmp image
						$this->image = @imagecreatefromwbmp($filename);
						// Set type
						$this->info['type'] = self::TYPE_BMP;
						break;
				}
			}
			// If there's no image, then destroy and exit
			if (!$this->image)
			{
				// Destroy
				$this->destroy();
				// Return false
				return FALSE;
			}
			else
			{
				// Set filename
				$this->filename = $filename;
				// Return true
				return TRUE;
			}
		}

		/**
		 * Resize and save
		 * @param int $image Image handle
		 * @param int $width Image width
		 * @param int $height Image height
		 * @return array Resize dimensions (width, height)
		 */
		function resize(&$image, $width=0, $height=0)
		{
			// If there's no image, exit
			if (!$this->image) return NULL;
			// If image has invalid size
			if (($this->info[0] <= 0) || ($this->info[1] <= 0)) return NULL;
			// If there's no both width and height
			if ($width <= 0 && $height <= 0) return NULL;

			// Get width to height ratio
			$ratio = $this->info[0] / $this->info[1];
			// If height is not set
			if ($height <= 0)
			{
				// Set height automatically (preserve dimensions ratio)
				$height = $width / $ratio;
			}
			elseif ($width <= 0)
			{
				// Set width automatically (preserve dimensions ratio)
				$width = $height * $ratio;
			}
			// Set vars
			$x = 0; $y = 0; $w = $this->info[0]; $h = $this->info[1];
			// Calculate dest ratio
			$destRatio = $width / $height;
			// If ratio is equal or greater than destination ratio
			if ($ratio >= $destRatio)
			{
				// Set w
				$w = $this->info[1] * $destRatio;
				// Get center x position
				$x = ($this->info[0] - $w) / 2;
			}
			// If ratio is less than destination ratio
			else
			{
				// Set h
				$h = $this->info[0] / $destRatio;
				// Get center y position
				$y = ($this->info[1] - $h) / 2;
			}

			// Create image dest
			$image = imagecreatetruecolor($width, $height);

			// Check for type
			switch ($this->info['type'])
			{
				// PNG
				case self::TYPE_PNG:
					// Allocate color
					$color = imagecolorallocate($image, 0, 0, 0);
					// Alpha blending
					imagealphablending($image, FALSE);
					// Save alpha
					imagesavealpha($image, TRUE);
					// Fill image
					imagefill($image, 0, 0, $color);
					// Set image color transparent
					$transparent = imagecolortransparent($image, $color);
					break;
				// GIF
				/**
				 * Note that GIF has a problem (fix this)
				 */
				case self::TYPE_GIF:
					// Get color transparent
					$transparent = imagecolortransparent($this->image);
					// Setup colors
					$colors = array('red'=>0, 'green'=>0, 'blue'=> 0);
					// If transparent is greater or equal to 0
					if ($transparent >= 0)
					{
						// Get colors for index
						$colors = imagecolorsforindex($this->image, $transparent);
					}
					// Allocate color
					$color = imagecolorallocate($image, $colors['red'], $colors['green'], $colors['blue']);
					// Fill image
					imagefill($image, 0, 0, $color);
					// Set image color transparent
					$transparent = imagecolortransparent($image, $color);
					break;
			}
			// Do resize image
			if (@imagecopyresampled($image, $this->image, 0, 0, $x, $y, $width, $height, $w, $h))
			{
				// Return with dimensions
				return array(
					'width'=> $width,
					'height'=> $height
				);
			}
			else
			{
				// Return FALSE
				return FALSE;
			}
		}

		/**
		 * Save image
		 * @param int $image Image handle
		 * @param string $filename Destination filename
		 */
		static function save(&$image, $filename, $type=self::TYPE_JPG)
		{
			// Switch type
			switch ($type)
			{
				// JPG
				case self::TYPE_JPG:
					// Save as jpg
					return @imagejpeg($image, $filename);
					break;
				// PNG
				case self::TYPE_PNG:
					// Save as png
					return @imagepng($image, $filename);
					break;
				// GIF
				case self::TYPE_GIF:
					// Save as gif
					return @imagegif($image, $filename);
					break;
				// BMP
				case self::TYPE_BMP:
					// Save as bmp
					return @imagewbmp($image, $filename);
					break;
			}
			// Return NULL
			return NULL;
		}

		/**
		 * Destroy image to free memory
		 */
		function destroy()
		{
			// If there's image
			if ($this->image)
			{
				// Destroy
				@imagedestroy($this->image);
				// Empty info
				$this->info = array();
			}
		}

		/**
		 * Get thumb filename
		 */
		static function thumb($filename, $size, $return=FALSE)
		{
			// Get thumb filename
			$thumb = dirname($filename).'/thumbs/'.$size.'/'.basename($filename);
			// If not return, then print
			if (!$return) echo $thumb;
			// Return anyway
			return $thumb;
		}

		/**
		 * Create thumb
		 * @param string $size Thumb size
		 * @param int $type Image type
		 */
		function createThumbs($size=NULL, $type=self::TYPE_JPG)
		{
			// Make sure there's image
			if (!$this->image) return NULL;
			// If there's size
			if ($size)
			{
				// Make sure size is valid
				if (self::thumbs($size))
				{
					// Set thumbs dir
					$thumbsDir = dirname($this->filename).'/thumbs';
					// If dir doesn't exist, create it
					if (!is_dir($thumbsDir)) @mkdir($thumbsDir);
					// Append thumb size dir
					$thumbsDir .= ('/'.$size);
					// If this dir doesn't exist, create it
					if (!is_dir($thumbsDir)) @mkdir($thumbsDir);
					// Declare image handle
					$image = NULL;
					// Resize image
					if ($this->resize($image, self::thumbs($size, 'width'), self::thumbs($size, 'height')))
					{
						// Save image
						$this->save($image, $thumbsDir.'/'.basename($this->filename), $type);
					}
					// Destoy image
					@imagedestroy($image);
				}
			}
			else
			{
				// Loop through each thumb
				foreach (self::thumbs() as $size=> $thumb)
				{
					// Create individual thumb
					$this->createThumbs($size, $type);
				}
			}
		}

		/**
		 * Move image /and thumbs
		 * @param string $src Source
		 * @param string $dest Destination
		 */
		static function move($src, $dest, $includeThumbs=TRUE, $deleteAfter=TRUE)
		{
			// Make sure src exists
			if (!is_file($src)) return NULL;
			// Source dir
			$srcDir = dirname($src);
			// Source name
			$srcName = basename($src);
			// Destination dir
			$destDir = dirname($dest);
			// Destination name
			$destName = basename($dest);
			// Move main image
			if (!@copy($src, $dest)) return FALSE;
			// If to delete
			if ($deleteAfter) File::delete($src);
			// Move thumbs as well if set
			if ($includeThumbs)
			{
				// Loop through each dest
				foreach (self::thumbs() as $size=> $thumb)
				{
					// Check if dest thumbs dir exists, else create
					if (!is_dir($destDir.'/thumbs')) @mkdir($destDir.'/thumbs');
					// Source thumb
					$srcThumb = $srcDir.'/thumbs/'.$size.'/'.$srcName;
					// Check if source thumb exists
					if (is_file($srcThumb))
					{
						// Check if thumb dir exists, else create
						if (!is_dir($destDir.'/thumbs/'.$size)) @mkdir($destDir.'/thumbs/'.$size);
						// Move file
						@copy($srcThumb, $destDir.'/thumbs/'.$size.'/'.$destName);
						// If to delete
						if ($deleteAfter) File::delete($srcThumb);
					}
				}
			}
			// Return true
			return TRUE;
		}

		/**
		 * Get thumbs
		 */
		static function thumbs($thumb=NULL, $attr=NULL)
		{
			// Thumbnail sizes
			$thumbs = array(
				// 60 width
				'60w'=> array('width'=> 60, 'height'=> 0),
				// 60 height
				'60h'=> array('width'=> 0, 'height'=> 45),
				// 60 dimension
				'60d'=> array('width'=> 60, 'height'=> 45),
				// 50 square
				'60s'=> array('width'=> 60, 'height'=> 60),

				// 100 width
				'100w'=> array('width'=> 100, 'height'=> 0),
				// 100 height
				'100h'=> array('width'=> 0, 'height'=> 75),
				// 100 dimension
				'100d'=> array('width'=> 100, 'height'=> 75),
				// 100 square
				'100s'=> array('width'=> 100, 'height'=> 100),

				// 200 width
				'200w'=> array('width'=> 200, 'height'=> 0),
				// 200 height
				'200h'=> array('width'=> 0, 'height'=> 150),
				// 200 dimension
				'200d'=> array('width'=> 200, 'height'=> 150),
				// 200 square
				'200s'=> array('width'=> 200, 'height'=> 200),

				// 400 width
				'400w'=> array('width'=> 400, 'height'=> 0),
				// 400 height
				'400h'=> array('width'=> 0, 'height'=> 300),
				// 400 dimension
				'400d'=> array('width'=> 400, 'height'=> 300),
				// 400 square
				'400s'=> array('width'=> 400, 'height'=> 400),

				// 600 width
				'600w'=> array('width'=> 600, 'height'=> 0),
				// 600 height
				'600h'=> array('width'=> 0, 'height'=> 450),
				// 600 dimension
				'600d'=> array('width'=> 600, 'height'=> 450),
				// 600 square
				'600s'=> array('width'=> 600, 'height'=> 600)
			);
			// If there's thumb return
			if ($thumb !== NULL)
			{
				// If set
				if (isset($thumbs[$thumb]))
				{
					// If attr is set
					if ($attr !== NULL)
					{
						// If set
						if (isset($thumbs[$thumb][$attr]))
						{
							// Return
							return $thumbs[$thumb][$attr];
						}
						else
						{
							// Return NULL
							return NULL;
						}
					}
					else
					{
						// Return thumb
						return $thumbs[$thumb];
					}
				}
				else
				{
					// Return null
					return NULL;
				}
			}
			else
			{
				// Return all
				return $thumbs;
			}
		}

	}