<?php


	class Photo
	{

		/**
		 * Reference types
		 */
		const REF_ESTABLISHMENT = 1;
		const REF_PROFILE = 2;
		/**
		 * Status
		 */
		const STATUS_ACTIVE = 1;
		const STATUS_DELETED = 2;

		/**
		 * Get dir
		 * @param string $subdir Subdirectory
		 */
		static function dir()
		{
			// Return dir
			return File::dir('images');
		}

		/**
		 * Get root
		 * @param string $subdir Subdirectory
		 */
		static function root()
		{
			// Return root
			return File::root('images');
		}

		/**
		 * Official photo format
		 * (REF_TYPE|REF_ID_TIME_MICROTIME_COPIES.EXT)
		 * 100000002_143567_137691700190560_03.jpg
		 */

		/**
		 * Move a photo from temp file
		 * @param string $temp Temp source
		 * @param int $refId Reference Id
		 * @param int $ref Reference type
		 * @param int $ext Image file extension
		 * @return string Photo source filename
		 */
		static function move($temp, $refId, $ref=self::REF_ESTABLISHMENT, $ext='jpg')
		{
			// Set src
			$src = File::dir('temp').'/'.$temp;
			// Make sure temp exists
			if (!is_file($src)) return NULL;
			// Get date
			$date = date('Y-m-d');
			// Set dir
			$dir = self::dir().'/'.$date;
			// If dir doesn't exist, create
			if (!is_dir($dir)) @mkdir($dir);
			// Generate a safe filename
			$filename = $ref.str_pad($refId, 8, '0', STR_PAD_LEFT).'_'.date('His').'_'.str_pad(str_replace('.', '', microtime(TRUE)), 15, '0', STR_PAD_RIGHT).'_';
			// Set counter
			$counter = 1;
			// While filename exists
			while (is_file($dir.'/'.$filename.(str_pad($counter, 2, '0', STR_PAD_LEFT)).'.'.$ext))
			{
				// Increment counter
				$counter++;
			}
			// Set final name
			$filename .= ((str_pad($counter, 2, '0', STR_PAD_LEFT)).'.'.$ext);
			// Set dest
			$dest = $dir.'/'.$filename;
			// Move image file
			if (!Image::move($src, $dest, TRUE, FALSE)) return FALSE;
			// Return final filename
			return $date.'/'.$filename;
		}

		/**
		 * Get single photo
		 */
		static function get($id)
		{
			// Get single photo
			$getPhoto = Cl::$db->select('dd_photo', self::fields(), TRUE, array(
				array('c'=> 'ph_id', 't'=> 'i', 'v'=> $id)
			));
			// Return single photo
			return ($getPhoto['rows']>0)?$getPhoto['data']:NULL;
		}

		/**
		 * Get all photos in an album
		 */
		static function getAll($aid)
		{
			// Get photos
			$getPhotos = Cl::$db->select('dd_photo', self::fields(), FALSE, array(
				array('c'=> 'pa_id', 't'=> 'i', 'v'=> $aid)
			), 'ph_order ASC');
			// Return photos
			return ($getPhotos['rows']>0)?$getPhotos['data']:NULL;
		}

		/**
		 * Edit single photo
		 */
		static function set($data, $id)
		{
			// Set fields allowed for editing
			$fields = array(
				'title'=> 's',
				'order'=> 'i',
				'status'=> 'i',
				'aid'=> 'i'
			);
			// Check if there's data
			if ($data)
			{
				// Set data to edit
				$edit = array();
				// Loop through each field
				foreach ($fields as $field=> $type)
				{
					// Check if data field is set
					if (isset($data[$field]))
					{
						// Set data to edit
						$edit[] = array(
							'c'=> (($field=='aid')?'pa_id':('ph_'.$field)),
							't'=> $type,
							'v'=> $data[$field]
						);
					}
				}
				// If there's anything to edit
				if ($edit)
				{
					// Do edit
					$editPhoto = Cl::$db->update('dd_photo', $edit, array(
						array('c'=> 'ph_id', 't'=> 'i', 'v'=> $id)
					));
				}
			}
		}
		
		/**
		 * Edit photos in an album
		 */
		static function editAlbum($photos, $aid)
		{
			// Get album info
			$album = self::getAlbum($aid);
			// If there's no album, exit
			if (!$album) return NULL;
			// Check it there's title
			if (isset($photos['title']) && $photos['title'])
			{
				// Set order
				$order = 1;
				// Ids edited
				$ids = array();
				// Set default
				$default = 0;
				// Loop through each photo
				foreach ($photos['title'] as $i=> $title)
				{
					// Check if there's id
					if (isset($photos['id'][$i]) && ($photos['id'][$i]>0))
					{
						// Edit photo here
						self::set(array(
							'title'=> $title,
							'order'=> $order
						), $photos['id'][$i]);
						// Append to ids
						$ids[] = $photos['id'][$id];
						// If default
						if (isset($photos['default'][$i]) && $photos['default'][$i] && !$default) $default = $Photos['id'][$id];
						// Increment order
						$order++;
					}
					elseif (isset($photos['temp'][$i]) && $photos['temp'][$i])
					{
						// Move photo first
						$movePhoto = self::move($photos['temp'][$i], $album['ref_id'], $album['ref']);
						// If there's filename
						if ($movePhoto)
						{
							// Create photo
							$createPhoto = self::create($title, $movePhoto, $order, $aid);
							// Append to ids
							$ids[] = $createPhoto;
							// If default
							if (isset($photos['default'][$i]) && $photos['default'][$i] && !$default) $default = $createPhoto;
							// Increment order
							$order++;
						}
					}
				}
				// If there are ids
				if ($ids)
				{
					// If there's no default
					if (!$default) $default = $ids[0];
					// Set default photo
					self::setAlbum(array(
						'default_photo'=> $default
					), $aid);
					// Delete photos
					$deletePhotos = Cl::$db->query(
						'UPDATE dd_photo 
							SET ph_status = '.self::STATUS_DELETED.'
							WHERE pa_id = '.$aid.'
								AND ph_id NOT IN ('.implode(', ', $ids).')');
					// Return true
					return TRUE;
				}
				// Return false by default
				return FALSE;
			}
		}

		/**
		 * Create photo
		 * @param string $title Title
		 * @param string $src Source
		 * @param int $order Order number
		 * @param int $aid Album id
		 */
		static function create($title, $src, $order, $aid)
		{
			// Just insert
			$create = Cl::$db->insert('dd_photo', array(
				array('c'=> 'ph_title', 't'=> 's', 'v'=> $title),
				array('c'=> 'ph_src', 't'=> 's', 'v'=> $src),
				array('c'=> 'ph_date_created', 't'=> 'fn', 'v'=> 'utc_timestamp()'),
				array('c'=> 'ph_order', 't'=> 'i', 'v'=> $order),
				array('c'=> 'ph_status', 't'=> 'i', 'v'=> self::STATUS_ACTIVE),
				array('c'=> 'pa_id', 't'=> 'i', 'v'=> $aid)
			));
			// Return id
			return $create['last_id'];
		}

		/**
		 * Get album
		 */
		static function getAlbum($aid, $getPhotos=FALSE)
		{
			// Get album
			$getAlbum = Cl::$db->select('dd_photo_album', self::albumFields(), TRUE, array(
				array('c'=> 'pa_id', 't'=> 'i', 'v'=> $aid)
			));
			// If getPhotos
			if ($getPhotos)
			{
				// Load photos
				$getAlbum['data']['photos'] = self::getAll($aid);
			}
			// Return
			return ($getAlbum['rows']>0)?$getAlbum['data']:NULL;
		}

		/**
		 * Get albums
		 */
		static function getAlbums($refId, $ref=self::REF_ESTABLISHMENT, $getPhotos=FALSE)
		{
			// Get all albums
			$getAlbums = Cl::$db->select('dd_photo_album', self::albumFields(), FALSE, array(
				array('c'=> 'pa_ref', 't'=> 'i', 'v'=> $ref),
				array('c'=> 'pa_ref_id', 't'=> 'i', 'v'=> $refId)
			), 'pa_date_created ASC');
			// Set albums
			$albums = array();
			// Check if there are albums
			if ($getAlbums['rows']>0)
			{
				// Loop through each album
				foreach ($getAlbums['data'] as $album)
				{
					// If getPhotos, set to album
					if ($getPhotos)
					{
						// Set photos
						$album['photos'] = self::getAll($album['id']);
					}
					// Append to albums
					$albums[] = $album;
				}
			}
			// Return albums
			return $albums;
		}

		/**
		 * Edit album
		 */
		static function setAlbum($data, $aid)
		{
			// Set fields allowed for editing
			$fields = array(
				'title'=> 's',
				'description'=> 's',
				'default_photo'=> 'i'
			);
			// Check if there's data
			if ($data)
			{
				// Set data to edit
				$edit = array();
				// Loop through each field
				foreach ($fields as $field=> $type)
				{
					// Check if data field is set
					if (isset($data[$field]))
					{
						// Set data to edit
						$edit[] = array(
							'c'=> 'pa_'.$field,
							't'=> $type,
							'v'=> $data[$field]
						);
					}
				}
				// If there's anything to edit
				if ($edit)
				{
					// Do edit
					$editPhoto = Cl::$db->update('dd_photo_album', $edit, array(
						array('c'=> 'pa_id', 't'=> 'i', 'v'=> $aid)
					));
				}
			}
		}

		/**
		 * Create album
		 * @param string $title Album title
		 * @param int $refId Reference Id
		 * @param int $ref Reference type
		 */
		static function createAlbum($title, $refId, $ref=self::REF_ESTABLISHMENT)
		{
			// Just insert album
			$createAlbum = Cl::$db->insert('dd_photo_album', array(
				array('c'=> 'pa_title', 't'=> 's', 'v'=> $title),
				array('c'=> 'pa_date_created', 't'=> 'fn', 'v'=> 'utc_timestamp()'),
				array('c'=> 'pa_last_update', 't'=> 'fn', 'v'=> 'utc_timestamp()'),
				array('c'=> 'pa_ref', 't'=> 'i', 'v'=> $ref),
				array('c'=> 'pa_ref_id', 't'=> 'i', 'v'=> $refId)
			));
			// Return id
			return $createAlbum['last_id'];
		}

		/**
		 * Photo fields
		 */
		static function fields()
		{
			// Return fields
			return array(
				'id'=> 'ph_id id',
				'title'=> 'ph_title title',
				'src'=> 'ph_src src',
				'date_created'=> 'ph_date_created date_created',
				'order'=> 'ph_order `order`',
				'status'=> 'ph_status status',
				'aid'=> 'pa_id aid'
			);
		}

		/**
		 * Album fields
		 */
		static function albumFields()
		{
			// Return fields
			return array(
				'id'=> 'pa_id id',
				'title'=> 'pa_title title',
				'description'=> 'pa_description description',
				'default_photo'=> 'pa_default_photo default_photo',
				'date_created'=> 'pa_date_created date_created',
				'last_update'=> 'pa_last_update last_update',
				'ref'=> 'pa_ref ref',
				'ref_id'=> 'pa_ref_id ref_id'
			);
		}

	}