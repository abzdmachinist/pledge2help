<?php

	/**
	 * Template class
	 * @author Ronald Borla
	 * @copyright June 21, 2013
	 * @package Template
	 */

	class Template
	{

		/**
		 * Template types
		 */
		const TYPE_MAIL = 1;
		const TYPE_SMS = 2;

		/**
		 * Get template types
		 * @param int $type Type
		 * @return array Types
		 */
		static function getTypes($type=NULL)
		{
			// Set types
			$types = array(
				self::TYPE_MAIL => 'mail',
				self::TYPE_SMS	=> 'sms'
			);
			// Return
			if ($type)
			{
				// Return single type
				return isset($types[$type])?$types[$type]:NULL;
			}
			else
			{
				// Return types
				return $types;
			}
		}

		/**
		 * Load template
		 * @param int $type Template type
		 * @param string $name Template name
		 * @param array $params Parameters
		 * @return array Template values
		 */
		static function load($type, $name, $params=array())
		{
			// Set templates
			static $templates;
			// Set template to use
			$template = array();
			// If set
			if (isset($templates[$type][$name]))
			{
				// Set template from cache
				$template = $templates[$type][$name];
			}
			else
			{
				// Check if template exists
				if (is_file($templateFile=(PRIVATE_PATH.'/templates/'.self::getTypes($type).'/'.$name.'.php')))
				{
					// Load template from file
					$template = require($templateFile);
				}
				// Set template to cache
				$templates[$type][$name] = $template;
			}
			// If template found, proceed
			if ($template && $params && is_array($params))
			{
				// Loop through each parameter
				foreach ($params as $param=> $value)
				{
					// Check if param is set
					if (isset($template[$param]) && $value && is_array($value))
					{
						// Find and replace
						$toFind = array();
						$toReplace = array();
						// Loop through each replaceable
						foreach ($value as $find=> $replace)
						{
							$toFind[] = '%'.$find.'%';
							$toReplace[] = $replace;
						}
						// Do replace
						$template[$param] = str_replace($toFind, $toReplace, $template[$param]);
					}
				}
			}
			// Return template
			return $template;
		}
	}