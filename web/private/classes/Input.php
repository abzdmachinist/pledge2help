<?php

	/**
	 * Input class
	 * @author Ronald Borla
	 * @copyright May 30, 2013
	 * @package Input
	 */

	class Input
	{

		/**
		 * Check if magic quotes is on
		 */
		static function magicQuotes()
		{
			static $magicQuotes;
			// If there's magic quotes, return
			if ($magicQuotes !== NULL) return $magicQuotes;
			// Return with magic quotes
			return ($magicQuotes=get_magic_quotes_gpc());
		}

		/**
		 * Get $_GET vars
		 */
		static function get()
		{
			// Return with $_GET as src
			return self::extract(func_get_args(), $_GET);
		}

		/**
		 * Get $_POST vars
		 */
		static function post()
		{
			// Return with $_GET as src
			return self::extract(func_get_args(), $_POST);
		}

		/**
		 * Get $_REQUEST vars
		 */
		static function request()
		{
			// Return with $_GET as src
			return self::extract(func_get_args(), $_REQUEST);
		}

		/**
		 * Extract input from an array
		 */
		static function extract($args, $src, $dontStrip=FALSE)
		{
			if ($args)
			{
				// Loop through each argument
				foreach ($args as $arg)
				{
					// Check if arg is string
					if (is_string($arg))
					{
						if (!isset($src[$arg]))
						{
							// If there's nothing set, return with NULL
							return NULL;
						}
						// Change src to its child
						$src = &$src[$arg];
					}
				}
				if (self::magicQuotes() && !$dontStrip)
				{
					// If there's magic quots and told not to strip
					return stripslashes($src);
				}
				else
				{
					// Return with src by default
					return $src;
				}
			} 
			else 
			{
				// Return with src by default
				return $src ;
			}
		}
	}