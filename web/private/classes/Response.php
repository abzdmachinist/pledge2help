<?php

	/**
	 * Response class
	 * @author Ronald Borla
	 * @copyright June 04, 2013
	 * @package Response
	 */

	class Response
	{
		/**
		 * Response types
		 */
		const TYPE_DEFAULT = 0;
		const TYPE_SUCCESS = 1;
		const TYPE_WARNING = 2;
		const TYPE_FAILED = 3;
		/**
		 * Response type
		 */
		public $type = self::TYPE_DEFAULT;
		/**
		 * Responses
		 */
		public $responses = array();
		/**
		 * Extra variable
		 */
		public $var;

		/**
		 * Set message
		 */
		function set($name, $message, $type=self::TYPE_SUCCESS)
		{
			// Append to messages
			$this->responses[$name]['type'] = $type;
			// Responses
			$this->responses[$name]['messages'][] = $message;
			// Set type
			if ($type > $this->type)
			{
				$this->type = $type;
			}
			// Return self
			return $this;
		}

	}