<?php

	/**
	 * Database handler class
	 * @author Ronald Borla
	 * @copyright April 15, 2013
	 * @package Database
	 */

	class Database extends MySQLi
	{
		/**
		 * Select data
		 * @param string|array $tbl table name or array of table names
		 * @param (optional) string|array $select column or array of columns to select
		 * @param (optional) bool $single true to return only first result or return all
		 * @param (optional) array $condition array of conditions
		 * @param (optional) string $order order sql
		 * @param (optional) string $limit limit sql
		 * @return array (data, rows, success, errors)
		 */
		function select($tbl, $select='*', $single=TRUE, $condition=NULL, $order=NULL, $limit=NULL)
		{
			$return = array('data'=>null, 'rows'=>0, 'success'=>FALSE, 'errors'=>NULL );
			$where = $condition?$this->filter( $condition ):NULL;
			if (isset( $where['_errors'] ))
			{
				$return['errors']['condition'] = isset($where['_errors'])?$where['_errors']:NULL;
			}
			else
			{
				if ($where)
					$conditions = $this->get_filtered_data_pairs($where, 'condition');
				
				if (is_array($tbl))
					$tbl = implode(', ', $tbl);
				if (is_array($select))
					$select = implode(', ', $select);
				
				$sql = sprintf('SELECT %s FROM %s WHERE 1', $select, $tbl);
				if ($where)
					$sql .= (' AND '.implode(' AND ', $conditions));
				if ($order)
					$sql .= ' ORDER BY '.$order;
				if ($limit)
					$sql .= ' LIMIT '.$limit;
					
				$query = $this->query($sql);
				
				if ($query->num_rows>0)
				{
					while ($col=$query->fetch_assoc())
					{
						if ($single)
						{
							$return['data'] = $col; break;
						}
						else
							$return['data'][] = $col;
					}
				}
				
				$return['rows'] = $query->num_rows;
				$return['success'] = ($query->num_rows > 0);
				$return['query'] = $sql;
			}
			return $return;
		}
		
		/**
		 * Insert data
		 * @param string|array $tbl table name or array of table names
		 * @param array $data array of data
		 * @return array (last_id, success, errors)
		 */
		function insert($tbl, $data)
		{
			$return = array('last_id'=>0, 'success'=>FALSE, 'errors'=>NULL );
			$filter = $this->filter($data);
			if (!$filter) return $return;
			if (isset($filter['_errors']))
				$return['errors'] = $filter['_errors'];
			else
			{
				$pairs = $this->get_filtered_data_pairs($filter);
				
				$sql = sprintf('INSERT INTO %s (%s) VALUES (%s)', $tbl,
					implode(', ', $pairs['c']), implode(', ', $pairs['v']));
				
				$query = $this->query($sql);
				
				$return['success'] = TRUE;
				$return['last_id'] = $this->insert_id;
				$return['query'] = $sql;
			}
			return $return;
		}
		
		/**
		 * Update data
		 * @param string|array $tbl table name or array of table names
		 * @param (optional) array $data array of data
		 * @param (optional) array $condition array of conditions
		 * @return array (success, affected, query, errors)
		 */
		function update($tbl, $data, $condition=NULL)
		{
			$return = array('affected'=>0, 'success'=>FALSE, 'errors'=>NULL );
			$filter = $this->filter($data);
			$where = $condition?$this->filter($condition):NULL;
			if (!$filter) return $return;
			if (isset($filter['_errors']) || isset($where['_errors']))
			{
				$return['errors']['data'] = isset($filter['_errors'])?$filter['_errors']:NULL;
				$return['errors']['condition'] = isset($where['_errors'])?$where['_errors']:NULL;
			}
			else
			{
				$pairs = $this->get_filtered_data_pairs($filter, 'update');
				$conditions = $this->get_filtered_data_pairs($where, 'condition');
				
				$sql = sprintf('UPDATE %s SET %s WHERE 1 AND %s', $tbl,
					implode(', ', $pairs), !$conditions?'1':implode(' AND ', $conditions));
				
				$query = $this->query($sql);
				
				$return['affected'] = $this->affected_rows;
				$return['success'] = ($this->affected_rows > 0);
				$return['query'] = $sql;
			}
			return $return;
		}
		
		/**
		 * Delete data
		 * @param string|array $tbl table name or array of table names
		 * @param (optional) array $condition array of conditions
		 * @return array (success, affected query)
		 */
		function delete($tbl, $condition=NULL)
		{
			$return = array('affected'=>0, 'success'=>FALSE, 'errors'=>NULL );
			$where = $condition?$this->filter( $condition ):NULL;
			if (isset($where['_errors']))
				$return['errors']['condition'] = isset($where['_errors'])?$where['_errors']:NULL;
			else
			{
				$conditions = $this->get_filtered_data_pairs($where, 'condition');
				
				$sql = sprintf('DELETE FROM %s WHERE 1 AND %s', $tbl, 
					!$conditions?'1':implode(' AND ', $conditions ));
				
				$query = $this->query($sql);
				
				$return['affected'] = $this->affected_rows;
				$return['success'] = ($this->affected_rows > 0);
				$return['query'] = $sql;
			}
			return $return;
		}
		
		/**
		 * Prepare filtered data for pairing
		 *
		 * @param array $data array of data (type, value)
		 * @param (optional) string $for pairing for what method?
		 * @return array of filtered data, pairs c & v or plain array
		 */
		function get_filtered_data_pairs($data, $for='insert')
		{
			$pairs = null;
			if (is_array($data))
			{
				foreach ($data as $d)
				{
					$col = ''; $val = ''; $opt = '='; // Default logical operator
					
					if (isset($d[0])) $col = $d[0]; // Check first if index 0 is declared
					if (isset($d['c'])) $col = $d['c']; // Check first if 'c' is declared
					if (isset($d['col'])) $col = $d['col']; // Check if 'col' is declared
					if (isset($d['column'])) $col = $d['column']; // Prioritize 'column' if declared
					
					if (isset($d[1])) $val = $d[1]; // Check first if index 1 is declared
					if (isset($d['v'])) $val = $d['v']; // Check first if 'v' is declared
					if (isset($d['val'])) $val = $d['val']; // Check if 'val' is declared
					if (isset($d['value'])) $val = $d['value']; // Prioritize 'value' if declared
					
					if (isset($d[2])) $opt = $d[2]; // Check first if index 2 is declared
					if (isset($d['o'])) $opt = $d['o']; // Check first if 'o' is declared
					if (isset($d['opt'])) $opt = $d['opt']; // Check if 'opt' is declared
					if (isset($d['operator'])) $opt = $d['operator']; // Prioritize 'operator' if declared
					
					switch ($for)
					{
						case 'insert':
							$pairs['c'][] = $col;
							$pairs['v'][] = $val;
							break;
						case 'update':
							$pairs[] = $col . '=' . $val ;
							break;
						case 'condition':
							$pairs[] = $col . $opt . $val ;
							break;
					}
				}
			}
			return $pairs;
		}
		
		/**
		 * Filter data
		 * @param array $data array of data (type, value)
		 * @return array of filtered data, errors (if any)
		 */
		function filter($data)
		{
			$new_data = $data; $filter = NULL;
			if (is_array($data))
			{
				foreach ($data as $i=>$d)
				{
					$type = 's'; $value = ''; $vn = NULL;
					
					if (isset($d[0])) $type = $d[0]; // Check first if index 0 is declared
					if (isset($d['t'])) $type = $d['t']; // Check first if 't' is declared
					if (isset($d['type'])) $type = $d['type']; // Prioritize this index 'type'
					
					if (isset($d[1])){ $value = $d[1]; $vn = 1; } // Check first if index 1 is declared
					if (isset($d['v'])){ $value = $d['v']; $vn = 'v'; } // Check first if 'v' is declared
					if (isset($d['value'])){ $value = $d['value']; $vn = 'value'; } // Prioritize this index 'value'
					
					switch ($type)
					{
						case 'i': case 'int': case 'integer': // Integer types
							$filter = filter_var($value, FILTER_VALIDATE_INT);
							break;
						case 'f': case 'd': case 'dbl': case 'float': case 'double'; // Floating types
							$filter = filter_var($value, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
							break;
						case 's': case 'str': case 'string': // String types
							$filter = '"'. $this->escape_string($value) . '"';
							break;
						case 'fn': case 'function': $filter = $value; break; // If function, then proceed default
						default: $filter = FALSE; break;
					}
					$new_data[$i][$vn] = ($filter===false) ? NULL : $filter ;
					if ($new_data[$i][$vn] === NULL)
						$new_data['_errors'][] = $i;
				}
			}
			return $new_data;
		}
	}