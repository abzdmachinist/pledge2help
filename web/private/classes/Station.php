<?php

	/**
	 * Station class
	 * @author Ronald Borla
	 * @copyright November 23, 2013
	 * @package Station
	 */

	class Station
	{

		/**
		 * Project status
		 */
		const STATUS_INACTIVE = 0;
		const STATUS_ACTIVE = 1;
		const STATUS_SUSPENDED = 2;
		const STATUS_DELETED = 3;

		/**
		 * Get
		 */
		static function get($id)
		{
			// Get station 
			$getStation = Cl::$db->select('p2h_station', self::fields(), TRUE, array(
				array('c'=> 's_id', 't'=> 'i', 'v'=> $id)
			));
			// Return
			return ($getStation['rows'] > 0) ? $getStation['data'] : NULL;
		}

		/**
		 * Get all
		 */
		static function getAll($status=NULL)
		{
			// Set condition
			$cond = array();
			// Get stations
			if ($status !== NULL)
			{
				// Add condition
				$cond[] = array('c'=> 's_status', 't'=> 'i', 'v'=> $status);
			}
			// Get all
			$getAllStations = Cl::$db->select('p2h_station', self::fields(), FALSE, $cond);
			// return
			return ($getAllStations['rows'] > 0) ? $getAllStations['data'] : NULL;
		}

		/**
		 * User fields
		 */
		static function fields()
		{
			// Return fields
			return array(
				'id'					=> 's_id id', 
				'name'		=> 's_name name', 
				'description'					=> 's_description description', 
				'address'		=> 's_address address', 
				'lat'		=> 's_lat lat', 
				'lng'		=> 's_lng lng', 
				'zoom'		=> 's_zoom zoom', 
				'status'=> 's_status status',
				'prid'=> 'pr_id prid'
			);
		}
	}