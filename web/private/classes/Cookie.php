<?php

	/**
	 * Cookie handler
	 * @author Ronald Borla
	 * @copyright October 03, 2012
	 * @package Cookie
	 */
	 
	class Cookie 
	{
		
		/**
		 * Get cookie value
		 * @param string $name Cookie name
		 * @return mixed|null value (null if empty)
		 */
		static function get($name)
		{
			if (isset($_COOKIE[$name]))
			{
				return $_COOKIE[$name];
			}
			else
			{
				return NULL;
			}
		}
		
		/**
		 * Set cookie value
		 * @param string $name Cookie name
		 * @param mixed $value Cookie value
		 * @param (optional) int $expire Expiration length in seconds
		 */
		static function set($name, $value, $expire=2592000) //60*60*24*30; // 30 Days
		{
			@setcookie($name, $value, mktime()+$expire);
		}
		
		/**
		 * Clear cookie
		 * @param string $name Cookie name
		 */
		static function clear($name)
		{
			if (isset($_COOKIE[$name]))
			{
				@setcookie($name, '', 0);
			}
		}
		
	}