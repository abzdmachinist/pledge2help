<?php

	/**
	 * Address handler class
	 * @author Ronald Borla
	 * @copyright August 17, 2013
	 * @package Address
	 */

	class Address
	{

		/**
		 * Reference type
		 */
		const REF_ESTABLISHMENT = 1;
		const REF_PROFILE 			= 2;

		/**
		 * Default x, y, z
		 */
		const DEFAULT_X = 7.077171743017243; // Latitude
		const DEFAULT_Y = 125.61252636572272; // Longitude
		const DEFAULY_Z = 9; // Zoom level

		/**
		 * Create address
		 */
		static function create($address, $description, $refId, $ref=self::REF_ESTABLISHMENT)
		{
			// Set fields
			$fields = self::fields();
			// Remove id
			if (isset($fields['id'])) unset($fields['id']);
			// Set description
			$address['description'] = $description;
			// Set ref
			$address['ref'] = $ref;
			// Set refId
			$address['ref_id'] = $refId;
			// If there's no x, y, or z, set default
			if (!isset($address['x'])) $address['x'] = self::DEFAULT_X;
			if (!isset($address['y'])) $address['y'] = self::DEFAULT_Y;
			if (!isset($address['z'])) $address['z'] = self::DEFAULT_Z;
			// Set data to insert
			$data = array();
			// Loop through fields
			foreach ($fields as $field=> $column)
			{
				// Set type
				$type = 's';
				// If int
				if (in_array($field, array('z', 'ref', 'ref_id'))) $type = 'i';
				// If double
				elseif (in_array($field, array('x', 'y'))) $type = 'd';
				// If address is set
				if (isset($address[$field]))
				{
					// Append to data
					$data[] = array(
						'c'=> 'a_'.$field,
						't'=> $type,
						'v'=> $address[$field]
					);
				}
			}
			// Create address
			$createAddress = Cl::$db->insert('dd_address', $data);
			// Return id
			return $createAddress['last_id'];
		}

		/**
		 * Get address
		 */
		static function get($id)
		{
			// Get address
			$getAddress = Cl::$db->select('dd_address', self::fields(), TRUE, array(
				array('c'=> 'a_id', 't'=> 'i', 'v'=> $id)
			));
			// Return
			return ($getAddress['rows']>0)?$getAddress['data']:NULL;
		}

		/**
		 * Get all address
		 */
		static function getAll($refId, $ref=self::REF_ESTABLISHMENT)
		{
			// Get all
			$getAllAddress = Cl::$db->select('dd_address', self::fields(), FALSE, array(
				array('c'=> 'a_ref', 't'=> 'i', 'v'=> $ref),
				array('c'=> 'a_ref_id', 't'=> 'i', 'v'=> $refId)
			));
			// Return all
			return ($getAllAddress['rows']>0)?$getAllAddress['data']:NULL;
		}

		/**
		 * Join address info
		 * @param array $address Address info
		 * @return string Address
		 */
		static function join($address)
		{
			// If array
			if (is_array($address) && $address)
			{
				// Address
				$addressString = '';
				// Loop through cols
				foreach (array_reverse(self::cols()) as $col=> $title)
				{
					// If set
					if (isset($address[$col]) && $address[$col])
					{
						// If there's address
						if ($addressString)
						{
							// If not zip, append comma
							if ($col != 'zip') $addressString .= ',';
							// Append space
							$addressString .= ' ';
						}
						// Append to string
						$addressString .= $address[$col];
					}
				}
				// Return string
				return $addressString;
			}
		}

		/**
		 * Get cols
		 */
		static function cols($col=NULL)
		{
			// Set cols
			$cols = array(
				'province'		=> 'Province',
				'zip'					=> 'Zip Code',
				'municipality'=> 'Municipality',
				'district'		=> 'District',
				'barangay'		=> 'Barangay',
				'village'			=> 'Village',
				'street'			=> 'Street'
			);
			// If there's col
			if ($col !== NULL)
			{
				// Check if set
				if (isset($cols[$col]))
				{
					// Return col
					return $cols[$col];
				}
				else
				{
					// Return null
					return NULL;
				}
			}
			else
			{
				// Return cols
				return $cols;
			}
		}

		/**
		 * Get fields
		 */
		static function fields()
		{
			// Return fields
			return array(
				'id'					=> 'a_id id', 
				'description'	=> 'a_description description',
				'province'		=> 'a_province province', 
				'municipality'=> 'a_municipality', 
				'zip'					=> 'a_zip zip', 
				'district'		=> 'a_district district',
				'barangay'		=> 'a_barangay barangay',
				'village'			=> 'a_village village',
				'street'			=> 'a_street street',
				'hint'				=> 'a_hint hint',
				'x'						=> 'a_x x',
				'y'						=> 'a_y y',
				'z'						=> 'a_z z'
			);
		}

	}