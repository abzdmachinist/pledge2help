<?php

	/**
	 * Establishment class
	 * @author Ronald Borla
	 * @copyright July 16, 2013
	 * @package Establishment
	 */

	class Establishment
	{

		/**
		 * Establishment status
		 */
		const STATUS_INACTIVE 	= 0;
		const STATUS_ACTIVE 		= 1;
		const STATUS_PENDING		= 2;
		const STATUS_SUSPENDED 	= 3;
		const STATUS_DELETED		= 4;

		/**
		 * Sections
		 */
		const SECTION_INFO 			= 1;
		const SECTION_PRODUCTS	= 2;
		const SECTION_PHOTOS 		= 4;
		const SECTION_REVIEWS 	= 8;
		const SECTION_CONTACT 	= 16;

		/**
		 * Establishment info
		 */
		public $info;

		/**
		 * Construct
		 */
		function __construct($id=NULL)
		{
			// If there's id, load
			if ($id) $this->load($id);
		}

		/**
		 * Get override
		 */
		function __get($name)
		{
			// Set cache
			static $cache;
			// If set
			if (isset($cache[$name])) return $cache[$name];
			// Switch name
			switch ($name)
			{
				// Get sections
				// Get info
				// Get permalink
				case 'permalink':
					// Return permalink
					return $cache[$name] = Page::url('establishment', array($this->info['permalink']), TRUE);
					// Break
					break;
				// If getting categories
				case 'categories':
					// Return categories
					return $cache[$name] = $this->getCategories($this->info['id']);
					// Break
					break;
				// Address
				case 'address':
					// Return address
					return $cache[$name] = Address::getAll($this->info['id']);
					// Break
					break;
				// Default address
				case 'defaultAddress':
					// Return default address
					return $cache[$name] = Address::get($this->info['default_address']);
					// Break
					break;
				// Get contacts
				case 'contacts':
					// Return contacts
					return $cache[$name] = Contact::get($this->info['id']);
					// Break
					break;
				// Get default photo
				case 'defaultPhoto':
					// Return default photo
					return $cache[$name] = Photo::get($this->info['default_photo']);
					// Break
					break;
				// Get photo albums
				case 'photoAlbums':
					// Return photoAlbums
					return $cache[$name] = Photo::getAlbums($this->info['id'], Photo::REF_ESTABLISHMENT, TRUE);
					// Break
					break;
				// Get photos
				case 'photos':
					// Get default photo
					$defaultPhoto = $this->defaultPhoto;
					// Return default photos or nothing
					return $cache[$name] = ($defaultPhoto ? Photo::getAlbum($defaultPhoto['aid'], TRUE) : NULL);
					// Break
					break;
				// Get sections
				case 'sections':
					// Get this section
					$thisSections = $this->info['sections'];
					// Get sections
					$getSections = self::getSections();
					// Set sections
					$sections = array();
					// Loop through each section
					foreach ($getSections as $section=> $sectionInfo)
					{
						// If it's included
						if ($thisSections & $section)
						{
							// Switch section
							switch ($section)
							{
								// Photos
								case self::SECTION_PHOTOS:
									// Check if there are no photos
									if (!$this->photos) break;
								default:
									// Append to sections
									$sections[$section] = $sectionInfo;
									break;
							}
						}
					}
					// Return sections
					return $cache[$name] = $sections;
					// Break
					break;
				// Default
				default:
					// Check if info is required
					if (isset($this->info[$name])) return $this->info[$name];
					// Break
					break;
			}
		}

		/**
		 * Load establishment
		 */
		function load($id)
		{
			// Get establishment and put to info
			$this->info = is_numeric($id)?$this->get($id):$this->getByPermalink($id);
		}

		/**
		 * Get statuses
		 * @param int $status Status (optional)
		 * @return array Statuses
		 */
		static function getStatuses($status=NULL)
		{
			$statuses = array(
				self::STATUS_INACTIVE		=> 'Inactive',
				self::STATUS_ACTIVE			=> 'Active',
				self::STATUS_PENDING		=> 'Pending',
				self::STATUS_SUSPENDED	=> 'Suspended',
				self::STATUS_DELETED		=> 'Deleted'
			);
			// If status is set, return with it
			if ($status)
			{
				return isset($statuses[$status])?$statuses[$status]:NULL;
			}
			else
			{
				// Return with all statuses
				return $statuses;
			}
		}

		/**
		 * Get sections
		 * @param int $section Section (optional)
		 * @return array Statuses
		 */
		static function getSections($section=NULL)
		{
			$sections = array(
				self::SECTION_INFO		=> 'Basic Information',
				self::SECTION_PHOTOS	=> 'Photo Albums',
				self::SECTION_REVIEWS	=> 'Reviews',
				self::SECTION_CONTACT	=> 'Contact Us'
			);
			// If section is set, return with it
			if ($section)
			{
				return isset($sections[$section])?$sections[$section]:NULL;
			}
			else
			{
				// Return with all sections
				return $sections;
			}
		}

		/**
		 * Create establishment
		 * @param int $uid User id
		 * @return int Establishment id
		 */
		static function create($uid=0, $status=self::STATUS_PENDING)
		{
			// Create establishment
			$createEstablishment = Cl::$db->insert('dd_establishment', array(
				array('c'=> 'e_date_created', 't'=> 'fn', 'v'=> 'utc_timestamp()'),
				array('c'=> 'e_last_update', 't'=> 'fn', 'v'=> 'utc_timestamp()'),
				array('c'=> 'e_status', 't'=> 'i', 'v'=> $status),
				array('c'=> 'e_sections', 't'=> 'i', 'v'=> (self::SECTION_INFO + self::SECTION_PHOTOS + self::SECTION_REVIEWS + self::SECTION_CONTACT)),
				array('c'=> 'u_id', 't'=> 'i', 'v'=> $uid)
			));
			// Return id
			return $createEstablishment['last_id'];
		}

		/**
		 * Get establishment
		 */
		static function get($eid)
		{
			// Get establishment
			$getEstablishment = Cl::$db->select('dd_establishment', self::fields(), TRUE, array(
				array('c'=> 'e_id', 't'=> 'i', 'v'=> $eid)
			));
			// Return data
			return ($getEstablishment['rows']>0)?$getEstablishment['data']:NULL;
		}

		/**
		 * Get establishment by permalink
		 * @param string $permalink Permalink
		 * @return array Establishment data
		 */
		static function getByPermalink($permalink)
		{
			// Find establishment by permalink
			$getEstablishment = Cl::$db->select('dd_establishment', self::fields(), TRUE, array(
				array('c'=> 'e_permalink_lcase', 't'=> 's', 'v'=> Str::lcase($permalink))
			), NULL, '0, 1');
			// Return establishment
			return ($getEstablishment['rows']>0)?$getEstablishment['data']:NULL;
		}

		/**
		 * Set
		 * @param string $data Data
		 * @param int $eid Establishment id
		 */
		static function set($data, $eid)
		{
			// If there's no data, exit
			if (!$data) return NULL;
			// Set valid fields
			$validFields = array(
				'name'						=> 's', 
				'description'			=> 's', 
				'default_address'	=> 'i',
				'default_photo'		=> 'i', 
				'search_tag'			=> 's'
			);
			// Set update field
			$update = array();
			// Loop through each valid field
			foreach ($validFields as $field=> $type)
			{
				// Check if set
				if (isset($data[$field]))
				{
					// Append to update
					$update[] = array(
						'c'=> 'e_'.$field,
						't'=> $type,
						'v'=> $data[$field]
					);
				}
			}
			// If there's update
			if ($update)
			{
				// Do update
				$updateEstablishment = Cl::$db->update('dd_establishment', $update, array(
					array('c'=> 'e_id', 't'=> 'i', 'v'=> $eid)
				));
				// Return
				return $updateEstablishment['success'];
			}
			// Return false
			return FALSE;
		}

		/**
		 * Set permalink
		 * @param string $name Raw name
		 * @param int $eid Establishment id
		 */
		static function setPermalink($name, $eid)
		{
			// Get establishment
			$establishment = self::get($eid);
			// If there's nothing, exit
			if (!$establishment) return NULL;
			// Generate permalink
			$tempPermalink = Str::unicodeToAscii(trim($name));
			// Remove all characters that are meant to be empty
			$tempPermalink = str_replace(array('\'', '`', '´', '’'), '', $tempPermalink);
			// Replace invalid characters with space
			$tempPermalink = preg_replace('/[^a-zA-Z0-9]/', ' ', $tempPermalink);
			// Explode by space
			$arrPermalink = explode(' ', $tempPermalink);
			// Set new permalink
			$permalink = '';
			// If there's no permalink, exit
			if (!$arrPermalink) return NULL;
			// Loop through each permalink
			foreach ($arrPermalink as $word)
			{
				// Trim word
				$word = trim($word);
				// If there's no word, skip
				if (!$word) continue;
				// Append to new permalink
				$permalink .= ucfirst(Str::lcase($word));
			}
			// If there's no permalink, quit
			if (!$permalink) return NULL;
			// Convert to lcase
			$lcase = Str::lcase($permalink);
			// If lcase is the same as current safe
			if ($lcase == $establishment['permalink_safe'])
			{
				// Get length of lcase
				$len = Str::len($lcase);
				// Check if permalink is not the same with current
				if (Str::subs($establishment['permalink'], 0, $len) != $permalink)
				{
					// Get suffix
					$suffix = Str::subs($establishment['permalink'], $len);
					// Append suffix to permalink
					$permalink .= $suffix;
					// Update permalink
					$updatePermalink = Cl::$db->update('dd_establishment', array(
						array('c'=> 'e_permalink', 't'=> 's', 'v'=> $permalink)
					), array(
						array('c'=> 'e_id', 't'=> 'i', 'v'=> $eid)
					));
				}
				// Exit
				return TRUE;
			}
			// Set counter
			$counter = 1;
			// Check until not found
			while (1)
			{
				// Check from list
				$getEstablishment = Cl::$db->select('dd_establishment', 'e_id id', TRUE, array(
					array('c'=> 'e_permalink_lcase', 't'=> 's', 'v'=> $lcase.(($counter>1)?($lcase.$counter):$lcase)),
					array('c'=> 'e_id', 't'=> 'i', 'v'=> $eid, 'o'=> '<>')
				), NULL, '0, 1');
				// If not found, quit
				if ($getEstablishment['rows']==0) break;
				// Increment counter
				$counter++;
			}
			// Append counter to permalink if it's greater than 1
			if ($counter > 1) $permalink .= $counter;
			// Update permalinks
			$updatePermalinks = Cl::$db->update('dd_establishment', array(
				array('c'=> 'e_permalink', 't'=> 's', 'v'=> $permalink),
				array('c'=> 'e_permalink_lcase', 't'=> 's', 'v'=> Str::lcase($permalink)),
				array('c'=> 'e_permalink_safe', 't'=> 's', 'v'=> $lcase)
			), array(
				array('c'=> 'e_id', 't'=> 'i', 'v'=> $eid)
			));
			// Return true
			return TRUE;
		}

		/**
		 * Set status
		 * @param int $status Status
		 * @param int $eid Establishment id
		 */
		static function setStatus($status, $eid)
		{
			// Make sure status is valid
			if (!self::getStatuses($status)) return NULL;
			// Set status
			$setStatus = Cl::$db->update('dd_establishment', array(
				array('c'=> 'e_status', 't'=> 'i', 'v'=> $status)
			), array(
				array('c'=> 'e_id', 't'=> 'i', 'v'=> $eid)
			));
			// Return
			return $setStatus['success'];
		}

		/**
		 * Get categories
		 */
		static function getCategories($eid)
		{
			// Set categories
			$categories = array();
			// Fetch categoires
			$getCategories = Cl::$db->select('dd_category_list', 'cg_id id', FALSE, array(
				array('c'=> 'e_id', 't'=> 'i', 'v'=> $eid)
			), 'cgl_order ASC');
			// Loop through each category
			foreach ($getCategories['data'] as $i=> $category)
			{
				// Get single category, and append to categories
				$categories[] = self::getCategoryById($category['id']);
			}
			// Return categories
			return $categories;
		}

		/**
		 * Edit categories
		 */
		static function editCategories($categories, $eid)
		{
			// Final categories
			$finalCategories = array();
			// Check if there's categories
			if ($categories)
			{
				// Loop through each category
				foreach ($categories as $cgId)
				{
					// If category is valid
					if (self::getCategoryById($cgId))
					{
						// Append to final categories
						$finalCategories[] = $cgId;
					}
				}
			}
			// If there are final categories
			if ($finalCategories)
			{
				// Delete categories
				$deleteCategories = Cl::$db->delete('dd_category_list', array(
					array('c'=> 'e_id', 't'=> 'i', 'v'=> $eid)
				));
				// Loop through each category
				foreach ($finalCategories as $i=> $cgId)
				{
					// Insert
					$insertCategory = Cl::$db->insert('dd_category_list', array(
						array('c'=> 'cgl_order', 't'=> 'i', 'v'=> ($i+1)),
						array('c'=> 'cg_id', 't'=> 'i', 'v'=> $cgId),
						array('c'=> 'e_id', 't'=> 'i', 'v'=> $eid)
					));
				}
			}
			// Return
			return $finalCategories?TRUE:FALSE;
		}

		/**
		 * Get category by ID
		 * @param int $cgid Category ID
		 * @return array Category
		 */
		static function getCategoryById($cgid)
		{
			// Get category
			$getCategoryById = Cl::$db->select('dd_category', self::categoryFields(), TRUE, array(
				array('c'=> 'cg_id', 't'=> 'i', 'v'=> $cgid)
			));
			// Return
			return ($getCategoryById['rows']>0)?$getCategoryById['data']:NULL;
		}

		/**
		 * Get vCard
		 * @return string vCard
		 */
		static function getVCard()
		{
			
		}

		/**
		 * Establishment fields
		 */
		static function fields()
		{
			// Return fields
			return array(
				'id'							=> 'e_id id',
				'name'						=> 'e_name name',
				'permalink'				=> 'e_permalink permalink',
				'permalink_safe'	=> 'e_permalink_safe permalink_safe',
				'description'			=> 'e_description description',
				'default_address'	=> 'e_default_address default_address',
				'default_photo'		=> 'e_default_photo default_photo',
				'date_created'		=> 'e_date_created date_created',
				'last_update'			=> 'e_last_update last_update',
				'status'					=> 'e_status status',
				'search_tag'			=> 'e_search_tag search_tag',
				'sections'				=> 'e_sections sections',
				'uid'							=> 'u_id uid'
			);
		}

		/**
		 * Category fields
		 */
		static function categoryFields()
		{
			// Return fields
			return array(
				'id'					=> 'cg_id id', 
				'name'				=> 'cg_name name', 
				'description'	=> 'cg_description description', 
				'search_tag'	=> 'cg_search_tag search_tag', 
				'rank'				=> 'cg_rank rank', 
				'parent'			=> 'cg_parent parent'
			);
		}
	}