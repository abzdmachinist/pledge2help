<?php

	/**
	 * Davao Directory Environment
	 * @author Ronald Borla
	 * @copyright April 15, 2013
	 */

	// Time start
	define('TIME_START', microtime(true));
	// Memory start
	define('MEM_START', memory_get_usage());
	// Private path directory
	define('PRIVATE_PATH', rtrim(dirname(__FILE__), '/\\'));
	// Public path directory
	define('PUBLIC_PATH', rtrim(dirname(dirname(__FILE__)), '/\\').'/public');
	// Set subdir
	define('SUB_DIR', "/pledge2help/android/assets/www/public");
	// Domain name
	define('DOMAIN', $_SERVER['HTTP_HOST'] . SUB_DIR);
	// Is https
	define('HTTPS', isset($_SERVER['HTTPS']));
	// Site root
	define('SITE_ROOT', (HTTPS?'https://':'http://').DOMAIN);
	
	// Set default timezone
	date_default_timezone_set('UTC');
	// Set to display errors
	// ini_set('display_errors', 1); 
	// error_reporting(E_ALL);

	// Set time limit
	set_time_limit(300);

	/**
	 * Auto load class function
	 * @param string $class Class name
	 */
	function loadClass($class)
	{
		// Load class
		require(PRIVATE_PATH.'/classes/'.ucwords($class).'.php');
	}
	
	// Set class autoload
	spl_autoload_register('loadClass');
	// Initialize controller
	Controller::init();