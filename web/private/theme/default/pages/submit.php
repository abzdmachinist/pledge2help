<?php

  /**
   * Default theme Submit page
   * @author Ronald Borla
   * @copyright July 08, 2013
   */

  // Get submit step
  $step = TDF::getSubmitStep();
  // If step is 0, then page error
  if (!$step) return FALSE;

  // Load theme data
  TDF::loadData();
  // Import data
  TDF::importSubmit(Input::get('import'));

  // If previous step doesn't have data, redirect to first step
  if ($step>1 && !isset(TDF::$data['s'][$step-1])) Page::go('submit');

  // Initialize actions
  Action::init();

  // If step 5, include javascript files
  if ($step==5)
  {
    // Insert script file
    Theme::script('text/javascript', Theme::$root.'/script/jquery-file-upload/jquery.iframe-transport.js');
    Theme::script('text/javascript', Theme::$root.'/script/jquery-file-upload/jquery.fileupload.js');
  }
  // If step 6, include jQuery cycle plugin
  if ($step==6)
  {
    // Insert script file
    Theme::script('text/javascript', Theme::$root.'/script/jquery.cycle.all.js');
  }
  // Insert script for each page
  Theme::script('text/javascript', NULL, '_core.onInit(function(){_core.establishment.submit.step'.$step.'.init();});');

  // Set login title
  Theme::title('Davao Directory - Submit');
  // Load header
  Theme::load('header');
  
?>

    <div id="middle-wrap">
      <div id="middle"><?php 
        // Load search widget
        Theme::load('widget-search'); 
        ?>
        
        <div id="tab-wrap"><?php
          // Load tab for user
          Theme::load('tab-user');
          ?>

          <div id="tab-body"><?php
            // Load step
            Theme::load('submit-step-'.$step);
            ?>

          </div>
        </div>
      </div>
    </div><?php
  
  // Load footer
  Theme::load('footer');