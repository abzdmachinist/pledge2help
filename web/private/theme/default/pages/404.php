<?php

	/**
	 * Default theme error page
	 * @author Ronald Borla
	 * @copyright April 15, 2013
	 */

	// Set header
	header('HTTP/1.0 404 Not Found');
	header('Status: 404 Not Found');

	// Set login title
	Theme::title('Davao Directory - Error');
	// Load header
	Theme::load('header');
	
?>

    <div id="middle-wrap">
      <div id="middle"><?php 
      	// Load search widget
      	Theme::load('widget-search'); 
        ?>
        
        <div id="tab-wrap"><?php
        	// Load tab for user
        	Theme::load('tab-user');
          ?>

          <div id="tab-body">
            <div id="tab">
              <div class="head">
                <h1>Error: Page not found</h1>
                <div>
                  <p>The page you are looking for does not exist</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div><?php
	
	// Load footer
	Theme::load('footer');