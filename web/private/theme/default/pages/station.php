<?php

	/**
	 * Station page
	 * @author Ronald Borla
	 * @copyright November 24, 2013
	 * @package station
	 */

  // Initialize actions
  Action::init();

	// Get station ID
	$stationId = Page::$wildcards[0];
	// Check if project exists
	$station = Station::get($stationId);

	// If there's no user and station
	if (!$station || !User::$loggedIn)
	{
		// Go to error page
		Page::go('error');
	}
	// Set project
	$project = Project::get($station['prid']);
	// Check if project
	if (!$project || ($project['uid'] != User::$loggedIn['id']))
	{
		// Go to error page
		Page::go('error');
	}

	