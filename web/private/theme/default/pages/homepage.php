<?php

	/**
	 * Default homepage
	 * @author Ronald Borla
	 * @copyright April 15, 2013
	 */

  // Set javascript
  Theme::script('text/javascript', NULL, '_core.onInit(function(){_core.hoverFade(\'#splash .search\', {\'opacity\':0.9,\'speed\':400});});');
	
	// Set homepage title
	Theme::title('Pledge2Help');
	// Load header
	Theme::load('header');
	

	// if logged in
	if (User::$loggedIn)
	{
		// If ordinary member
		if (User::is(User::PRIVILEGE_ORGANIZATION, User::$loggedIn['privilege']))
		{

			/**
			 * User is organization
			 */
			?>




			<?php
		}
		else
		{

			/**
			 * User is regular
			 */
			$allStations = Station::getAll();

			?>
<script type="text/javascript">
	var stations = <?php echo json_encode($allStations); ?>;
	// Set
	$(document).ready(function(){
    // Initialize google map
    var gmapId = gmap.init($('.gmap')[0]);
    // If there's id
    if (gmapId>=0){
    	// loop through stations
    	for (var i in stations){
	      // Add marker
	      var markerId = gmap.mark(gmapId, stations[i]['lat'], stations[i]['lng'], stations[i]['name']);
    	}
    }
	});
</script>
<div class="gmap" style="width: 400px; height: 300px; border: 3px white solid; box-shadow: 1px 1px 2px #888;"></div>
			<?php

			if ($allStations)
			{
				?>
				<div id="list">
				<?php
				foreach ($allStations as $station)
				{

				}
				?>
				<?php
			}
			?>




			<?php
		}
	}

	// Load footer
	Theme::load('footer');