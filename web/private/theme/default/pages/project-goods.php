<?php

	/**
	 * Station goods page
	 * @author Ronald Borla
	 * @copyright November 24, 2013
	 * @package station
	 */

  // Initialize actions
  Action::init();

	// Get project ID
	$projectId = Page::$wildcards[0];
	// Check if project exists
	$project = Project::get($projectId);

	// If there's no project
	if (!$project || !User::$loggedIn || (User::$loggedIn['id'] != $project['uid']))
	{
		// Go to error page
		Page::go('error');
	}

  // Set register title
  Theme::title('Pledge2Help - Add Goods');
  // Load header
  Theme::load('header');

?>

<script type="text/javascript">
	$(document).ready(function(){

		$('button[name="btn_add"]').click(function(){
			// Add
			addGoods();
		});
		// ADd
		addGoods();
	});

	function addGoods(){
		$('#goods').append(
			'<div>'+
				'<input name="description[]" type="text" placeholder="Description" /> '+
				'<input name="quantity[]" type="text" placeholder="Quantity" /> '+
				'<input name="unit[]" type="text" placeholder="Unit" /> '+
				'<a href="#" onclick="$(this).parent().remove(); return false;">[x]</a>'+
			'</div>')
	}
</script>

<?php Action::begin('project.goods', Page::url('project-goods', array($projectId), TRUE)); ?>

<?php Action::button('btn_add', 'button', 'Add Goods'); ?>

<div id="goods"></div>

<?php Action::button('btn_submit', 'submit', 'Submit Goods'); ?>

<?php Action::end(); ?>

<?php

  // Load footer
  Theme::load('footer');