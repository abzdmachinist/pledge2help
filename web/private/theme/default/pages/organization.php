<?php

	/**
	 * Default theme Register page
	 * @author Ronald Borla
	 * @copyright November 23, 2013
	 */
  
  // Initialize actions
  Action::init();
	 
  // If user is already logged in, go to profile page
  //if (User::$loggedIn) Page::go('profile');

	// Set register title
	Theme::title('Pledge2Help - Organization Registration');
	// Load header
	Theme::load('header');
	
?>

<?php Action::begin('user.register', Page::url('organization', array(), TRUE).((Input::request('next')!==NULL)?('?next='.urlencode(Input::request('next'))):'')); ?>

<div>
  <span>Organization</span> <?php Action::input('name', 'text', Action::value('name'), array('placeholder'=> 'Organization Name')); ?>
  <?php Action::response('name', array(Response::TYPE_DEFAULT=> ''), Action::OPTION_OVERWRITE_AND_RETURN); ?>
</div>
<div>
  <span>Email Address</span> <?php Action::input('email', 'text', Action::value('email'),array('placeholder'=> 'Email Address')); ?>
  <?php Action::response('email', array(Response::TYPE_DEFAULT=> ''), Action::OPTION_OVERWRITE_AND_RETURN); ?>
</div>
<div>
  <span>Password</span> <?php Action::input('password', 'password'); ?>
  <?php Action::response('password', array(Response::TYPE_DEFAULT=> ''), Action::OPTION_OVERWRITE_AND_RETURN); ?>
</div>
<?php Action::button('btn_register', 'submit', 'Register'); ?>
<?php Action::input('is_org', 'hidden', '1'); ?>

<?php Action::end(); ?>

<?php
	
	// Load footer
	Theme::load('footer');