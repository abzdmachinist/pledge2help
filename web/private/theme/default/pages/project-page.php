<?php

	/**
	 * Project page
	 * @author Ronald Borla
	 * @copyright November 23, 2013
	 * @package project-page
	 */

	// Get project ID
	$projectId = Page::$wildcards[0];
	// Check if project exists
	$project = Project::get($projectId);

	// If there's no project
	if (!$project)
	{
		// Go to error page
		Page::go('error');
	}

	$stations = Project::getStations($projectId);

  // Set register title
  Theme::title('Pledge2Help - '.$project['name']);
  // Load header
  Theme::load('header');

?>
<script type="text/javascript">
	var stations = <?php echo json_encode($stations); ?>;
	$(document).ready(function(){
    // Initialize google map
    var gmapId = gmap.init($('.gmap')[0]);
    // If there's id
    if (gmapId>=0){
    	// loop through stations
    	for (var i in stations){
	      // Add marker
	      var markerId = gmap.mark(gmapId, stations[i]['lat'], stations[i]['lng'], stations[i]['name']);
    	}
    }
    // Set zoom
    gmap.zoom(gmapId, 10);
  });
</script>
<h1><?php Str::p($project['name']); ?></h1>
<h4><?php Str::p($project['description']); ?></h4>
<div class="gmap" style="width: 400px; height: 300px; border: 3px white solid; box-shadow: 1px 1px 2px #888;"></div>
<?php

  // Load footer
  Theme::load('footer');