<?php

	/**
	 * Establishment Basic information section
	 * @author Ronald Borla
	 * @copyright September 22, 2013
	 * @package establishment.section.info
	 */

	// Retrieve est
	global $est;

                // Check if there are photos
                if ($est->photos)
                {
                  // Print photos
                  ?>

                  <div class="element photos">
                    <div class="slideshow"><?php
                      // Loop through each photo
                      foreach ($est->photos['photos'] as $photo)
                      {
                      ?>

                      <div title="<?php Str::p($photo['title']); ?>" class="slide<?php if ($est->photos['default_photo']==$photo['id']) echo ' active'; ?>">
                        <img src="<?php Image::thumb(File::root('images').'/'.$photo['src'], '400d'); ?>" /><?php
                        if ($photo['title'])
                        {
                        ?>

                        <div class="title"><?php Str::p(Str::shorten($photo['title'], 60)); ?></div><?php
                        }
                        ?>

                      </div><?php
                      }
                    ?>

                    </div>
                  </div><?php
                }

                // If there's description
                if ($est->description)
                {
                  // Print description
                  ?>

                  <div class="element description">
                    <?php Str::essay($est->description); ?>

                  </div><?php
                }

                // If there are contacts
                if ($est->contacts)
                {
                  // Print contacts
                  ?>

                  <div class="element result contacts">
                    <div class="head">
                      <div class="icon">
                        <img src="<?php Theme::root(); ?>/style/images/icon-contact.png" />
                      </div>
                      <span class="title italic">Contact Information</span>
                    </div>
                    <div class="body"><?php

                      //print_r($est->contacts);
                      // Loop through each contact
                      foreach ($est->contacts as $type=> $contacts)
                      {
                      ?>

                      <div class="list contact type-<?php echo $type; ?>">
                        <div class="title">
                          <span class="icon"></span>
                          <span class="text"><?php Str::p(Contact::getTypes($type)); ?></span>
                        </div>
                        <div class="value"><?php
                          // Loop through each subcontact
                          foreach ($contacts as $i=> $contact)
                          {
                            if ($i > 0) echo ', ';
                            // anchor
                            $anchorOpen = '';
                            $anchorClose = '';
                            // Check type
                            switch ($type)
                            {
                              // Email
                              case Contact::TYPE_EMAIL:
                                // Set anchor
                                $anchorOpen = '<a '.Str::attr(array('href'=> 'mailto:'.$contact['value']), TRUE).'>';
                                $anchorClose = '</a>';
                                break;
                              // Website
                              case Contact::TYPE_WEBSITE:
                                // Set anchor
                                $anchorOpen = '<a '.Str::attr(array('href'=> $contact['value'], 'target'=> '_blank', 'rel'=> 'nofollow'), TRUE).'>';
                                $anchorClose = '</a>';
                                break;
                            }
                          ?>

                          <span title="<?php Str::p($contact['description']); ?>"><?php echo $anchorOpen; Str::p($contact['value']); echo $anchorClose; ?></span><?php
                          }
                          ?>

                        </div>
                      </div><?php
                      }
                      ?>

                    </div>
                    <div class="foot"></div>
                  </div><?php
                }

                // If there's address
                if ($est->address)
                {
                  // Loop through each address
                  foreach ($est->address as $address)
                  {
                    // Set full address
                    $fullAddress = Address::join($address);
                    // If there's no address
                    if (!$fullAddress) continue;
                  ?>

                  <div class="element result address">
                    <div class="head">
                      <div class="icon">
                        <img src="<?php Theme::root(); ?>/style/images/icon-address.png" />
                      </div>
                      <span class="title italic"><?php Str::p($address['description']); ?></span>
                    </div>
                    <div class="body">
                      <div class="gmap small" config="<?php 
                        // Set config
                        $config=array(
                          'x'=> $address['x'],
                          'y'=> $address['y'],
                          'z'=> $address['z']
                        );
                        // Print config
                        Str::p(json_encode($config));
                      ?>"></div>
                      <div class="full"><?php Str::p($fullAddress); ?></div><?php
                      // Make sure there's hint
                      if ($address['hint'])
                      {
                      ?>

                      <div class="hint"><?php Str::p($address['hint']); ?></div><?php
                      }
                      ?>

                    </div>
                    <div class="foot"></div>
                  </div><?php
                  }
                }