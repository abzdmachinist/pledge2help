<?php

	/**
	 * Establishment submit step 5
	 * @author Ronald Borla
	 * @copyright August 11, 2013
	 */

  // Set form action page
  $actionPage = Page::url('submit-step', array('step-5'), TRUE);

?>

            <div id="tab" class="narrow">
              <?php Action::begin('establishment.submit', $actionPage); ?>

                <div class="head">
                  <h1>Setup photos for your establishment</h1>
                  <div>
                    <p>Attract your customers by adding photos of your establishment. You may skip this step if you wish to add photos later</p>
                  </div><?php
                  // Load steps
                  Theme::load('submit-steps');
                  ?>

                </div>
                <div class="row">
                  <div class="input-row">
                    <div class="icon">
                      <img src="<?php Theme::root(); ?>/style/images/icon-photos.png" />
                    </div>
                    <span class="title">Add Photos</span>
                    <?php Action::button(NULL, 'button', 'Select Photos', NULL, array('class'=> 'narrow')); ?>

                    <span class="file narrow"><?php Action::input('file', 'file', NULL, array('id'=> 'upload', 'multiple'=> NULL, 'data-url'=> $actionPage, 'accept'=> 'image/*')); ?></span>
                    <span class="message">or</span>
                    <span class="text-wrap default">
                      <div class="default">Import photo from URL</div>
                      <?php Action::input(NULL, 'text', NULL, array('id'=> 'import', 'default'=> 'Import photo from URL')); ?>

                    </span>
                    <?php Action::button(NULL, 'button', 'Import Photo', NULL, array('id'=> 'download', 'class'=> 'narrow', 'target'=> $actionPage)); ?>

                    <span class="message"></span>
                  </div>
                </div><?php
                  // If there's photos
                  if (isset(TDF::$data['s'][5]['temp']) && TDF::$data['s'][5]['temp'])
                  {
                ?>

                <div class="row">
                  <div id="photos"><?php
                    // Loop through each photo
                    foreach (TDF::$data['s'][5]['temp'] as $i=> $temp)
                    {
                    ?>

                    <div class="photo<?php if (TDF::$data['s'][5]['default'][$i]) echo ' default'; ?>" pid="<?php echo $i; ?>">
                      <div class="wrap">
                        <span class="image"><img src="<?php Image::thumb(File::root('temp').'/'.$temp, '200d'); ?>" /></span>
                        <span class="text-wrap default">
                          <div class="default small">Insert title here</div>
                          <?php Action::input('title[]', 'text', TDF::$data['s'][5]['title'][$i], array('class'=> 'title', 'default'=> 'Insert title here')); ?>

                        </span>
                        <span class="icon-edit icon-edit-default" title="Set default photo"></span>
                        <span class="icon-edit icon-edit-move" title="Move photo"></span>
                        <span class="icon-edit icon-edit-remove" title="Remove photo"></span>
                      </div>
                      <?php Action::input('temp[]', 'hidden', $temp, array('class'=> 'temp')); ?>

                      <?php Action::input('default[]', 'hidden', TDF::$data['s'][5]['default'][$i], array('class'=> 'default')); ?>

                    </div><?php
                    }
                  ?>

                  </div>
                </div><?php
                  }
                ?>

                <div class="foot">
                  <div class="hint">
                    <p>* The photos you will upload at this moment will serve as the main photo album</p>
                    <p>* Click on the &quot;Select Photos&quot; button to select photos from your hard drive (you may select multiple files at once)</p>
                    <p>* You may initially upload up to 10 photos per album</p>
                    <p>* Write a short caption or title to describe the photos</p>
                    <p>* Each photo may be rearranged according to your preferred order (by dragging the hand icon or the photo)</p>
                    <p>* Click on the default button to set a photo as the primary photo</p>
                    <p>* You can import photos by providing the absolute URL of the image. Learn how to import photos from URL by <a href="#">clicking here</a></p>
                    <p>* We recommend uploading or importing photos with resolution not higher than 2 megapixels</p>
                  </div>
                  <div class="option"></div>
                  <div class="buttons">
                    <?php Action::button('btn_submit', 'submit', isset(TDF::$data['s'][5])?'Save Changes':'Next Step', 'next', isset(TDF::$data['s'][5])?array('class'=> 'narrow'):NULL); ?>

                    <span>or</span>
                    <?php Action::button('btn_submit', 'submit', 'Start Over', 'reset', array('confirm'=> 'Are you sure you want to start over?\n\nPress OK to continue')); ?>

                  </div>
                </div>
                <div class="clear overflow"></div>
              <?php Action::end(); ?>

            </div>