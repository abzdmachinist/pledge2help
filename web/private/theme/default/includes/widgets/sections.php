<?php

	/**
	 * Sections widget
	 * @author Ronald Borla
	 * @copyright October 12, 2013
	 * @package widget.sections
	 */

?>

                    <div class="widget sections">
                    	<ul>
                    		<li class="info selected open"><span class="icon"></span> <a class="text" href="#">Basic Information</a></li>
                    		<li class="products"><span class="icon"></span> <a class="text" href="#">Products &amp; Services</a></li>
                    		<li class="photos"><span class="icon"></span> <a class="text" href="#">Photo Albums</a></li>
                    		<li class="reviews"><span class="icon"></span> <a class="text" href="#">Reviews</a></li>
                    		<li class="contact"><span class="icon"></span> <a class="text" href="#">Contact Us</a></li>
                    	</ul>
                    </div>