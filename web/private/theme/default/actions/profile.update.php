<?php

	/**
	 * Update profile
	 * @author Ronald Borla
	 * @copyright July 02, 2013
	 * @package profile.update
	 */

	if (Page::$page=='profile' && User::$loggedIn)
	{
		// Set first name, last name, and middle name
		$firstName = trim(Input::post('first_name'));
		$lastName = trim(Input::post('last_name'));
		$middleName = trim(Input::post('middle_name'));
		// Set response
		$updateProfile = new Response();
		// If there's no first name, set error
		if (!$firstName) $updateProfile->set('first_name', 'Type your first name', Response::TYPE_FAILED);
		// If there's no last name, set error
		if (!$lastName) $updateProfile->set('last_name', 'Type your last name', Response::TYPE_FAILED);
		// Set birthdate
		$birth_date = Input::post('birth_year').'-'.Input::post('birth_month').'-'.Input::post('birth_day');
		// Timestamp
		$birth_timestamp = strtotime($birth_date);
		// If date is invalid
		if (!($birth_timestamp >= strtotime('1900-01-01') && $birth_timestamp <= strtotime((date('Y')-10).'-12-31')))
		{
			// Cancel birthdate
			$birth_date = '0000-00-00';
		}
		// Set gender
		$gender = Input::post('gender');
		// If invalid, set to default
		if (!Profile::getGenders($gender)) $gender = Profile::GENDER_MALE;
		// Set relationship status
		$relationship_status = Input::post('relationship_status');
		// If invalid, set to default
		if (!Profile::getRelationshipStatuses($relationship_status)) $relationship_status = Profile::RELATIONSHIP_STATUS_SINGLE;
		// If there's no error
		if ($updateProfile->type != Response::TYPE_FAILED)
		{
			// Set to update
			$update = array(
				'first_name'=> $firstName,
				'last_name'=> $lastName,
				'middle_name'=> $middleName,
				'birth_date'=> $birth_date,
				'gender'=> $gender,
				'relationship_status'=> $relationship_status
			);
			// Update profile
			Profile::set(User::$profile->id, $update);
			// Set success
			$updateProfile->type = Response::TYPE_SUCCESS;
			// Refresh profile
			User::$profile->refresh();
		}
		// Set response
		Action::response($updateProfile);
	}