<?php

	/**
	 * Login with Facebook
	 * @author Ronald Borla
	 * @copyright June 23, 2013
	 * @package user.login.fb
	 */

	// Make sure key is correct
	if (Page::$page=='login' && Input::get(User::FB_VAR_KEY) && (Input::get(User::FB_VAR_KEY)==Session::get(User::FB_VAR_KEY)))
	{
		// Set response
		$fblogin = new Response();
		// Clear session
		Session::clear(User::FB_VAR_KEY);
		// Get user
		$fbId = User::$fb->getUser();
		// If there's a user
		if ($fbId)
		{
			// Find facebook id
			$uid = User::find($fbId, User::AUTH_TYPE_FB);
			// If no user, create it
			if (!$uid)
			{
				// Get user values
				$fbUser = User::$fb->api('/me');
				// Make sure there's user
				if ($fbUser)
				{
					// Make sure email is declared
					if (isset($fbUser['email']))
					{
						// Check if email is already registered
						$uid = User::find($fbUser['email'], User::AUTH_TYPE_EMAIL);
						// If user is found
						if ($uid)
						{
							// Set user auth
							User::setAuth($uid, User::AUTH_TYPE_FB, $fbId);
						}
						else
						{
							// If not found, then create user
							$createUser = User::create(array(
								User::AUTH_TYPE_EMAIL=> $fbUser['email'],
								User::AUTH_TYPE_FB=> $fbId
							), User::PRIVILEGE_MEMBER, User::STATUS_ACTIVE);
							// After creating, add first and last name
							$profileId = Profile::create($createUser->var['id']);
							// Set gender
							$gender = Profile::GENDER_MALE;
							// Set relationship status
							$relationshipStatus = Profile::RELATIONSHIP_STATUS_SINGLE;
							// If gender is set
							if (isset($fbUser['gender']))
							{
								// Find gender
								if (($findGender=Arr::find($fbUser['gender'], Profile::getGenders()))!==FALSE)
								{
									// Set gender
									$gender = $findGender;
								}
							}
							// If relationship status is set
							if (isset($fbUser['relationship_status']))
							{
								// Find relationship status
								if (($findRelationshipStatus=Arr::find($fbUser['relationship_status'], Profile::getRelationshipStatuses()))!==FALSE)
								{
									// Set relationship status
									$relationshipStatus = $findRelationshipStatus;
								}
							}
							// Set profile
							Profile::set($profileId, array(
								'first_name'=> isset($fbUser['first_name'])?$fbUser['first_name']:'',
								'last_name'=> isset($fbUser['last_name'])?$fbUser['last_name']:'',
								'middle_name'=> isset($fbUser['middle_name'])?$fbUser['middle_name']:'',
								'gender'=> $gender,
								'birth_date'=> isset($fbUser['birthday'])?(date('Y-m-d', strtotime($fbUser['birthday']))):'0000-00-00',
								'relationship_status'=> $relationshipStatus
							));
							// Set uid
							$uid = $createUser->var['id'];
							// Send email
						  Mailer::sendTemplate('register.fb.welcome', $fbUser['email'], array(
						    'content'=> array(
						      'first-name'=> $fbUser['first_name'],
						      'email'=> $fbUser['email'],
						      'password'=> $createUser->var['password'],
						      'login-url'=> Page::url('login', array(), TRUE).'?user='.urlencode($fbUser['email']),
						      'spam-url'=> 'http://www.davaodirectory.com'
						    )
						  ));
							// Set response as success
							$fblogin->type = Response::TYPE_SUCCESS;
						}
					}
					else
					{
						// Set error message
						$fblogin->set('user', 'Facebook email address is required for authentication', Response::TYPE_FAILED);
					}
				}
				else
				{
					// There's an error
					$fblogin->set('user', 'Unexpected network error occurred. Please try again', Response::TYPE_FAILED);
				}
			}
			// If there's uid
			if ($uid)
			{
				// Proceed login
				User::login($uid, Input::get(User::FB_VAR_PERSIST)!==NULL);
				// If login was successful
				if (User::$loggedIn)
				{
					// If there's a next parameter
					if (Input::request('next')!==NULL)
					{
						// Redirect to that page
						Page::redirect(Input::request('next'));
					}
				}
			}
		}

		// Set response for login
		Action::$response['user.login'] = $fblogin;
	}