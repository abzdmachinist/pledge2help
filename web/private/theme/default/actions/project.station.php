<?php

	/**
	 * Project create station
	 * @author Ronald Borla
	 * @copyright November 23, 2013
	 * @package project.station
	 */

	if (Page::$page=='project-station' && User::$loggedIn)
	{
		// Get project ID
		$projectId = Page::$wildcards[0];
		// Check if project exists
		$project = Project::get($projectId);

		if ($project && ($project['uid'] == User::$loggedIn['id']))
		{
			// Create station
			// Set default value
			Action::value('name', Input::post('name'));
			Action::value('description', Input::post('description'));
			Action::value('address', Input::post('address'));
			Action::value('lat', Input::post('lat'));
			Action::value('lng', Input::post('lng'));
			Action::value('zoom', Input::post('zoom'));

			// Declare response
			$createStation = new Response();

			// Name, Description, Date Start, Date End
			$name = trim(Input::post('name'));
			$description = trim(Input::post('description'));
			$address = trim(Input::post('address'));
			$lat = trim(Input::post('lat'));
			$lng = trim(Input::post('lng'));
			$zoom = trim(Input::post('zoom'));

			// If there's no name
			if (!$name) $createStation->set('name', 'Type the name of the station', Response::TYPE_FAILED);
			// If there's no name
			if (!$description) $createStation->set('name', 'Type a description of the station', Response::TYPE_FAILED);
			// If there's no name
			if (!$address) $createStation->set('address', 'Type the address of the station', Response::TYPE_FAILED);

			if ($createStation->type <= Response::TYPE_SUCCESS)
			{
				// Create station
				$stationId = Project::addStation($name, $description, $address, $lat, $lng, $zoom, $projectId);
				// Go to project page
				Page::go('project-page', array($projectId));
			}

		}
	}