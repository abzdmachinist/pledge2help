<?php

	/**
	 * Project goods
	 * @author Ronald Borla
	 * @copyright November 24, 2013
	 * @package station.goods
	 */

	if (Page::$page=='project-goods' && User::$loggedIn)
	{
		// Get project ID
		$projectId = Page::$wildcards[0];
		// Check if project exists
		$project = Project::get($projectId);

		if ($project && ($project['uid'] == User::$loggedIn['id']))
		{
			// Set goods
			$goods = array();
			// Loop through each
			if (($description = Input::post('description')) &&
				  ($quantity = Input::post('quantity')) &&
				  ($unit = Input::post('unit')))
			{
				// Loop
				foreach ($description as $i=> $desc)
				{
					// Add to goods
					if (trim($desc))
					{
						// Add
						$goods[] = array(
							'description'=> $desc,
							'quantity'=> $quantity[$i],
							'unit'=> $unit[$i]
						);
					}
				}
			}

			// Create goods
			if ($goods)
			{
				// Create
				$addGoods = Project::addGoods($goods, $projectId);
				// Go to station page
				Page::go('project-station', array($projectId));
			}

		}
	}