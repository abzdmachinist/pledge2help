<?php

	/**
	 * Login user
	 * @author Ronald Borla
	 * @copyright November 23, 2013
	 * @package user.login
	 */

	// Make sure page is login and there's no user logged in
	if (Page::$page=='login' && !User::$loggedIn)
	{

		// Set default value
		Action::value('email', Input::post('email'));

		$email = trim(Input::post('email'));
		$password = Input::post('password');

		// Set response
		$login = new Response();
		// If there's no user or password
		if (!$email) $login->set('email', 'Type your email address', Response::TYPE_FAILED);
		if (!$password) $login->set('password', 'Type your password', Response::TYPE_FAILED);

		if ($login->type <= Response::TYPE_SUCCESS)
		{
			// Find user
			$uid = User::find(Input::post('email'));

			// If user is found and verified, then proceed login
			if ($uid && User::verifyPassword($uid, $password))
			{
				// Login user
				User::login($uid);
				// If user is inactive, set to active, then send welcome email
				if (User::$loggedIn['status'] == User::STATUS_INACTIVE)
				{
					// Set to active
					User::setStatus($uid, User::STATUS_ACTIVE);
					// Update status
					User::$loggedIn['status'] = User::STATUS_ACTIVE;

				}
				// If login was successful
				if (User::$loggedIn)
				{
					// If there's a next parameter
					if (Input::request('next')!==NULL)
					{
						// Redirect to that page
						Page::redirect(Input::request('next'));
					}
					// Redirect to project page
					if (User::is(User::PRIVILEGE_ORGANIZATION, User::$loggedIn['privilege']))
					{
						// Redirect
						Page::go('project');
					}
					// Redirect
					Page::go('homepage');
				}
			}
			else
			{
				// Set invalid response
				$login->set('email', 'Invalid user or password', Response::TYPE_FAILED); 
			}
		}
		// If invalid, set response
		Action::response($login);

	}