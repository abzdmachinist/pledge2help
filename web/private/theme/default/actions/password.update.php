<?php

	/**
	 * Update password
	 * @author Ronald Borla
	 * @copyright July 05, 2013
	 * @package password.update
	 */

	if (Page::$page=='password' && User::$loggedIn)
	{
		// Set response
		$updatePassword = new Response();

		// Get new password
		$newPassword = Input::post('new_password');
		// Get confirm password
		$confirmPassword = Input::post('confirm_password');
		// Get old password
		$oldPassword = Input::post('old_password');

		// Make sure password is correct length
		$passwordLength = Str::len($newPassword);
		// Filter min length
		if ($passwordLength < User::PASSWORD_MIN_LENGTH)
		{
			$updatePassword->set('new_password', 'Password must contain at least '.User::PASSWORD_MIN_LENGTH.' characters', Response::TYPE_FAILED);
		}
		// Filter max length
		if ($passwordLength > User::PASSWORD_MAX_LENGTH)
		{
			$updatePassword->set('new_password', 'Password must not contain more than '.User::PASSWORD_MAX_LENGTH.' characters', Response::TYPE_FAILED);
		}
		// Make sure new password and confirm password are the same
		if ($newPassword !== $confirmPassword)
		{
			$updatePassword->set('confirm_password', 'New password and confirm password does not match', Response::TYPE_FAILED);
		}
		// Old password should be correct
		if (!User::verifyPassword(User::$loggedIn['id'], $oldPassword))
		{
			$updatePassword->set('old_password', 'Old password is invalid', Response::TYPE_FAILED);
		}

		// If failed
		if ($updatePassword->type != Response::TYPE_FAILED)
		{
			// Update password
			User::updatePassword(User::$loggedIn['id'], $newPassword);
			// Set success
			$updatePassword->type = Response::TYPE_SUCCESS;
		}

		// Set response
		Action::response($updatePassword);
	}