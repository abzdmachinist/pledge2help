<?php

	/**
	 * Davao Directory Default theme
	 * @author Ronald Borla
	 * @copyright April 15, 2013
	 */
	
	// Load theme configuration
	require(Theme::$path.'/config.php');
	
	// Initialize User
	User::init();
	// Initialize Page
	Page::init(array('ThemeDefaultFunctions', 'pageAutoCorrect'));