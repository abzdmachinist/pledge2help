<?php

	/**
	 * Default theme configuration
	 * @author Ronald Borla
	 * @copyright April 15, 2013
	 */

	// Require functions
	require(Theme::$path.'/classes/Functions.php');
	
	// Set pages configuration
	
	// Homepage
	Page::$pages['homepage']['path'] = Theme::$path.'/pages/homepage.php';
	Page::$pages['homepage']['url'] = array('');
	// Login page
	Page::$pages['login']['path'] = Theme::$path.'/pages/login.php';
	Page::$pages['login']['url'] = array('login');
	// Register page
	Page::$pages['register']['path'] = Theme::$path.'/pages/register.php';
	Page::$pages['register']['url'] = array('register');
	// Organization register page
	Page::$pages['organization']['path'] = Theme::$path.'/pages/organization.php';
	Page::$pages['organization']['url'] = array('organization');
	// Project page
	Page::$pages['project']['path'] = Theme::$path.'/pages/project.php';
	Page::$pages['project']['url'] = array('project');
	// Project page
	Page::$pages['project-page']['path'] = Theme::$path.'/pages/project-page.php';
	Page::$pages['project-page']['url'] = array('%project%/(.+)');
	// Project goods
	Page::$pages['project-goods']['path'] = Theme::$path.'/pages/project-goods.php';
	Page::$pages['project-goods']['url'] = array('%project-page%/goods');
	// Project create station
	Page::$pages['project-station']['path'] = Theme::$path.'/pages/project-station.php';
	Page::$pages['project-station']['url'] = array('%project-page%/station');
	// Station page
	Page::$pages['station']['path'] = Theme::$path.'/pages/station.php';
	Page::$pages['station']['url'] = array('station/(.+)');

	// Logout page
	Page::$pages['logout']['path'] = Theme::$path.'/pages/logout.php';
	Page::$pages['logout']['url'] = array('logout');
	// Error page
	Page::$pages['error']['path'] = Theme::$path.'/pages/404.php';
	Page::$pages['error']['url'] = array('error');
	
	// Set actions directory
	Action::$path = Theme::$path.'/actions';
	
	// Register files
	Theme::$files['header'] = Theme::$path.'/includes/header.php';
	Theme::$files['footer'] = Theme::$path.'/includes/footer.php';
	
	// Set logo
	//Theme::link('shortcut icon', Theme::$root.'/style/images/logo-icon.png');

	// Load jQuery UI stylesheet
	//Theme::link('stylesheet', Theme::$root.'/script/jquery-ui/css/default/jquery-ui-1.10.3.custom.min.css', 'text/css');
	// Load main stylesheet
	//Theme::link('stylesheet', Theme::$root.'/style/stylesheet.css', 'text/css');

	// Load jQuery
	Theme::script('text/javascript', SITE_ROOT.'/script/jquery-1.10.2.min.js');

	// If not disabled
	if (!isset(Cl::$config['gmap']['disabled']) || !Cl::$config['gmap']['disabled'])
	{
		// Activate gmap
		Theme::script('text/javascript', 'https://maps.googleapis.com/maps/api/js?key='.Cl::$config['gmap']['key'].'&sensor=true');
	}

	// Load core js
	Theme::script('text/javascript', SITE_ROOT.'/script/core.js');
	// Set vars
	$vars = array(
		'root'=> array(
			'theme'=> Theme::root(TRUE),
			'site'=> SITE_ROOT
		)
	);
	// If disabled
	if (isset(Cl::$config['gmap']['disabled']) && Cl::$config['gmap']['disabled'])
	{
		// Set vars
		$vars['gmap']['disabled'] = TRUE;
	}
	// Insert into javascript
	Theme::script('text/javascript', NULL, '_core.vars='.json_encode($vars).';');