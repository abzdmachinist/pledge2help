<?php

	/**
	 * Project page
	 * @author Ronald Borla
	 * @copyright November 23, 2013
	 * @package project-page
	 */

	// Get project ID
	$projectId = Page::$wildcards[1];
	// Check if project exists
	$project = Project::get($projectId);