<?php

	/**
	 * Default search page
	 * @author Ronald Borla
	 * @copyright May 01, 2013
	 */

	// Initialize actions
	Action::init();
	// Manually execute search.go
	Action::exec('search.go');
	
	// Set search title
	Theme::title('Davao Directory - Search');
	// Load header
	Theme::load('header');
	
?>

    <div id="middle-wrap">
      <div id="middle"><?php 
      	// Load search widget
      	Theme::load('widget-search'); 
        ?>
        
      </div>
    </div><?php
	
	// Load footer
	Theme::load('footer');