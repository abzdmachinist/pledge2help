<?php

	/**
	 * Default theme Update password page
	 * @author Ronald Borla
	 * @copyright July 05, 2013
	 */

  if (!User::$loggedIn) Page::go('login');

  // Initialize actions
  Action::init();
	 
	// Set profile title
	Theme::title('Davao Directory - Update Password');
	// Load header
	Theme::load('header');
	
?>

    <div id="middle-wrap">
      <div id="middle"><?php 
      	// Load search widget
      	Theme::load('widget-search'); 
        ?>
        
        <div id="tab-wrap"><?php
        	// Load tab for user
        	Theme::load('tab-user');
          ?>

          <div id="tab-body">
            <div id="tab">
              <?php Action::begin('password.update', Page::url('password', array(), TRUE)); ?>

                <div class="head">
                  <h1>Password settings</h1>
                  <div>
                    <p>Update your password</p>
                  </div>
                </div>
                <div class="row">
                  <div class="input-row">
                    <label>
                      <div class="icon">
                        <img src="<?php Theme::root(); ?>/style/images/icon-password.png" />
                      </div>
                      <span class="title">New Password</span>
                      <span class="text-wrap"><?php Action::input('new_password', 'password', '', (Action::response('new_password', NULL, Action::OPTION_RETURN_STATUS) == Response::TYPE_FAILED)?array('class'=> 'focus'):array()); ?></span>
                      <span class="message<?php 
                        Action::response('new_password', array(Response::TYPE_FAILED=> ' invalid'), Action::OPTION_RETURN_ONLY); ?>"><?php 
                        Action::response('new_password', array(Response::TYPE_DEFAULT=> 'Type your new password'), Action::OPTION_OVERWRITE_AND_RETURN); ?></span>
                    </label>
                  </div>
                  <div class="input-row">
                    <label>
                      <div class="icon"></div>
                      <span class="title">Confirm Password</span>
                      <span class="text-wrap"><?php Action::input('confirm_password', 'password', '', (Action::response('confirm_password', NULL, Action::OPTION_RETURN_STATUS) == Response::TYPE_FAILED)?array('class'=> 'focus'):array()); ?></span>
                      <span class="message<?php 
                        Action::response('confirm_password', array(Response::TYPE_FAILED=> ' invalid'), Action::OPTION_RETURN_ONLY); ?>"><?php 
                        Action::response('confirm_password', array(Response::TYPE_DEFAULT=> 'Confirm new password'), Action::OPTION_OVERWRITE_AND_RETURN); ?></span>
                    </label>
                  </div>
                  <div class="input-row">
                    <label>
                      <div class="icon"></div>
                      <span class="title">Old Password</span>
                      <span class="text-wrap"><?php Action::input('old_password', 'password', '', (Action::response('old_password', NULL, Action::OPTION_RETURN_STATUS) == Response::TYPE_FAILED)?array('class'=> 'focus'):array()); ?></span>
                      <span class="message<?php 
                        Action::response('old_password', array(Response::TYPE_FAILED=> ' invalid'), Action::OPTION_RETURN_ONLY); ?>"><?php 
                        Action::response('old_password', array(Response::TYPE_DEFAULT=> 'Type your old password'), Action::OPTION_OVERWRITE_AND_RETURN); ?></span>
                    </label>
                  </div>
                </div>
                <div class="foot">
                  <div class="hint">
                    <p>* All fields are required</p>
                  </div>
                  <div class="option"></div>
                  <div class="buttons">
                    <?php Action::button('btn_save', 'submit', 'Update Password', NULL, array('class'=> 'medium')); ?>

                    <?php if (isset(Action::$response[Action::$action]) && Action::$response[Action::$action]->type == Response::TYPE_SUCCESS){ ?><span class="message valid">Password successfully updated</span><?php } ?>

                  </div>
                </div>
                <div class="clear overflow"></div>
              <?php Action::end(); ?>

            </div>
          </div>
        </div>
      </div>
    </div><?php
	
	// Load footer
	Theme::load('footer');