<?php

	/**
	 * Project page
	 * @author Ronald Borla
	 * @copyright November 23, 2013
	 * @package project
	 */

  // Initialize actions
  Action::init();

  // If user is already logged in, go to profile page
  if (!User::$loggedIn || !User::is(User::PRIVILEGE_ORGANIZATION, User::$loggedIn['privilege'])) Page::go('login');

  // Set register title
  Theme::title('Pledge2Help - Create Project');
  // Load header
  Theme::load('header');

?>

<?php Action::begin('project.create', Page::url('project', array(), TRUE)); ?>

<div>
  <span>Project Name</span> <?php Action::input('name', 'text', Action::value('name'), array('placeholder'=> 'Project Name')); ?>
</div>
<div>
  <span>Description</span> <?php Action::textarea('description', Action::value('description'), array('placeholder'=> 'Description')); ?>
</div>
<div>
  <span>Date Start</span> <?php Action::input('date_start', 'text', Action::value('date_start'), array('placeholder'=> 'Date Start')); ?>
</div>
<div>
  <span>Date End</span> <?php Action::input('date_end', 'text', Action::value('date_end'), array('placeholder'=> 'Date End')); ?>
</div>
<?php Action::button('btn_create', 'submit', 'Create Project'); ?>

<?php Action::end(); ?>

<?php

  // Load footer
  Theme::load('footer');