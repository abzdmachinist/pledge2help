<?php

	/**
	 * Station goods page
	 * @author Ronald Borla
	 * @copyright November 24, 2013
	 * @package station
	 */

  // Initialize actions
  Action::init();

	// Get station ID
	$stationId = Page::$wildcards[0];
	// Check if project exists
	$station = Station::get($stationId);

	// If there's no user and station
	if (!$station || !User::$loggedIn)
	{
		// Go to error page
		Page::go('error');
	}
	// Set project
	$project = Project::get($station['prid']);
	// Check if project
	if (!$project || ($project['uid'] != User::$loggedIn['id']))
	{
		// Go to error page
		Page::go('error');
	}

  // Set register title
  Theme::title('Pledge2Help - Add Goods');
  // Load header
  Theme::load('header');

?>

<?php Action::begin('project.goods', Page::url('project-goods', array($projectId), TRUE)); ?>

<div>
  <span>Station Name</span> <?php Action::input('name', 'text', Action::value('name'), array('placeholder'=> 'Station Name')); ?>
</div>
<div>
  <span>Description</span> <?php Action::textarea('description', Action::value('description'), array('placeholder'=> 'Description')); ?>
</div>
<div>
  <span>Address</span> <?php Action::input('address', 'text', Action::value('address'), array('placeholder'=> 'Address')); ?>
</div>
<?php Action::button('btn_create', 'submit', 'Create Station'); ?>

<?php Action::end(); ?>

<?php

  // Load footer
  Theme::load('footer');