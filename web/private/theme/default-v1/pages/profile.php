<?php

	/**
	 * Default theme Profile page
	 * @author Ronald Borla
	 * @copyright June 30, 2013
	 */

  if (!User::$loggedIn) Page::go('login');

  // Initialize actions
  Action::init();
	 
	// Set profile title
	Theme::title('Davao Directory - Profile');
	// Load header
	Theme::load('header');
	
?>

    <div id="middle-wrap">
      <div id="middle"><?php 
      	// Load search widget
      	Theme::load('widget-search'); 
        ?>
        
        <div id="tab-wrap"><?php
        	// Load tab for user
        	Theme::load('tab-user');
          ?>

          <div id="tab-body">
            <div id="tab" class="medium">
              <?php Action::begin('profile.update', Page::url('profile', array(), TRUE)); ?>

                <div class="head">
                  <h1>Profile settings</h1>
                  <div>
                    <p>Manage your profile settings</p>
                  </div>
                </div>
                <div class="row">
                  <div class="input-row">
                    <label>
                      <div class="icon">
                        <img src="<?php Theme::root(); ?>/style/images/icon-user.png" />
                      </div>
                      <span class="title">First Name *</span>
                      <span class="text-wrap"><?php Action::input('first_name', 'text', User::$profile->first_name, (Action::response('first_name', NULL, Action::OPTION_RETURN_STATUS) == Response::TYPE_FAILED)?array('class'=> 'focus'):array()); ?></span>
                      <span class="message<?php 
                        Action::response('first_name', array(Response::TYPE_FAILED=> ' invalid'), Action::OPTION_RETURN_ONLY); ?>"><?php 
                        Action::response('first_name', array(Response::TYPE_DEFAULT=> 'Type your first name'), Action::OPTION_OVERWRITE_AND_RETURN); ?></span>
                    </label>
                  </div>
                  <div class="input-row">
                    <label>
                      <div class="icon"></div>
                      <span class="title">Last Name *</span>
                      <span class="text-wrap"><?php Action::input('last_name', 'text', User::$profile->last_name, (Action::response('last_name', NULL, Action::OPTION_RETURN_STATUS) == Response::TYPE_FAILED)?array('class'=> 'focus'):array()); ?></span>
                      <span class="message<?php 
                        Action::response('last_name', array(Response::TYPE_FAILED=> ' invalid'), Action::OPTION_RETURN_ONLY); ?>"><?php 
                        Action::response('last_name', array(Response::TYPE_DEFAULT=> 'Type your last name'), Action::OPTION_OVERWRITE_AND_RETURN); ?></span>
                    </label>
                  </div>
                  <div class="input-row">
                    <label>
                      <div class="icon"></div>
                      <span class="title">Middle Name</span>
                      <span class="text-wrap"><?php Action::input('middle_name', 'text', User::$profile->middle_name, (Action::response('middle_name', NULL, Action::OPTION_RETURN_STATUS) == Response::TYPE_FAILED)?array('class'=> 'focus'):array()); ?></span>
                      <span class="message<?php 
                        Action::response('middle_name', array(Response::TYPE_FAILED=> ' invalid'), Action::OPTION_RETURN_ONLY); ?>"><?php 
                        Action::response('middle_name', array(Response::TYPE_DEFAULT=> 'Type your middle name'), Action::OPTION_OVERWRITE_AND_RETURN); ?></span>
                    </label>
                  </div>
                  <div class="input-row">
                    <div class="icon"></div>
                    <span class="title">Gender</span>
                    <span class="text-wrap"><?php Action::select(Profile::getGenders(), 'gender', User::$profile->gender, (Action::response('gender', NULL, Action::OPTION_RETURN_STATUS) == Response::TYPE_FAILED)?array('class'=> 'focus auto-width'):array('class'=> 'auto-width')); ?></span>
                    <span class="message<?php 
                      Action::response('gender', array(Response::TYPE_FAILED=> ' invalid'), Action::OPTION_RETURN_ONLY); ?>"><?php 
                      Action::response('gender', array(Response::TYPE_DEFAULT=> 'Select your gender'), Action::OPTION_OVERWRITE_AND_RETURN); ?></span>
                  </div>
                  <div class="input-row">
                    <div class="icon"></div>
                    <span class="title">Birthdate</span>
                    <span class="text-wrap"><?php 
                      // Get birthdate
                      $birthDate = explode('-', User::$profile->birth_date);
                      Action::select(array(''=> '', 1=> 'Jan', 2=> 'Feb', 3=> 'Mar', 4=> 'Apr', 5=> 'May', 6=> 'Jun', 7=> 'Jul', 8=> 'Aug', 9=> 'Sep', 10=> 'Oct', 11=> 'Nov', 12=> 'Dec'), 'birth_month', $birthDate[1], (Action::response('birth_month', NULL, Action::OPTION_RETURN_STATUS) == Response::TYPE_FAILED)?array('class'=> 'focus auto-width'):array('class'=> 'auto-width')); 
                      ?></span>
                    <span class="text-wrap"><?php
                      // Set days
                      $days = array(''=> '');
                      for ($i=1; $i<=31; $i++)
                      {
                        $days[$i]=$i;
                      }
                      Action::select($days, 'birth_day', $birthDate[2], (Action::response('birth_day', NULL, Action::OPTION_RETURN_STATUS) == Response::TYPE_FAILED)?array('class'=> 'focus auto-width'):array('class'=> 'auto-width')); 
                      ?></span>
                    <span class="text-wrap"><?php
                      // Set years
                      $years = array(''=> '');
                      for ($i=(date('Y')-10); $i>=1900; $i--)
                      {
                        $years[$i]=$i;
                      }
                      Action::select($years, 'birth_year', $birthDate[0], (Action::response('birth_year', NULL, Action::OPTION_RETURN_STATUS) == Response::TYPE_FAILED)?array('class'=> 'focus auto-width'):array('class'=> 'auto-width')); 
                      ?></span>
                    <span class="message<?php 
                      Action::response('birth_date', array(Response::TYPE_FAILED=> ' invalid'), Action::OPTION_RETURN_ONLY); ?>"><?php 
                      Action::response('birth_date', array(Response::TYPE_DEFAULT=> 'Select your birthdate'), Action::OPTION_OVERWRITE_AND_RETURN); ?></span>
                  </div>
                  <div class="input-row">
                    <div class="icon"></div>
                    <span class="title">Relationship Status</span>
                    <span class="text-wrap"><?php Action::select(Profile::getRelationshipStatuses(), 'relationship_status', User::$profile->relationship_status, (Action::response('relationship_status', NULL, Action::OPTION_RETURN_STATUS) == Response::TYPE_FAILED)?array('class'=> 'focus auto-width'):array('class'=> 'auto-width')); ?></span>
                    <span class="message<?php 
                      Action::response('relationship_status', array(Response::TYPE_FAILED=> ' invalid'), Action::OPTION_RETURN_ONLY); ?>"><?php 
                      Action::response('relationship_status', array(Response::TYPE_DEFAULT=> 'Select your relationship status'), Action::OPTION_OVERWRITE_AND_RETURN); ?></span>
                  </div>
                </div>
                <div class="foot">
                  <div class="hint">
                    <p>* Fields with asterisk (*) are required</p>
                  </div>
                  <div class="option"></div>
                  <div class="buttons">
                    <?php Action::button('btn_save', 'submit', 'Update Profile', NULL, array('class'=> 'medium')); ?>

                    <?php if (Action::responseType() == Response::TYPE_SUCCESS){ ?><span class="message valid">Profile successfully updated</span><?php } ?>

                  </div>
                </div>
                <div class="clear overflow"></div>
              <?php Action::end(); ?>

            </div>
          </div>
        </div>
      </div>
    </div><?php
	
	// Load footer
	Theme::load('footer');