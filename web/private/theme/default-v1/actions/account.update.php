<?php

	/**
	 * Update account
	 * @author Ronald Borla
	 * @copyright July 05, 2013
	 * @package account.update
	 */

	if (Page::$page=='account' && User::$loggedIn)
	{
		// Set response
		$updateAccount = new Response();

		$updateAuth = array();

		// Get username
		$username = trim(Input::post('username'));

		// If there's username
		if ($username && $username!=User::getAuth(User::$loggedIn['id'], User::AUTH_TYPE_USERNAME))
		{
			// Make sure it's valid
			$filterUsername = User::filterAuth(User::AUTH_TYPE_USERNAME, $username);
			// If there's error
			if (!$filterUsername['success'])
			{
				$updateAccount->set('username', $filterUsername['errors'][0], Response::TYPE_FAILED);
			}
			else
			{
				$updateAuth[User::AUTH_TYPE_USERNAME] = $username;
			}
		}

		// If there's no failure
		if ($updateAccount->type != Response::TYPE_FAILED && $updateAuth)
		{
			// Loop through each update
			foreach ($updateAuth as $authType=> $authValue)
			{
				User::setAuth(User::$loggedIn['id'], $authType, $authValue);
			}
			// Set success
			$updateAccount->type = Response::TYPE_SUCCESS;
		}

		// Set response
		Action::response($updateAccount);
	}