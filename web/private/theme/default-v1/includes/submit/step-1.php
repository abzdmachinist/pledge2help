<?php

	/**
	 * Establishment submit step 1
	 * @author Ronald Borla
	 * @copyright July 08, 2013
	 */
  
?>

            <div id="tab" class="medium">
              <?php Action::begin('establishment.submit', Page::url('submit', array(), TRUE)); ?>

                <div class="head">
                  <h1>Submit an establishment for free</h1>
                  <div>
                    <p>It's easy! Just follow our simple steps. First, fill-in basic information</p>
                  </div><?php
                  // Load steps
                  Theme::load('submit-steps');
                  ?>

                </div>
                <div class="row">
                  <div class="input-row">
                    <label>
                      <div class="icon">
                        <img src="<?php Theme::root(); ?>/style/images/icon-establishment.png" />
                      </div>
                      <span class="title">Establishment Name *</span>
                      <span class="text-wrap"><?php Action::input('name', 'text', TDF::$data['s'][1]['name'], TDF::$data['se'][1]['name']?array('class'=> 'wide focus'):array('class'=> 'wide')); ?></span>
                      <span class="message<?php 
                        Str::p(TDF::$data['se'][1]['name']?' invalid':''); ?>"><?php 
                        Str::p(TDF::$data['se'][1]['name']?TDF::$data['se'][1]['name'][0]:'Type establishment name'); ?></span>
                    </label>
                  </div>
                </div>
                <div class="row">
                  <div class="input-row">
                    <label>
                      <div class="icon">
                        <img src="<?php Theme::root(); ?>/style/images/icon-description.png" />
                      </div>
                      <span class="title">Description</span>
                      <span class="text-wrap"><?php Action::textarea('description', TDF::$data['s'][1]['description'], TDF::$data['se'][1]['description']?array('class'=> 'wide focus'):array('class'=> 'wide')); ?></span>
                      <span class="message<?php 
                        Str::p(TDF::$data['se'][1]['description']?' invalid':''); ?>"><?php 
                        Str::p(TDF::$data['se'][1]['description']?TDF::$data['se'][1]['description'][0]:'Write a short description about your establishment'); ?></span>
                    </label>
                  </div>
                </div>
                <div class="foot">
                  <div class="hint">
                    <p>* Check if the spelling are all correct. Please input data as accurate as possible</p>
                    <p>* Please refrain from using all caps establishment name</p>
                    <p>* If your establishment already exists in our directory, please contact us and include your request to claim over its ownership</p>
                    <p>* You may edit the establishment name later but take note that you cannot freely modify your page's permalink unless permitted by the administrators</p>
                    <p>* You will automatically be assigned a permalink which closely resembles your establishment's name (<a target="_blank" href="#">learn more about permalinks</a>)</p>
                    <p>* Limit the description to 1000 characters</p>
                    <p>* Fields with asterisk (*) are required</p>
                  </div>
                  <div class="option"></div>
                  <div class="buttons">
                    <?php Action::button('btn_submit', 'submit', isset(TDF::$data['s'][1])?'Save Changes':'Next Step', 'next', isset(TDF::$data['s'][1])?array('class'=> 'narrow'):NULL); ?>

                    <span>or</span>
                    <?php Action::button('btn_submit', 'submit', 'Start Over', 'reset', array('confirm'=> 'Are you sure you want to start over?\n\nPress OK to continue')); ?>

                  </div>
                </div>
                <div class="clear overflow"></div>
              <?php Action::end(); ?>

            </div>