
                  <div class="steps">
                    <ul><?php
                    // Get current step
                    $currentStep = TDF::getSubmitStep();
                    // Set steps
                    $steps = array(
                      1=> 'Basic Information',
                      2=> 'Select Categories',
                      3=> 'Setup Address',
                      4=> 'Contact Information',
                      5=> 'Setup Photos',
                      6=> 'Confirm Submission'
                    );
                    // Loop through each step
                    foreach ($steps as $step=> $title)
                    {
                      // Set class
                      $class = array();
                      // Set img
                      $img = '';
                      // Set url
                      $url = '';

                      // If step is 1, add first
                      if ($step==1) $class[] = 'first';
                      // If previous and current is not set
                      if (!isset(TDF::$data['s'][$step-1]) && !isset(TDF::$data['s'][$step])) $class[] = 'disabled';
                      // If current step
                      if ($step==$currentStep)
                      {
                        // Set selected
                        $class[] = 'selected';
                      }
                      else
                      {
                        // Set page
                        if ($step == 1)
                        {
                          // Set submit page
                          $url = Page::url('submit', array(), TRUE);
                        }
                        else
                        {
                          // Set url for step page
                          $url = Page::url('submit-step', array('step-'.$step), TRUE);
                        }
                      }

                      // If step is set
                      if (isset(TDF::$data['s'][$step]))
                      {
                        // If there errors
                        if (isset(TDF::$data['se'][$step]) && TDF::$data['se'][$step])
                        {
                          // Set warning
                          $img = 'warning';
                        }
                        else
                        {
                          // Set ok
                          $img = 'check';
                        }
                      }
                      ?>

                      <li<?php if ($class) echo ' class="',implode(' ', $class),'"'; ?>><?php if ($url){ ?><a href="<?php echo $url;  ?>"><?php } ?><h3>STEP <?php echo $step,($img?(' <img src="'.Theme::root(TRUE).'/style/images/icon-'.$img.'.png" />'):''); ?></h3><p><?php Str::p($title); ?></p><?php if ($url){ ?></a><?php } ?></li><?php
                    }
                    ?>

                    </ul>
                  </div>