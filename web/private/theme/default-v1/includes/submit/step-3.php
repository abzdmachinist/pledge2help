<?php

	/**
	 * Establishment submit step 3
	 * @author Ronald Borla
	 * @copyright Auguts 03, 2013
	 */
  
?>

            <div id="tab">
              <?php Action::begin('establishment.submit', Page::url('submit-step', array('step-3'), TRUE)); ?>

                <div class="head">
                  <h1>Setup the address of the establishment</h1>
                  <div>
                    <p>Fill-in the complete address of your establishment. If address is not applicable, you may skip this step</p>
                  </div><?php
                  // Load steps
                  Theme::load('submit-steps');
                  ?>

                </div>
                <div class="row">
                  <div class="input-row">
                    <label>
                      <div class="icon">
                        <img src="<?php Theme::root(); ?>/style/images/icon-address.png" />
                      </div>
                      <span class="title">Province</span>
                      <span class="text-wrap"><?php Action::input('province', 'text', TDF::$data['s'][3]['province']); ?></span>
                      <span class="message<?php 
                        Str::p(TDF::$data['se'][3]['province']?' invalid':''); ?>"><?php 
                        Str::p(TDF::$data['se'][3]['province']?TDF::$data['se'][3]['province'][0]:'Type the name of the province'); ?></span>
                    </label>
                  </div>
                  <div class="input-row">
                    <label>
                      <div class="icon"></div>
                      <span class="title">City / Municipality</span>
                      <span class="text-wrap"><?php Action::input('municipality', 'text', TDF::$data['s'][3]['municipality']); ?></span>
                      <span class="message<?php 
                        Str::p(TDF::$data['se'][3]['municipality']?' invalid':''); ?>"><?php 
                        Str::p(TDF::$data['se'][3]['municipality']?TDF::$data['se'][3]['municipality'][0]:'Type the name of the city or municipality'); ?></span>
                    </label>
                  </div>
                  <div class="input-row">
                    <label>
                      <div class="icon"></div>
                      <span class="title">Zip Code</span>
                      <span class="text-wrap"><?php Action::input('zip', 'text', TDF::$data['s'][3]['zip'], array('class'=> 'narrow')); ?></span>
                      <span class="message<?php 
                        Str::p(TDF::$data['se'][3]['zip']?' invalid':''); ?>"><?php 
                        Str::p(TDF::$data['se'][3]['zip']?TDF::$data['se'][3]['zip'][0]:'Type the zip code of the city or municipality'); ?></span>
                    </label>
                  </div>
                  <div class="input-row">
                    <label>
                      <div class="icon"></div>
                      <span class="title">District</span>
                      <span class="text-wrap"><?php Action::input('district', 'text', TDF::$data['s'][3]['district']); ?></span>
                      <span class="message<?php 
                        Str::p(TDF::$data['se'][3]['district']?' invalid':''); ?>"><?php 
                        Str::p(TDF::$data['se'][3]['district']?TDF::$data['se'][3]['district'][0]:'Type the name of the district where your establishment belongs'); ?></span>
                    </label>
                  </div>
                  <div class="input-row">
                    <label>
                      <div class="icon"></div>
                      <span class="title">Barangay</span>
                      <span class="text-wrap"><?php Action::input('barangay', 'text', TDF::$data['s'][3]['barangay']); ?></span>
                      <span class="message<?php 
                        Str::p(TDF::$data['se'][3]['barangay']?' invalid':''); ?>"><?php 
                        Str::p(TDF::$data['se'][3]['barangay']?TDF::$data['se'][3]['barangay'][0]:'Type the name of the barangay'); ?></span>
                    </label>
                  </div>
                  <div class="input-row">
                    <label>
                      <div class="icon"></div>
                      <span class="title">Village</span>
                      <span class="text-wrap"><?php Action::input('village', 'text', TDF::$data['s'][3]['village']); ?></span>
                      <span class="message<?php 
                        Str::p(TDF::$data['se'][3]['village']?' invalid':''); ?>"><?php 
                        Str::p(TDF::$data['se'][3]['village']?TDF::$data['se'][3]['village'][0]:'Type the name of the village'); ?></span>
                    </label>
                  </div>
                  <div class="input-row">
                    <label>
                      <div class="icon"></div>
                      <span class="title">Street</span>
                      <span class="text-wrap"><?php Action::input('street', 'text', TDF::$data['s'][3]['street'], array('class'=> 'wide')); ?></span>
                      <span class="message<?php 
                        Str::p(TDF::$data['se'][3]['street']?' invalid':''); ?>"><?php 
                        Str::p(TDF::$data['se'][3]['street']?TDF::$data['se'][3]['street'][0]:'Type the complete street address'); ?></span>
                    </label>
                  </div>
                  <div class="input-row">
                    <label>
                      <div class="icon"></div>
                      <span class="title">Location Hint</span>
                      <span class="text-wrap"><?php Action::input('hint', 'text', TDF::$data['s'][3]['hint'], array('class'=> 'wide')); ?></span>
                      <span class="message<?php 
                        Str::p(TDF::$data['se'][3]['hint']?' invalid':''); ?>"><?php 
                        Str::p(TDF::$data['se'][3]['hint']?TDF::$data['se'][3]['city'][0]:'Type a hint of the location (e.g. "In front of tennis court")'); ?></span>
                    </label>
                  </div>
                  <div class="input-row">
                    <label>
                      <div class="icon"></div>
                      <span class="title">Location on Map</span>
                      <div class="gmap medium" config="<?php 
                        // Set config
                        $config=array();
                        // If x is set
                        if (TDF::$data['s'][3]['x']) $config['x']=TDF::$data['s'][3]['x'];
                        // If y is set
                        if (TDF::$data['s'][3]['y']) $config['y']=TDF::$data['s'][3]['y'];
                        // If z is set
                        if (TDF::$data['s'][3]['z']) $config['z']=TDF::$data['s'][3]['z'];
                        // If there's config
                        if ($config) Str::p(json_encode($config));
                      ?>"></div>
                      <span class="message">Locate your establishment on the map</span>
                    </label>
                    <?php Action::input('x', 'hidden', TDF::$data['s'][3]['x']); ?>

                    <?php Action::input('y', 'hidden', TDF::$data['s'][3]['y']); ?>

                    <?php Action::input('z', 'hidden', TDF::$data['s'][3]['z']); ?>

                  </div>
                </div>
                <div class="foot">
                  <div class="hint">
                    <p>* Note that the above address should be the official location of your establishment</p>
                    <p>* Drag your mouse on the map above and locate your establishment. You may zoom-in or zoom-out using your mouse wheel or by clicking the zoom icons</p>
                    <p>* The marker in the map above will represent the exact location of your establishment. Be as accurate as possible when setting it up</p>
                    <p>* If your establishment has a separate address for the office, you may add an office address later (learn more about <a href="#">office address</a>)</p>
                    <p>* Your establishment may not have an address within Davao Region, you may add a website address in the contact information</p>
                    <p>* Note that if you don't specify at least one entry on any of the fields above, address will not be shown in your establishment page</p>
                    <p>* All fields are optional</p>
                  </div>
                  <div class="option"></div>
                  <div class="buttons">
                    <?php Action::button('btn_submit', 'submit', isset(TDF::$data['s'][3])?'Save Changes':'Next Step', 'next', isset(TDF::$data['s'][3])?array('class'=> 'narrow'):NULL); ?>

                    <span>or</span>
                    <?php Action::button('btn_submit', 'submit', 'Start Over', 'reset', array('confirm'=> 'Are you sure you want to start over?\n\nPress OK to continue')); ?>

                  </div>
                </div>
                <div class="clear overflow"></div>
              <?php Action::end(); ?>

            </div>