<?php

	/**
	 * Establishment submit step 6
	 * @author Ronald Borla
	 * @copyright August 13, 2013
	 */

?>

            <div id="tab" class="wide info">
              <?php Action::begin('establishment.submit', Page::url('submit-step', array('step-6'), TRUE)); ?>

                <div class="head">
                  <h1>Confirm your submission</h1>
                  <div>
                    <p>Review all basic information, categories, address, contact information and photos</p>
                  </div><?php
                  // Load steps
                  Theme::load('submit-steps');
                  ?>

                </div>
                <div class="row">
                  <div class="input-row">
                    <div class="icon">
                      <img src="<?php Theme::root(); ?>/style/images/icon-establishment.png" />
                    </div>
                    <span class="title">Establishment Name</span>
                    <div class="info major">
                      <span class="value<?php if (TDF::$data['se'][1]['name']) echo ' invalid'; ?>"><?php Str::p(TDF::$data['se'][1]['name']?TDF::$data['se'][1]['name'][0]:TDF::$data['s'][1]['name']); ?></span>
                      <a class="edit" href="<?php Page::url('submit'); ?>">[edit <img src="<?php Theme::root(); ?>/style/images/icon-edit.png" />]</a>
                    </div>
                  </div><?php
                  // If there's description
                  if (TDF::$data['s'][1]['description'])
                  {
                  ?>

                  <div class="input-row short">
                    <div class="icon"></div>
                    <span class="title">Description</span>
                    <div class="info justify">
                      <span class="value"><?php Str::essay(TDF::$data['s'][1]['description']); ?></span>
                      <a class="edit" href="<?php Page::url('submit'); ?>">[edit <img src="<?php Theme::root(); ?>/style/images/icon-edit.png" />]</a>
                    </div>
                  </div><?php
                  }
                  ?>

                  <div class="input-row">
                    <div class="icon"></div>
                    <span class="title">Categories</span>
                    <div class="info major"><?php
                    // Check if there's categories
                    if (isset(TDF::$data['s'][2]['category']) && TDF::$data['s'][2]['category'])
                    {
                      // Set count
                      $countCats = count(TDF::$data['s'][2]['category']);
                      // Loop through each category
                      foreach (TDF::$data['s'][2]['category'] as $i=> $cgId)
                      {
                        // Set category
                        $category = Establishment::getCategoryById($cgId);
                      ?>

                      <span class="value"><a href="#"><?php Str::p($category['name']); ?></a><?php if ($i!=($countCats-1)) echo ', '; ?></span><?php
                      }
                    }
                    else
                    {
                      // Type error
                      ?>

                      <span class="value invalid"><?php Str::p(TDF::$data['se'][2][0]); ?></span><?php
                    }
                    ?>

                      <a class="edit" href="<?php Page::url('submit-step', array('step-2')); ?>">[edit <img src="<?php Theme::root(); ?>/style/images/icon-edit.png" />]</a>
                    </div>
                  </div><?php
                  // If there's description
                  if ($address=Address::join(TDF::$data['s'][3]))
                  {
                  ?>

                  <div class="input-row short">
                    <div class="icon"></div>
                    <span class="title">Address</span>
                    <div class="info">
                      <span class="value fl clear"><?php Str::p($address); if (TDF::$data['s'][3]['hint']) echo ' (<em>',Str::p(TDF::$data['s'][3]['hint'], TRUE),'</em>)'; ?></span>
                      <div class="gmap medium" config="<?php 
                        // Set config
                        $config=array();
                        // If x is set
                        if (TDF::$data['s'][3]['x']) $config['x']=TDF::$data['s'][3]['x'];
                        // If y is set
                        if (TDF::$data['s'][3]['y']) $config['y']=TDF::$data['s'][3]['y'];
                        // If z is set
                        if (TDF::$data['s'][3]['z']) $config['z']=TDF::$data['s'][3]['z'];
                        // If there's config
                        if ($config) Str::p(json_encode($config));
                      ?>"></div>
                      <div class="clear"></div>
                      <a class="edit bottom" href="<?php Page::url('submit-step', array('step-3')); ?>">[edit <img src="<?php Theme::root(); ?>/style/images/icon-edit.png" />]</a>
                    </div>
                  </div><?php
                  }
                  // If there's contact
                  if (isset(TDF::$data['s'][4]['contact']) && TDF::$data['s'][4]['contact'])
                  {
                  ?>

                  <div class="input-row">
                    <div class="icon"></div>
                    <span class="title">Contact Information</span>
                  </div><?php
                    // Loop through each contact information
                    foreach (TDF::$data['s'][4]['contact'] as $type=> $contacts)
                    {
                      // Get type
                      $contactType = Contact::getAllTypes($type);
                      // If there's contacts
                      if ($contacts)
                      {
                  ?>

                  <div class="input-row short">
                    <div class="icon"></div>
                    <span class="title small"><?php Str::p($contactType); ?></span>
                    <div class="info">
                      <?php
                      // Loop through each contact
                      foreach ($contacts as $i=> $contact)
                      {
                        if ($i>0) echo '<br />';
                      ?><span class="value<?php if (isset(TDF::$data['se'][4][$type][$i])) echo ' invalid'; ?>"><?php 
                          // Anchor
                          $anchor = ''; $target = '_self';
                          // If type is website and there's no error
                          if (($type==Contact::TYPE_WEBSITE || $type==Contact::TYPE_EMAIL) && !isset(TDF::$data['se'][4][$type][$i]))
                          {
                            // Set anchor
                            $anchor = (($type==Contact::TYPE_EMAIL)?'mailto:':'').$contact;
                            // If type website
                            if ($type==Contact::TYPE_WEBSITE) $target = '_blank';
                          }
                          // If there's anchor, insert
                          if ($anchor) echo '<a ',Str::attr(array('href'=> $anchor, 'target'=> $target), TRUE),'>';
                          // Print value
                          Str::p(($contact?$contact:'-'));
                          // If there's anchor
                          if ($anchor) echo '</a>';

                          // If there's description
                          if (TDF::$data['s'][4]['description'][$type][$i]) echo ' (<em>',Str::p(TDF::$data['s'][4]['description'][$type][$i], TRUE),'</em>)';
                          // Print error if there's any
                          if (isset(TDF::$data['se'][4][$type][$i])) Str::p(' ('.TDF::$data['se'][4][$type][$i][0].')');

                          ?></span><?php
                      }
                    ?>

                      <a class="edit" href="<?php Page::url('submit-step', array('step-4')); ?>">[edit <img src="<?php Theme::root(); ?>/style/images/icon-edit.png" />]</a>
                    </div>
                  </div><?php
                      }
                    }
                  }
                  // If there are photos
                  if (isset(TDF::$data['s'][5]['temp']) && TDF::$data['s'][5]['temp'])
                  {
                  ?>

                  <div class="input-row">
                    <div class="icon"></div>
                    <span class="title">Photos</span>
                    <div class="info">
                      <div id="photos">
                        <div class="slideshow">
                          <?php
                          // Has title
                          $oneHasTitle = FALSE;
                          // Set default title
                          $defaultTitle = '';
                          // Loop through each photo
                          foreach (TDF::$data['s'][5]['temp'] as $i=> $temp)
                          {
                            // Set attributes
                            $attr = array(
                              'src'=> Image::thumb(File::root('temp').'/'.$temp, '400h', TRUE),
                              'large'=> Image::thumb(File::root('temp').'/'.$temp, '600w', TRUE),
                              'thumb'=> Image::thumb(File::root('temp').'/'.$temp, '100d', TRUE),
                              'title'=> TDF::$data['s'][5]['title'][$i],
                              'default'=> TDF::$data['s'][5]['default'][$i],
                            );
                            // If i > 0
                            if ($i > 0) $attr['class'] = 'hidden';

                            echo '<img ',Str::attr($attr, TRUE),' />';
                            // If there's title
                            if (TDF::$data['s'][5]['title'][$i]) $oneHasTitle=TRUE;
                            // If default
                            if (TDF::$data['s'][5]['default'][$i]) $defaultTitle = TDF::$data['s'][5]['title'][$i];
                          }
                          ?>

                        </div><?php
                        // If there's title
                        if ($oneHasTitle)
                        {
                        ?>

                        <div class="caption"><?php Str::p($defaultTitle?$defaultTitle:' '); ?></div><?php
                        }
                        ?>

                      </div>
                      <a class="edit bottom" href="<?php Page::url('submit-step', array('step-5')); ?>">[edit <img src="<?php Theme::root(); ?>/style/images/icon-edit.png" />]</a>
                    </div>
                  </div><?php
                  }
                ?>

                </div><?php
                // If not logged in
                if (!User::$loggedIn)
                {
                ?>

                <div class="row">
                  <div class="input-row">
                    <div class="icon">
                      <img src="<?php Theme::root(); ?>/style/images/icon-user.png" />
                    </div>
                    <span class="title">Login / Register</span>
                    <div class="info major plain">
                      <span class="value"><a href="<?php Page::url('login'); echo '?next=',Page::url('submit-step', array('step-6'), TRUE); ?>">Click here to login</a> or <a href="<?php Page::url('register'); echo '?next=',Page::url('submit-step', array('step-6'), TRUE); ?>">follow this link to register</a></span>
                      <span class="value minor">
                        <br />
                        <br />• You are required to login before submitting an establishment
                        <br />• Unsaved data will not be lost when you leave this page for login or registration
                        <br />• If you are not redirected automatically to this page upon login, click on the "Submit" link at the top
                      </span>
                    </div>
                  </div>
                </div><?php
                }
                ?>

                <div class="row">
                  <div class="input-row">
                    <div class="icon">
                      <img src="<?php Theme::root(); ?>/style/images/icon-terms.png" />
                    </div>
                    <span class="title">Terms and Policy</span>
                    <div class="info major plain">
                      <span class="value">Please read our <a href="#">Terms of Service</a> and <a href="#">Privacy Policy</a></span>
                      <span class="value minor">
                        <br />
                        <br />By submitting the data above:
                        <br />
                        <br />• You certify that the information is authentic and free from mispellings
                        <br />• You agree that Davao Directory will gain ownership over the photos and other media you upload
                        <br />• You understand that we may anytime change or censor any information or media being uploaded
                        <br />
                        <br />
                        <label class="agree<?php Action::response('agree', array(Response::TYPE_FAILED=> ' invalid'), Action::OPTION_RETURN_ONLY); ?>"><?php Action::input('agree', 'checkbox', NULL, (Action::value('agree')!==NULL)?array('checked'=> 'checked'):NULL); ?> <?php Action::response('agree', array(Response::TYPE_DEFAULT=> 'I agree to the terms of service and privacy policy'), Action::OPTION_OVERWRITE_AND_RETURN); ?></label>
                      </span>
                    </div>
                  </div>
                </div><?php
                // Set error exists
                $errorExists = FALSE;
                // Check if there are errors
                if (TDF::$data['se'] && is_array(TDF::$data['se']))
                {
                  // Loop through each error
                  foreach (TDF::$data['se'] as $step=> $error)
                  {
                    // If step is valid, and there's error
                    if ($step >= 1 && $step <= 5 && $error)
                    {
                      // Set error exists
                      $errorExists = TRUE;
                      // Quit
                      break;
                    }
                  }
                }
                // If there are errors
                if ($errorExists)
                {
                  // Set error messages
                  $steps = array(
                    1=> 'Basic Information',
                    2=> 'Categories',
                    3=> 'Address',
                    4=> 'Contact Information',
                    5=> 'Photos'
                  );
                ?>

                <div class="row">
                  <div class="input-row">
                    <div class="icon">
                      <img src="<?php Theme::root(); ?>/style/images/icon-alert.png" />
                    </div>
                    <span class="title">Errors / Warnings</span>
                    <div class="info major plain">
                      <span class="value invalid">We have detected error/s in your submission</span>
                      <span class="value minor">
                        <br />
                        <br />Please review all data (spelling, required information, etc.):
                        <br /><?php
                        // Loop through each step
                        foreach ($steps as $step=> $title)
                        {
                          // If there's error
                          if (isset(TDF::$data['se'][$step]) && TDF::$data['se'][$step])
                          {
                        ?>

                        <br />• Error/s found in <a href="<?php Page::url('submit-step', array('step-'.$step)); ?>">Step <?php Str::p($step.' - '.$title); ?></a><?php
                          }
                        }
                        ?>

                      </span>
                    </div>
                  </div>
                </div><?php
                }
                ?>

                <div class="foot">
                  <div class="hint"><?php
                    // Export URL
                    $exportUrl = TDF::exportSubmit();
                    // If there's export url
                    if ($exportUrl)
                    {
                    ?>

                    <p>* You can export and share your unsaved data by using this URL: <a href="<?php echo $exportUrl; ?>"><?php echo Str::subs($exportUrl,0,28); ?>..</a>. <a href="#">Click here</a> to learn more about exporting unsaved data</p><?php
                    }
                    ?>

                  </div>
                  <div class="option"></div>
                  <div class="buttons">
                    <?php Action::button('btn_submit', 'submit', 'Submit', 'next', $errorExists?array('class'=> 'disabled', 'disabled'=> 'disabled'):NULL); ?>

                    <span>or</span>
                    <?php Action::button('btn_submit', 'submit', 'Start Over', 'reset', array('confirm'=> 'Are you sure you want to start over?\n\nPress OK to continue')); ?>

                  </div>
                </div>
                <div class="clear overflow"></div>
              <?php Action::end(); ?>

            </div>