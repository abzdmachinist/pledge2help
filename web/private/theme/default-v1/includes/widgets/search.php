<?php

	/**
	 * Search widget
	 * @author Ronald Borla
	 * @copyright May 04, 2013
	 * @package widgetSearch
	 */
	 
?>

        <div class="widget" id="search">
          <?php Action::begin('@search.go', Page::url('search', array(), TRUE), 'get'); ?>

            <div class="logo">
              <a href="<?php Page::url('homepage'); ?>"><img src="<?php Theme::root(); ?>/style/images/logo.png" /></a>
            </div>
            <span class="text-wrap">
              <input type="text" />
            </span>
            <button type="submit">Search</button>
          <?php Action::end(); ?>

        </div>