<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php Theme::title(); ?></title>
<?php
	// Load meta tags
	Theme::meta();
	// Load link tags
	Theme::link();
	// Load scripts
	Theme::script();
?>
</head>
<body>
  <div id="wrapper">
    