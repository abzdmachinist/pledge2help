<?php

	class Station
	{

		/**
		 * Project status
		 */
		const STATUS_INACTIVE = 0;
		const STATUS_ACTIVE = 1;
		const STATUS_SUSPENDED = 2;
		const STATUS_DELETED = 3;

		/**
		 * Get
		 */
		static function get($id)
		{
			// Get station 
			$getStation = Cl::$db->select('p2h_station', self::fields(), TRUE, array(
				array('c'=> 's_id', 't'=> 'i', 'v'=> $id)
			));
			// Return
			return ($getStation['rows'] > 0) ? $getStation['data'] : NULL;
		}

		/**
		 * Search
		 */
		static function search($keyword, $sort, $lat, $lng)
		{
			// Keyword cond
			$keyCond = trim($keyword) ? (' AND p.pr_name LIKE "%'. Cl::$db->escape_string(trim($keyword)) .'%"') : '';
			// Custom query
			$sql = 
				'SELECT s.s_id id,
						((6372.797*(2 * atan2(sqrt((sin(((s.s_lat*(pi()/180)) - ('.$lat.'*(pi()/180))) / 2) * sin(((s.s_lat*(pi()/180)) - ('.$lat.'*(pi()/180))) / 2) + 
						cos('.$lat.'*(pi()/180)) * cos(s.s_lat*(pi()/180)) * sin(((s.s_lng*(pi()/180)) - ('.$lng.'*(pi()/180))) / 2) * sin(((s.s_lng*(pi()/180)) - 
						('.$lng.'*(pi()/180))) / 2))), sqrt(1 - (sin(((s.s_lat*(pi()/180)) - ('.$lat.'*(pi()/180))) / 2) * sin(((s.s_lat*(pi()/180)) - ('.$lat.'*(pi()/180))) / 2) + 
						cos('.$lat.'*(pi()/180)) * cos(s.s_lat*(pi()/180)) * sin(((s.s_lng*(pi()/180)) - ('.$lng.'*(pi()/180))) / 2) * sin(((s.s_lng*(pi()/180)) - ('.$lng.'*(pi()/180))) / 2))))))*1000) `distance`
					FROM 
						p2h_station s, p2h_project p
					WHERE 
						p.pr_id = s.pr_id' . $keyCond .'
						AND p.pr_status = '. Project::STATUS_ACTIVE .'
						AND s.s_status = '. self::STATUS_ACTIVE .'
					ORDER BY '.$sort;

			// Do query
			$query = Cl::$db->query($sql);
			// Set results
			$results = array();
			// Check rows
			if ($query->num_rows)
			{
				// Set
				while ($result = $query->fetch_assoc())
				{
					// Add
					$results[] = $result;
				}
			}
			// Return
			return array(
				'rows'=> $query->num_rows,
				'results'=> $results
			);
		}

		/**
		 * Get all
		 */
		static function getAll($status=NULL)
		{
			// Set condition
			$cond = array();
			// Get stations
			if ($status !== NULL)
			{
				// Add condition
				$cond[] = array('c'=> 's_status', 't'=> 'i', 'v'=> $status);
			}
			// Get all
			$getAllStations = Cl::$db->select('p2h_station', self::fields(), FALSE, $cond);
			// return
			return ($getAllStations['rows'] > 0) ? $getAllStations['data'] : NULL;
		}

		/**
		 * User fields
		 */
		static function fields($addTableName='')
		{
			// Return fields
			return array(
				'id'					=> ($addTableName?($addTableName.'.'):'').'s_id id', 
				'name'		=> ($addTableName?($addTableName.'.'):'').'s_name name', 
				'description'					=> ($addTableName?($addTableName.'.'):'').'s_description description', 
				'address'		=> ($addTableName?($addTableName.'.'):'').'s_address address', 
				'lat'		=> ($addTableName?($addTableName.'.'):'').'s_lat lat', 
				'lng'		=> ($addTableName?($addTableName.'.'):'').'s_lng lng', 
				'zoom'		=> ($addTableName?($addTableName.'.'):'').'s_zoom zoom', 
				'status'=> ($addTableName?($addTableName.'.'):'').'s_status status',
				'prid'=> ($addTableName?($addTableName.'.'):'').'pr_id prid'
			);
		}
	}