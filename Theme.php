<?php
	
	class Theme
	{
		
		// Theme name
		static $name;
		// Private path
		static $path;
		// Theme root
		static $root;
		// Theme files
		static $files;
		
		/**
		 * Initialize theme
		 */
		static function init($theme)
		{
			// Find theme
			if (is_file($path=(PRIVATE_PATH.'/theme/'.$theme.'/index.php')))
			{
				// Set name, path and root
				self::$name = $theme;
				self::$path = dirname($path);
				self::$root = SITE_ROOT.'/dd-theme/'.$theme;
				// Load theme
				require($path);
			}
			else
			{
				// No theme selected
				echo 'Theme not loaded';
			}
		}
		
		/**
		 * Get root
		 * @param bool $return Set true to return only
		 * @return string Theme root
		 */
		static function root($return=FALSE)
		{
			if (!$return)
			{
				echo self::$root;
			}
			// Return root
			return self::$root;
		}
		
		/**
		 * Load a theme file
		 * @param string $file File name
		 * @return bool True if file is found
		 */
		static function load($file)
		{
			if (isset(self::$files[$file]) && is_file(self::$files[$file]))
			{
				// Load file
				require(self::$files[$file]);
				// Return true
				return TRUE;
			}
			// Return false
			return FALSE;
		}
		
		/**
		 * Title
		 * @param string $value Title
		 */
		static function title($value=NULL)
		{
			static $title;
			// If there's title, set it
			if ($value!==NULL)
			{
				$title = $value;
			}
			else
			{
				// Print title
				Str::p($title);
			}
		}
		
		/**
		 * Meta
		 * @param string $name Meta name
		 * @param string $content Meta content
		 */
		static function meta($name=NULL, $content=NULL)
		{
			static $meta;
			// If there's name, then store
			if ($name!==NULL)
			{
				$meta[$name] = $content;
			}
			else
			{
				// Print all meta
				if ($meta)
				{
					foreach ($meta as $metaName=> $metaContent)
					{
						?><meta <?php Str::attr(array('name'=> $metaName, 'content'=> $metaContent)); ?> /><?php
						echo PHP_EOL;
					}
				}
			}
		}
		
		/**
		 * Link
		 * @param string $rel Rel
		 * @param string $href Href
		 * @param string $type Type
		 */
		static function link($rel=NULL, $href=NULL, $type=NULL)
		{
			static $link;
			// If there's rel, href or type
			if ($rel!==NULL || $href!==NULL || $type!==NULL)
			{
				// Tag
				$tag = array();
				if ($rel!==NULL) $tag['rel'] = $rel;
				if ($href!==NULL) $tag['href'] = $href;
				if ($type!==NULL) $tag['type'] = $type;
				// Append to links
				$link[] = $tag;
			}
			else
			{
				// Print all link
				if ($link)
				{
					foreach ($link as $linkTag)
					{
						?><link <?php Str::attr($linkTag); ?> /><?php
						echo PHP_EOL;
					}
				}
			}
		}
		
		/**
		 * Script
		 * @param string $type Type
		 * @param string $src Source
		 * @param string $content Content
		 */
		static function script($type=NULL, $src=NULL, $content=NULL)
		{
			// Script with source
			static $script;
			// Inline script
			static $inline;
			// If there's type, src or content
			if ($type!==NULL || $src!==NULL || $content!==NULL)
			{
				// Tag
				$tag = array();
				if ($type!==NULL) $tag['type'] = $type;
				if ($src!==NULL) $tag['src'] = $src;
				if ($content!==NULL) $tag['content'] = $content;
				// If content only, add to inline
				if ($content && !$src)
				{
					// Append to inline
					$inline[] = $tag;
				}
				else
				{
					// Append to scripts
					$script[] = $tag;
				}
			}
			else
			{
				// If there's no script and inline, declare as array
				if (!$script) $script = array();
				if (!$inline) $inline = array();
				// Merge script and inline
				$script = array_merge($script, $inline);
				// Print all scripts
				if ($script)
				{
					foreach ($script as $scriptTag)
					{
						// Set content
						$content = '';
						// If there's content, unset
						if (isset($scriptTag['content']))
						{
							$content = $scriptTag['content'];
							unset($scriptTag['content']);
						}
						?><script <?php Str::attr($scriptTag); ?>><?php echo $content; ?></script><?php
						echo PHP_EOL;
					}
				}
			}
		}
		
	}