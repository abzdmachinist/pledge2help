<?php
	
	class Session
	{
		
		/**
		 * Set if session is active
		 */
		private static $active=false;
		
		/**
		 * Start session
		 */
		static function start()
		{
			if (!self::$active)
			{
				// Start session
				@session_start();
				self::$active = true;
			}
		}
		
		/**
		 * Close session
		 */
		static function close()
		{
			if (self::$active)
			{
				// Write and close session
				@session_write_close();
				self::$active = false;
			}
		}
		
		/**
		 * Check if session is active
		 *
		 * @return bool True if active, false if not
		 */
		static function active()
		{
			return self::$active;
		}
		
		/**
		 * Get session value
		 * @param string $name Session name
		 * @return mixed Session value
		 */
		static function get($name)
		{
			$value=NULL;
			// Start
			self::start();
			if (isset($_SESSION[$name])) $value = $_SESSION[$name];
			// Close
			self::close();
			// Return with value
			return $value;
		}
		
		/**
		 * Set session value
		 * @param string $name Session name
		 * @param mixed $value Session value
		 */
		static function set($name, $value)
		{
			// Start
			self::start();
			$_SESSION[$name] = $value;
			// Close
			self::close();
		}
		
		/**
		 * Clear session value
		 * @param string $name Session name
		 */
		static function clear($name)
		{
			// Start
			self::start();
			if (isset($_SESSION[$name])) unset($_SESSION[$name]);
			// Close
			self::close();
		}
		
	}